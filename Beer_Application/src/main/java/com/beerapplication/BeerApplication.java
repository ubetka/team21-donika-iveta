package com.beerapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BeerApplication {

    private static final Logger logger = LoggerFactory.getLogger(BeerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BeerApplication.class, args);
    }



}

