package com.beerapplication.controllers;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.*;
import com.beerapplication.models.dtoes.BeerDto;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.models.dtoes.SingleBeerDto;
import com.beerapplication.services.*;
import org.omg.CORBA.BAD_PARAM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static com.beerapplication.utils.Constants.BEER_ALREADY_EXISTS;
import static com.beerapplication.utils.Constants.USER_ALREDY_RATED_BEER;

@Controller
public class BeersController {
    private BeersService service;
    private BreweriesService breweriesService;
    private OriginCountriesService originCountriesService;
    private StylesService stylesService;
    private UsersService usersService;
    private DtoMapper mapper;
    private TagsService tagsService;

    @Autowired
    public BeersController(BeersService service,
                           BreweriesService breweriesService,
                           OriginCountriesService originCountriesService,
                           StylesService stylesService, UsersService usersService,
                           TagsService tagsService, DtoMapper mapper) {
        this.service = service;
        this.breweriesService = breweriesService;
        this.originCountriesService = originCountriesService;
        this.stylesService = stylesService;
        this.usersService = usersService;
        this.tagsService = tagsService;
        this.mapper = mapper;
    }

    @GetMapping("/beers")
    public String showBeers(Model model) {
        model.addAttribute("beers", service.getAll());
        return "beers";
    }

    @GetMapping("/beers/{id}")
    public String showBeer(@Valid @PathVariable int id,
                           Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String usernameAttempt = authentication.getName();
        try {
            Beer beer = service.getById(id);
            SingleBeerDto singleBeerDto = mapper.fromBeerToSingleBeerDto(beer);
            singleBeerDto.setRating(service.getBeerRating(id));

            List<Rating> listOfRatings = service.getListOfRatings(id);
            List<Integer> listOfCreators = new ArrayList<>();
            for (Rating rating : listOfRatings) {
                listOfCreators.add(rating.getUserId());
            }
            singleBeerDto.setRatersList(listOfCreators);

            model.addAttribute("beer", service.getById(id));
            model.addAttribute("singleBeerDto", singleBeerDto);
            model.addAttribute("style", beer.getStyle().getName());
            model.addAttribute("brewery", beer.getBrewery().getName());
            model.addAttribute("country", beer.getOriginCountry().getName());

        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
        try{
            model.addAttribute("user", usersService.getByUsername(usernameAttempt).get(0));
            return "show-beer";
        }catch (IndexOutOfBoundsException iofEx){
            return "redirect:/beers";
        }
    }

//    @GetMapping("/beers/{id}")
//    public String showBeer(Model model, @Valid @PathVariable int id) {
//
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String usernameAttempt = authentication.getName();
//        try {
//            Beer beer = service.getById(id);
//            model.addAttribute("beer", service.getById(id));
//            model.addAttribute("style", beer.getStyle().getName());
//            model.addAttribute("brewery", beer.getBrewery().getName());
//            model.addAttribute("country", beer.getOriginCountry().getName());
//            model.addAttribute("tags", service.getById(id).getTags());
//        } catch (EntityNotFoundException enfEx) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
//        }
//        try{
//            model.addAttribute("user", usersService.getByUsername(usernameAttempt).get(0));
//            return "show-beer";
//        }catch (IndexOutOfBoundsException iofEx){
//            return "redirect:/beers";
//        }
//    }

    @ModelAttribute("styles")
    public List<Style> populateStyles() {
        return stylesService.getAll();
    }

    @ModelAttribute("breweries")
    public List<Brewery> populateBreweries() {
        return breweriesService.getAll();
    }

    @ModelAttribute("countries")
    public List<OriginCountry> populateCountries() {
        return originCountriesService.getAll();
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model) {
        model.addAttribute("newBeer", new BeerDto());
        model.addAttribute("styles", stylesService.getAll());
        model.addAttribute("breweries", breweriesService.getAll());
        model.addAttribute("countries", originCountriesService.getAll());
        return "create-beer";
    }

    @PostMapping("/beers/new")
    public String createBeer (@RequestParam MultipartFile file,
                             @Valid @ModelAttribute("newBeer") BeerDto beerDto,
                             BindingResult errors, Model model) throws Exception{
        if (errors.hasErrors()) {
            String errorMsg = "";
            if (errors.getFieldError() != null) {
                errorMsg = errors.getFieldError().getDefaultMessage();
            }
            return "create-beer";
        }
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String username = authentication.getName();
            Beer beer = mapper.fromBeerDto(beerDto);
            beer.setCreator(usersService.getByUsername(username).get(0));
            if (!file.isEmpty()) {
                beer.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            }
//            beer.setName(beerDto.getName());
//            beer.setAbv(beerDto.getAbv());
//            beer.setDescription(beerDto.getDescription());
//            beer.setStyle(stylesService.getById(beerDto.getStyleId()));
//            beer.setBrewery(breweriesService.getById(beerDto.getBreweryId()));
//            beer.setOriginCountry(originCountriesService.getById(beerDto.getOriginCountryId()));

            service.create(beer);
            return "redirect:/beers";
        }  catch (IndexOutOfBoundsException iobEx) {
            model.addAttribute("error", "You should login or register to create beer.");
            return showNewBeerForm(model);
        } catch (DuplicateEntityException deEx) {
            model.addAttribute("error", String.format(BEER_ALREADY_EXISTS, beerDto.getName()));
            return showNewBeerForm(model);
        }
    }

    @GetMapping("/beers/{id}/update")
    public String showUpdateBeerForm(Model model, @Valid @PathVariable(name = "id") int beerId) {
        Beer beer = service.getById(beerId);
        SingleBeerDto singleBeerDto = mapper.fromBeerToSingleBeerDto(beer);
        model.addAttribute("singleBeerDto", singleBeerDto);
        model.addAttribute("styles", stylesService.getAll());
        model.addAttribute("breweries", breweriesService.getAll());
        model.addAttribute("countries", originCountriesService.getAll());
        model.addAttribute("allTags", tagsService.getAll());
        return "update-beer";
    }

    @PostMapping("/beers/{id}/update")
    public String updateBeer(@Valid @PathVariable(name = "id") int beerId,
                             @RequestParam MultipartFile file,
                             @Valid @ModelAttribute("singleBeerDto") SingleBeerDto singleBeerDto,
                             BindingResult errors,
                             Model model) throws Exception {
        if (errors.hasErrors()) {
            String errorMsg = "";
            if (errors.getFieldError() != null) {
                errorMsg = errors.getFieldError().getDefaultMessage();
            }
            return "update-beer";
        }
        try {
            Beer beerToUpdate = service.getById(beerId);
            beerToUpdate = mapper.fromSingleBeerDtoToBeer(singleBeerDto, beerToUpdate, file);
            service.update(beerId, beerToUpdate);
            return showUpdateBeerForm(model, beerId);
        } catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (DuplicateEntityException deEx) {
            model.addAttribute("error", String.format(BEER_ALREADY_EXISTS, singleBeerDto.getName()));
            return showUpdateBeerForm(model, beerId);
        }
    }

    @GetMapping("/beers/{id}/delete")
    public String showDeleteBeerForm(Model model, @Valid @PathVariable(name = "id") int beerId) {
        Beer beer = service.getById(beerId);
        model.addAttribute("beer", beer);
        return "update-beer";
    }

    @PostMapping("/beers/{id}/delete")
    public String deleteBeer(@Valid @PathVariable(name = "id") int beerId) {
        try {
            service.delete(beerId);
            return "redirect:/beers";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @GetMapping("/filter{style}{country}{tag}{name}")
    public String showFilteredBeers(@RequestParam(defaultValue = "") String style,
                                    @RequestParam(defaultValue = "") String country,
                                    @RequestParam(defaultValue = "") String tag,
                                    @RequestParam(defaultValue = "") String name,
                                    Model model) {
        model.addAttribute("beers", service.getAll(style, country, tag, name));
        return "filter-beers";
    }

//    @GetMapping("/filter{style}{country}{tag}")
//    public String showFilteredBeers(Model model, @RequestParam(defaultValue = "") String style,
//                                    @RequestParam(defaultValue = "") String country,
//                                    @RequestParam(defaultValue = "") String tag) {
//        model.addAttribute("beers", service.getAll(style, country, tag));
//        return "filter-beers";
//    }

    @GetMapping("/beers/sort{by}")
    public String showSortedByTagBeers(@RequestParam String by,
                                       Model model) {
        try {
            model.addAttribute("beers", service.sort(by));
            return "sort-beers";
        }catch (BAD_PARAM bad_param){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, bad_param.getMessage());
        }
    }

    @PostMapping("beers/{id}/rate")
    public String rateBeer(@Valid @PathVariable(name = "id") int beerId,
                           @Valid SingleBeerDto singleBeerDto,
                           BindingResult errors,
                           Model model) {
        if (errors.hasErrors()) {
            String errorMsg = "";
            if (errors.getFieldError() != null) {
                errorMsg = errors.getFieldError().getDefaultMessage();
            }

            return showUpdateBeerForm(model, beerId);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        try {
            User user = usersService.getByUsername(username).get(0);
            Rating rating = new Rating();
            Beer beer = service.getById(beerId);
//            singleBeerDto = mapper.fromBeerToSingleBeerDto(beer);

            rating.setRating(singleBeerDto.getRating());
            rating.setBeerId(beerId);
            rating.setUserId(user.getUserId());
            service.addBeerRating(beerId, rating);

        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        } catch (DuplicateEntityException deEx) {
            model.addAttribute("error", String.format(USER_ALREDY_RATED_BEER, username, singleBeerDto.getName()));
        }
        return "redirect:/beers";
    }
}
