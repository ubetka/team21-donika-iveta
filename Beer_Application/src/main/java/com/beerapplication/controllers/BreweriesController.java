package com.beerapplication.controllers;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Brewery;
import com.beerapplication.services.BreweriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

import static com.beerapplication.utils.Constants.*;
import static com.beerapplication.utils.Constants.MAX_BREWERY_NAME_LENGTH;

@Controller
public class BreweriesController {
    private BreweriesService service;

    @Autowired
    public BreweriesController(BreweriesService service) {
        this.service = service;
    }

    @GetMapping("/breweries")
    public String showBreweries(Model model) {
        model.addAttribute("breweries", service.getAll());
        return "breweries";
    }

    @GetMapping("/breweries/{id}")
    public String showBrewery(Model model, @Valid @PathVariable(name = "id") int breweryId) {
        try {
            model.addAttribute("brewery", service.getById(breweryId));
            return "show-brewery";
        } catch (
                EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @GetMapping("/breweries/new")
    public String showNewBreweryForm(Model model) {
        model.addAttribute("brewery", new Brewery());
        return "create-brewery";
    }

    @PostMapping("/breweries/new")
    public String createBrewery(@Valid @ModelAttribute("brewery") Brewery brewery, BindingResult errors,
                                Model model) {
        if (errors.hasFieldErrors()) {
            return "create-brewery";
        }
        try {
            service.create(brewery);
            return "redirect:/breweries";
        } catch (DuplicateEntityException deEx) {
            model.addAttribute("error", String.format(BREWERY_ALREADY_EXISTS, brewery.getName()));
            return showNewBreweryForm(model);
        }
    }


    @GetMapping("/breweries/{id}/update")
    public String showUpdateBreweryForm(Model model, @Valid @PathVariable(name = "id") int breweryId) {
        try {
            Brewery brewery = service.getById(breweryId);
            model.addAttribute("brewery", brewery);
            return "update-brewery";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @PostMapping("/breweries/{id}/update")
    public String updateBrewery(@Valid @PathVariable(name = "id") int breweryId,
                                @Valid @ModelAttribute("brewery") Brewery brewery, BindingResult errors,
                                Model model) {
        if (errors.hasFieldErrors()) {
            model.addAttribute("error",
                    String.format(BREWERY_NAME_ERROR_MESSAGE
                            .replace("{min}", "%d")
                            .replace("{max}", "%d"), MIN_BREWERY_NAME_LENGTH, MAX_BREWERY_NAME_LENGTH));
            return showUpdateBreweryForm(model, breweryId);
        }
        try {
            service.update(breweryId, brewery);
            return "redirect:/breweries";
        } catch (DuplicateEntityException deEx) {
            model.addAttribute("error", String.format(deEx.getMessage(), brewery.getName()));
            return showUpdateBreweryForm(model, breweryId);
        }
    }

    @GetMapping("/breweries/{id}/delete")
    public String showDeleteBreweryForm(Model model, @Valid @PathVariable(name = "id") int breweryId) {
        try {
            Brewery brewery = service.getById(breweryId);
            model.addAttribute("brewery", brewery);
            return "delete-brewery";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @PostMapping("/breweries/{id}/delete")
    public String deleteBrewery(@Valid @PathVariable(name = "id") int breweryId) {
        try {
            service.delete(breweryId);
            return "redirect:/breweries";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }

    }

}
