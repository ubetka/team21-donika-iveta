package com.beerapplication.controllers;

import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.services.BeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collections;
import java.util.List;

@Controller
public class HomeController {

    private BeersService service;

    @Autowired
    public HomeController(BeersService service) {
        this.service = service;
    }

    @GetMapping("/")
    public String showHomePage(Model model){
        List<BeerGetAllDto> listOfTopBeers = service.getTopThreeBeers();
        model.addAttribute("topBeers", listOfTopBeers);

        BeerGetAllDto topBeer = listOfTopBeers.get(0);
        topBeer.setPicture(service.getById(topBeer.getId()).getPicture());
        model.addAttribute("topBeer", listOfTopBeers.get(0));

        BeerGetAllDto secondBeer = listOfTopBeers.get(1);
        secondBeer.setPicture(service.getById(secondBeer.getId()).getPicture());
        model.addAttribute("secondBeer", listOfTopBeers.get(1));

        BeerGetAllDto thirdBeer = listOfTopBeers.get(1);
        thirdBeer.setPicture(service.getById(thirdBeer.getId()).getPicture());
        model.addAttribute("thirdBeer", listOfTopBeers.get(2));
        return "index";
    }

    @GetMapping("/admin")
    public String showAdminPage(){
        return "admin";
    }

    @GetMapping("/catalogue")
    public String showCatalogue(){
        return "catalogue";
    }
}
