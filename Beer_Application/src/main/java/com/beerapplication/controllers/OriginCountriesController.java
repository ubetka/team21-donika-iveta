package com.beerapplication.controllers;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.OriginCountry;
import com.beerapplication.services.OriginCountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Controller
public class OriginCountriesController {
    OriginCountriesService service;

    @Autowired
    public OriginCountriesController(OriginCountriesService service) {
        this.service = service;
    }

    @GetMapping("/countries")
    public String showCountry(Model model) {
        model.addAttribute("countries", service.getAll());
        return "countries";
    }

    @GetMapping("/countries/{id}")
    public String showOriginCountry(Model model, @Valid @PathVariable(name = "id") int countryId) {
        try {
            model.addAttribute("country", service.getById(countryId));
            return "show-country";
        } catch (
                EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @GetMapping("/countries/new")
    public String showNewCountryForm(Model model) {
        model.addAttribute("country", new OriginCountry());
        return "create-country";
    }

    @PostMapping("/countries/new")
    public String createCountry(@Valid @ModelAttribute("country") OriginCountry originCountry, BindingResult errors) {
        if (errors.hasErrors()) {
            return "create-country";
        }
        try {
            service.create(originCountry);
            return "redirect:/countries";
        } catch (DuplicateEntityException deEx) {
            return "create-country";
        }
    }

    @GetMapping("/countries/{id}/update")
    public String showUpdateCountryForm(Model model, @Valid @PathVariable(name = "id") int countryId) {
        try {
            OriginCountry country = service.getById(countryId);
            model.addAttribute("country", country);
            return "update-country";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @PostMapping("/countries/{id}/update")
    public String updateCountry(@Valid @PathVariable(name = "id") int countryId,
                                @Valid @ModelAttribute("country") OriginCountry country, BindingResult errors) {
        if (errors.hasErrors()) {
            return "update-country";
        }
        try {
            service.update(countryId, country);
            return "redirect:/countries";
        } catch (DuplicateEntityException deEx) {
            return "update-country";
        }
    }

    @GetMapping("/countries/{id}/delete")
    public String showDeleteCountryForm(Model model, @Valid @PathVariable(name = "id") int countryId) {
        try {
            OriginCountry country = service.getById(countryId);
            model.addAttribute("country", country);
            return "delete-country";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @PostMapping("/countries/{id}/delete")
    public String deleteCountry(@Valid @PathVariable(name = "id") int countryId) {
        try {
            service.delete(countryId);
            return "redirect:/countries";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }

    }

}
