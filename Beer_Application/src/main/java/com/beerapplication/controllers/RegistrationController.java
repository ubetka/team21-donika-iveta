package com.beerapplication.controllers;

import com.beerapplication.models.dtoes.UserRegDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {

    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegistrationPage(Model model) {
        model.addAttribute("userRegDto", new UserRegDto());
        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute UserRegDto userRegDto,
                           BindingResult bindingResult,
                           Model model) {
        String errors = "";

        if (bindingResult.hasErrors()) {
            if (bindingResult.getFieldError() != null){
                errors = bindingResult.getFieldError().getDefaultMessage();
            }
            if (bindingResult.getGlobalError() != null) {
                errors = bindingResult.getGlobalError().getDefaultMessage()
                        .replace(bindingResult.getGlobalError().getDefaultMessage(), "Passwords do not match.");
            }

            model.addAttribute("error", errors);
            return "register";
        }
        if (userDetailsManager.userExists(userRegDto.getUsername())){
            model.addAttribute("error", errors);
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User
                        (userRegDto.getUsername(),
                        passwordEncoder.encode(userRegDto.getPassword()), authorities);
        userDetailsManager.createUser(newUser);
        return "register-confirmation";
    }

    @GetMapping("/register-confirmation")
    public String showRegistrationConfirmation(Model model) {
        return "register-confirmation";
    }
}
