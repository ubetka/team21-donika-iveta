package com.beerapplication.controllers;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Style;
import com.beerapplication.services.StylesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Controller
public class StylesController {
    StylesService service;

    @Autowired
    public StylesController(StylesService service) {
        this.service = service;
    }

    @GetMapping("/styles")
    public String showStyles(Model model) {
        model.addAttribute("styles", service.getAll());
        return "styles";
    }

    @GetMapping("/styles/{id}")
    public String showStyle(Model model, @Valid @PathVariable(name = "id") int styleId) {
        try {
            model.addAttribute("style", service.getById(styleId));
            return "show-style";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @GetMapping("/styles/new")
    public String showNewStyleForm(Model model) {
        model.addAttribute("style", new Style());
        return "create-style";
    }


    @PostMapping("/styles/new")
    public String createStyle(@Valid @ModelAttribute("style") Style style, BindingResult errors) {
        if (errors.hasErrors()) {
            return "create-style";
        }
        try {
            service.create(style);
            return "redirect:/styles";
        }catch (DuplicateEntityException deEx){
            return "create-style";
        }

    }

    @GetMapping("/styles/{id}/update")
    public String showUpdateStyleForm(Model model, @Valid @PathVariable(name = "id") int styleId) {
        try {
            Style style = service.getById(styleId);
            model.addAttribute("style", style);
            return "update-style";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping("/styles/{id}/update")
    public String updateStyle(@Valid @PathVariable(name = "id") int styleId,
                              @Valid @ModelAttribute("style") Style style, BindingResult errors) {
        if (errors.hasErrors()) {
            return "update-style";
        }
        try {
            service.update(styleId, style);
            return "redirect:/styles";
        } catch (DuplicateEntityException deEx) {
            return "update-style";
        }
    }

    @GetMapping("/styles/{id}/delete")
    public String showDeleteStyleForm(Model model, @Valid @PathVariable(name = "id") int styleId) {
        try {
            Style style = service.getById(styleId);
            model.addAttribute("style", style);
            return "delete-style";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @PostMapping("/styles/{id}/delete")
    public String deleteStyle(@Valid @PathVariable(name = "id") int styleId) {
        try {
            service.delete(styleId);
            return "redirect:/styles";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }

    }

}
