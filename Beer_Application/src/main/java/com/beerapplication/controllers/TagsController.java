package com.beerapplication.controllers;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Tag;
import com.beerapplication.services.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import static com.beerapplication.utils.Constants.*;

@Controller
public class TagsController {
    private TagsService service;

    @Autowired
    public TagsController(TagsService service) {
        this.service = service;
    }

    @GetMapping("/tags")
    public String showTags(Model model) {
        model.addAttribute("tags", service.getAll());
        return "tags";
    }

    @GetMapping("/tags/filter{name}")
    public String showFilteredTags(@RequestParam String name, Model model) {
        model.addAttribute("tags", service.filterByName(name));
        return "filter-tags";
    }


    @GetMapping("/tags/{id}")
    public String showTag(Model model, @Valid @PathVariable(name = "id") int tagId) {
        try {
            model.addAttribute("tag", service.getById(tagId));
            return "show-tag";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @GetMapping("/tags/new")
    public String showNewTagForm(Model model) {
        model.addAttribute("tag", new Tag());
        return "create-tag";
    }


    @PostMapping("/tags/new")
    public String createTag(@Valid @ModelAttribute("tag") Tag tag, BindingResult errors,
                            Model model) {
        if (errors.hasFieldErrors()) {
            return "create-tag";
        }
        try {
            service.create(tag);
            return "redirect:/tags";
        } catch (DuplicateEntityException deEx) {
            model.addAttribute("error", String.format(TAG_ALREADY_EXISTS, tag.getName()));
            return showNewTagForm(model);
        }
    }

    @GetMapping("/tags/{id}/update")
    public String showUpdateTagForm(Model model, @Valid @PathVariable(name = "id") int tagId) {
        try {
            Tag tag = service.getById(tagId);
            model.addAttribute("tag", tag);
            return "update-tag";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }



    @PostMapping("/tags/{id}/update")
    public String updateTag(@Valid @PathVariable(name = "id") int tagId,
                            @Valid @ModelAttribute("tag") Tag tag, BindingResult errors,
                            Model model) {
        if (errors.hasFieldErrors()) {
            model.addAttribute("error",
                    String.format(TAG_ERROR_MESSAGE
                                    .replace("{min}", "%d")
                                    .replace("{max}", "%d"),
                            MIN_TAG_NAME_LENGTH, MAX_TAG_NAME_LENGTH));
            return showUpdateTagForm(model, tagId);
        }
        try {
            service.update(tagId, tag);
            return "redirect:/tags";
        } catch (DuplicateEntityException deEx) {
            model.addAttribute("error", String.format(TAG_ALREADY_EXISTS, tag.getName()));
            return showUpdateTagForm(model, tagId);
        }
    }

    @GetMapping("/tags/{id}/delete")
    public String showDeleteTagForm(Model model, @Valid @PathVariable(name = "id") int tagId) {
        try {
            Tag tag = service.getById(tagId);
            model.addAttribute("tag", tag);
            return "delete-tag";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }


    @PostMapping("/tags/{id}/delete")
    public String deleteTag(@Valid @PathVariable(name = "id") int tagId) {
        try {
            service.delete(tagId);
            return "redirect:/tags";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }

    }

}
