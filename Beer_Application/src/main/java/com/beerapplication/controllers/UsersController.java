package com.beerapplication.controllers;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Beer;
import com.beerapplication.models.User;
import com.beerapplication.models.dtoes.*;
import com.beerapplication.repositories.UsersRepository;
import com.beerapplication.services.BeersService;
import com.beerapplication.services.UsersService;
import com.beerapplication.utils.UsersHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Base64;
import java.util.List;

@Controller
public class UsersController {

    private UsersService service;
    private BeersService beersService;
    private DtoMapper mapper;
    private PasswordEncoder encoder;

    @Autowired
    public UsersController(UsersService service, BeersService beersService, DtoMapper mapper, PasswordEncoder encoder) {
        this.service = service;
        this.beersService = beersService;
        this.mapper = mapper;
        this.encoder = encoder;
    }

/*    @ModelAttribute("/users")
    public List<UserGetAllDto> populateUsers() {
        return service.getAll();
    }*/

    @GetMapping("/users")
    public String showUsers(Model model) {
        model.addAttribute("users", service.getAll());
        return "users";
    }

    @GetMapping("/users/{username}")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUserProfile(@PathVariable String username, Model model) {
        try {
            User user = service.getByUsername(username).get(0);
            UserProfileDto userProfileDto = mapper.fromUserToUserProfileDto(user);
            model.addAttribute("userProfileDto", userProfileDto);
            model.addAttribute("userwishlist", service.getUserWishList(user.getUserId()));
            model.addAttribute("userdranklist", service.getUserDrankList(user.getUserId()));
            model.addAttribute("beerDto", new BeerDto());

            return "user-profile";
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @GetMapping("/users/{username}/update")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String showUpdateUserForm(@PathVariable String username,
                                     Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String usernameAttempt = authentication.getName();
        User user = service.getByUsername(username).get(0);
        UserProfileDto userProfileDto = mapper.fromUserToUserProfileDto(user);
        model.addAttribute("userProfileDto", userProfileDto);

        return "update-user";
    }

    @PostMapping("/users/{username}/update")
//    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String updateUser (@PathVariable String username,
                             @RequestParam MultipartFile file,
                             @Valid UserProfileDto userProfileDto,
                             BindingResult bindingResult,
                             Model model) throws Exception {
        String errors = "";
        if (bindingResult.hasErrors()) {
            if (bindingResult.getFieldError() != null) {
                errors = bindingResult.getFieldError().getDefaultMessage();
            }
            if (bindingResult.getGlobalError() != null) {
                errors = bindingResult.getGlobalError().getDefaultMessage()
                        .replace(bindingResult.getGlobalError().getDefaultMessage(), "Passwords do not match.");

            }
            model.addAttribute("error", "Passwords do not match.");
//            return "redirect:/users/{username}";
            return showUpdateUserForm(username, model);
        }
        try {
            User updateUser = service.getByUsername(username).get(0);
            UsersHelper.updateParameters(updateUser, userProfileDto);
            updateUser.setPassword(encoder.encode(userProfileDto.getPassword()));
            if (!file.isEmpty()) {
                updateUser.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
            }
//            SecurityContextHolder.getContext().setAuthentication(null);
            service.update(updateUser.getUserId(), updateUser);
            model.addAttribute("userProfileDto", UserProfileDto.class);
            return "redirect:/users/{username}";
        } catch (ResponseStatusException rsEx) {
            return "update-user";
        } catch (DuplicateEntityException deEx) {
            return "update-user";
        }
    }

//    @GetMapping("/users/{username}/delete")
//    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
//    public String showDeleteUserForm(@PathVariable String username,
//                                     Model model) {
//        User user = service.getByUsername(username).get(0);
//        model.addAttribute("user", user);
//
//        return "user-delete";
//    }

    @PostMapping("/users/{username}/delete")
//    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
        public String deleteUser(@PathVariable String username,
                             Model model) {
        User user = service.getByUsername(username).get(0);
        service.delete(user.getUserId());
        SecurityContextHolder.getContext().setAuthentication(null);
//        model.addAttribute("user", user);

        return "redirect:/";
    }

    @GetMapping("users/{username}/wish-list")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String getUserWishList(@PathVariable String username,
                                  Model model){
//        User user = service.getByUsername(username).get(0);
        return "user-profile";
    }

    @GetMapping("/users/{username}/drank-list")
    @PreAuthorize("authentication.principal.username == #username or hasAuthority('ADMIN')")
    public String getUserDrankList(@PathVariable String username,
                                   Model model){
        return "user-profile";
    }

    @PostMapping("/users/{username}/add-beer-to-wish-list")
    public String addBeerToWishList(@PathVariable String username,
                                    BeerGetAllDto beerGetAllDto,
                                    Model model){
        User user = service.getByUsername(username).get(0);
        Beer beer = beersService.getById(beerGetAllDto.getId());
        service.addBeerToWishList(user.getUserId(), beer.getBeerId());
        return "redirect:/";
    }

    @PostMapping("/users/{username}/add-beer-to-drank-list")
    public String addBeerToDrankList(@PathVariable String username,
                                     BeerGetAllDto beerGetAllDto,
                                     Model model){
        User user = service.getByUsername(username).get(0);
        Beer beer = beersService.getById(beerGetAllDto.getId());
        service.addBeerToDrankList(user.getUserId(), beer.getBeerId());
        return "redirect:/";
    }
}

