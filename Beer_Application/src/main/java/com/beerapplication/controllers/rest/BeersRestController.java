package com.beerapplication.controllers.rest;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.*;
import com.beerapplication.models.dtoes.BeerDto;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.services.BeersService;
import com.beerapplication.exceptions.EntityNotFoundException;
import org.omg.CORBA.BAD_PARAM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

import static com.beerapplication.utils.Constants.PICTURE_ADDED_SUCCESS_MESSAGE;
import static com.beerapplication.utils.Constants.PICTURE_REMOVED_SUCCESS_MESSAGE;

@RestController
@RequestMapping("api/beers")
public class BeersRestController {

    private BeersService service;
    private DtoMapper mapper;

    @Autowired
    public BeersRestController(BeersService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<BeerGetAllDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping
    public Beer create(@RequestBody @Valid BeerDto beerDto) {
        try {
            Beer newBeer = mapper.fromBeerDto(beerDto);
            return service.create(newBeer);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @GetMapping("/sort")
    public List<BeerGetAllDto> sort(@RequestParam(defaultValue = "") String orderBy) {
        try {
            return service.sort(orderBy);
        }catch (BAD_PARAM bp){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bp.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Beer delete(@PathVariable int id) {
        try {
            Beer beerToDelete = service.getById(id);
            service.delete(id);
            return beerToDelete;
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id,
                       @RequestBody @Valid BeerDto beerDto) {
        try {
            Beer beerToEdit = mapper.fromBeerDto(beerDto);
            return service.update(id, beerToEdit);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @GetMapping("/{id}/rating")
    public Double getBeerRating(@PathVariable int id){
        try {
            return service.getBeerRating(id);
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,enfEx.getMessage());
        }
    }

    @PostMapping("/{id}/rating")
    public Double addBeerRating(@PathVariable int id,
                                @RequestBody Rating rating){
        try{
            return service.addBeerRating(id, rating);
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }catch (DuplicateEntityException deEx){
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @PutMapping("/{id}/picture")
    public String addPicture(@PathVariable int id,
                             @RequestBody String pictureURL){
        try {
            service.addPicture(id, pictureURL);
            return String.format(PICTURE_ADDED_SUCCESS_MESSAGE, "beer", id);
        }catch (EntityNotFoundException enfEX){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEX.getMessage());
        }
    }

    @DeleteMapping("/{id}/picture")
    public String removePicture(@PathVariable int id){
        try {
            service.removePicture(id);
            return String.format(PICTURE_REMOVED_SUCCESS_MESSAGE, "beer", id);
        }catch (EntityNotFoundException enfEX){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEX.getMessage());
        }
    }
}
