package com.beerapplication.controllers.rest;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Brewery;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.services.BreweriesService;
import com.beerapplication.utils.BeersCollectionHelper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/breweries")
public class BreweriesRestController {

    private BreweriesService service;
    private DtoMapper mapper;

    public BreweriesRestController(BreweriesService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Brewery> getAll(@RequestParam(defaultValue = "") String name) {
        List<Brewery> result = service.getAll();

        result = BeersCollectionHelper.filterBreweryByName(result, name);

        return result;
    }

    @GetMapping("/{id}")
    public Brewery getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping
    public Brewery create(@RequestBody @Valid Brewery brewery) {
        try {
            return service.create(brewery);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Brewery delete(@PathVariable int id) {
        try {
           return service.delete(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery update(@PathVariable int id,
                   @RequestBody @Valid Brewery brewery) {
        try {
            return service.update(id, brewery);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }
}
