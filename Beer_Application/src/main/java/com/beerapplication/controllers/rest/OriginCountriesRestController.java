package com.beerapplication.controllers.rest;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.models.OriginCountry;
import com.beerapplication.services.OriginCountriesService;
import com.beerapplication.utils.BeersCollectionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/countries")
public class OriginCountriesRestController {

    private OriginCountriesService service;
    private DtoMapper mapper;

    @Autowired
    public OriginCountriesRestController(OriginCountriesService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<OriginCountry> getAll(@RequestParam(defaultValue = "") String name) {
        List<OriginCountry> result = service.getAll();

        result = BeersCollectionHelper.filterOriginCountryByName(result, name);

        return result;
    }

    @GetMapping("/{id}")
    public OriginCountry getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping
    public OriginCountry create(@RequestBody @Valid OriginCountry originCountry) {
        try {
            return service.create(originCountry);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @PutMapping("/{id}")
    public OriginCountry update(@PathVariable int id,
                              @RequestBody @Valid OriginCountry originCountry){
        try {
            return service.update(id, originCountry);
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }catch (DuplicateEntityException deEX){
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEX.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public OriginCountry delete(@PathVariable int id){
        try{
            OriginCountry originCountryToDelete = service.getById(id);
            service.delete(id);
            return originCountryToDelete;
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }
}
