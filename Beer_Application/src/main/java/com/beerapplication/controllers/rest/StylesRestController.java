package com.beerapplication.controllers.rest;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.models.Style;
import com.beerapplication.services.StylesService;
import com.beerapplication.utils.BeersCollectionHelper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/styles")
public class StylesRestController {

    private StylesService service;
    private DtoMapper mapper;

    public StylesRestController(StylesService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Style> getAll(@RequestParam(defaultValue = "") String name) {
        List<Style> result = service.getAll();

        result = BeersCollectionHelper.filterStyleByName(result, name);

        return result;
    }

    @GetMapping("/{id}")
    public Style getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping
    public Style create(@RequestBody @Valid Style style){
        try {
            return service.create(style);
        }catch (DuplicateEntityException deEx){
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Style delete(@PathVariable int id){

        try {
           Style styleToDelete = service.getById(id);
           service.delete(id);
           return styleToDelete;
        }catch (EntityNotFoundException enfEX){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEX.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Style update(@PathVariable int id,
                        @RequestBody @Valid Style style) {
        try {
            return service.update(id, style);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }
}
