package com.beerapplication.controllers.rest;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.models.Tag;
import com.beerapplication.services.TagsService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/tags")
public class TagsRestController {

    private TagsService service;
    private DtoMapper mapper;

    public TagsRestController(TagsService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    List<Tag> getAll(@RequestParam(defaultValue = "") String name) {
        if (name.isEmpty()) {
            return service.getAll();
        } else {
            return service.filterByName(name);
        }
    }

    @GetMapping("/{id}")
    Tag getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping
    Tag create(@RequestBody @Valid Tag tag) {
        try {
            return service.create(tag);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @PutMapping("/{id}")
    Tag update(@PathVariable int id,
               @RequestBody @Valid Tag tag) {
        try {
            return service.update(id, tag);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    Tag delete(@PathVariable int id) {
        try {
            return service.delete(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }
}
