package com.beerapplication.controllers.rest;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.*;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.models.dtoes.UserGetAllDto;
import com.beerapplication.models.dtoes.UserRegDto;
import com.beerapplication.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

import static com.beerapplication.utils.Constants.PICTURE_ADDED_SUCCESS_MESSAGE;
import static com.beerapplication.utils.Constants.PICTURE_REMOVED_SUCCESS_MESSAGE;

@RestController
@RequestMapping("api/users")
public class UsersRestController {
    private UsersService service;
    private DtoMapper mapper;

    @Autowired
    public UsersRestController(UsersService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<UserGetAllDto> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PostMapping
    public User create(@RequestBody @Valid UserRegDto userRegDto) {
        try {
            User newUser = mapper.fromRegUserDto(userRegDto);
            return service.create(newUser);
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User update(@PathVariable int id,
                       @RequestBody @Valid User user) {
        try {
            return service.update(id, user);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        } catch (DuplicateEntityException deEx) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, deEx.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public User delete(@PathVariable int id) {
        try {
            return service.delete(id);
        } catch (EntityNotFoundException enfEx) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @GetMapping("/{id}/wish-list")
    public Set<BeerGetAllDto> getUserWishList(@PathVariable int id){
        try{
            return service.getUserWishList(id);
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PutMapping ("/{userId}/wish-list/{beerId}")
    public Set<BeerGetAllDto> addBeerToWishList(@PathVariable int userId,
                                                @PathVariable int beerId){
        try{
            return service.addBeerToWishList(userId, beerId);
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @GetMapping("/{id}/drank-list")
    public Set<BeerGetAllDto> getUserDrankList(@PathVariable int id){
        try{
            return service.getUserDrankList(id);
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

    @PutMapping("/{userId}/drank-list/{beerId}")
    public Set<BeerGetAllDto> addBeerToDrankList(@PathVariable int userId,
                                                 @PathVariable int beerId){
        try{
            return service.addBeerToDrankList(userId, beerId);
        }catch (EntityNotFoundException enfEx){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
        }
    }

//    @PutMapping("/{id}/picture")
//    public String addPicture(@PathVariable int id,
//                             @RequestBody String pictureURL){
//        try{
//            service.addPicture(id, pictureURL);
//            return String.format(PICTURE_ADDED_SUCCESS_MESSAGE,"user", id);
//        }catch (Exception enfEx){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
//        }
//    }
//
//    @DeleteMapping("/{id}/picture")
//    public String removePicture(@PathVariable int id){
//        try{
//            service.removePicture(id);
//            return String.format(PICTURE_REMOVED_SUCCESS_MESSAGE,"user", id);
//        }catch (EntityNotFoundException enfEx){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, enfEx.getMessage());
//        }
//    }
}
