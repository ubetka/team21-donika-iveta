package com.beerapplication.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BeerID")
    private int beerId;

    @Column(name = "Name")
    private String name;

    @Column(name = "Description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "OriginCountryID")
    private OriginCountry originCountry;

    @ManyToOne
    @JoinColumn(name = "BreweryID")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name = "StyleID")
    private Style style;

    @ManyToMany
    @JoinTable(name = "beers_tags",
    joinColumns = @JoinColumn (name = "BeerID"),
    inverseJoinColumns = @JoinColumn(name = "TagID"))
    private List<Tag> tags;

    @Column (name = "ABV")
    private Double abv;

    @Column(name = "Enabled")
    private boolean enabled;

    @Lob
    @Column(name = "Picture")
    private String picture;

    @ManyToOne
    @JoinColumn(name = "UserID")
    @JsonIgnore
    private User creator;

    public Beer() {
        enabled = true;
        tags = new ArrayList<>();
    }

    public Beer(String name, String description, Double abv) {
        this.name = name;
        this.description = description;
        this.abv = abv;
        enabled = true;
        tags = new  ArrayList<>();
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OriginCountry getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(OriginCountry originCountry) {
        this.originCountry = originCountry;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Double getAbv() {
        return abv;
    }

    public void setAbv(Double abv) {
        this.abv = abv;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void addTag(Tag tag){
        tags.add(tag);
    }
}
