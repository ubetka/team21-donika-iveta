package com.beerapplication.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.beerapplication.utils.Constants.*;

@Entity
@Table(name = "breweries")
public class Brewery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BreweryID")
    @PositiveOrZero
    private int breweryId;

    @NotNull
    @Size(min = MIN_BREWERY_NAME_LENGTH, max = MAX_BREWERY_NAME_LENGTH, message = BREWERY_NAME_ERROR_MESSAGE)
    @Column(name = "Name")
    private String name;

    @Column(name = "Enabled")
    private boolean enabled;

    public Brewery() {
        enabled = true;
    }

    public Brewery(String name) {
        this.name = name;
        enabled = true;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
