package com.beerapplication.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.beerapplication.utils.Constants.*;

@Entity
@Table (name = "origin_countries")
public class OriginCountry {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "OriginCountryID")
    @PositiveOrZero
    private int originCountryId;

    @NotNull
    @Size(min = MIN_ORIGIN_COUNTRY_NAME_LENGTH, max = MAX_ORIGIN_COUNTRY_NAME_LENGTH , message = ORIGIN_COUNTRY_NAME_ERROR_MESSAGE)
    @Column (name = "Name")
    private String name;

    @Column (name = "Enabled")
    private boolean enabled;

    public OriginCountry() {
        enabled = true;
    }

    public OriginCountry(String name) {
        this.name = name;
        enabled = true;
    }

    public int getOriginCountryId() {
        return originCountryId;
    }

    public void setOriginCountryId(int originCountryId) {
        this.originCountryId = originCountryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
