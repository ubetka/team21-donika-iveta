package com.beerapplication.models;

import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Table(name = "beers_ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BeerRatingID")
    @PositiveOrZero
    private int beerRatingId;

    @Column(name = "BeerID")
    @PositiveOrZero
    private int beerId;

    @Column(name = "UserID")
    @PositiveOrZero
    private int userId;

    @Column(name = "Rating")
    @PositiveOrZero
    private Double rating;

    public Rating() {
    }

    public int getBeerRatingId() {
        return beerRatingId;
    }

    public void setBeerRatingId(int beerRatingId) {
        this.beerRatingId = beerRatingId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}
