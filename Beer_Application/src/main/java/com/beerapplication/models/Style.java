package com.beerapplication.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.beerapplication.utils.Constants.*;

@Entity
@Table(name = "styles")
public class Style {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "StyleID")
    @PositiveOrZero
    private int styleId;

    @NotNull
    @Size(min = MIN_STYLE_NAME_LENGTH, max = MAX_STYLE_NAME_LENGTH, message = STYLE_NAME_ERROR_MESSAGE)
    @Column(name = "Name")
    private String name;

    @Column(name = "Enabled")
    private boolean enabled;

    public Style() {
        enabled = true;
    }

    public Style(String name) {
        this.name = name;
        enabled = true;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
