package com.beerapplication.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.beerapplication.utils.Constants.*;

@Entity
@Table(name = "tags")
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "TagID")
    @PositiveOrZero
    private int id;

    @Column (name = "Name")
    @NotNull
    @Size(min = MIN_TAG_NAME_LENGTH, max = MAX_TAG_NAME_LENGTH, message = TAG_ERROR_MESSAGE)
    private String name;

    @Column (name = "Enabled")
    private boolean enabled;

    public Tag() {
        enabled = true;
    }

    public Tag(String name) {
        this.name = name;
        enabled = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
