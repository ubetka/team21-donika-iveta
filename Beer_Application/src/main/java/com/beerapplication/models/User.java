package com.beerapplication.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UserID")
    private int userId;

    @Column(name = "Username")
    private String username;

    @Column(name = "Email")
    private String email;

    @Column(name = "Password")
    private String password;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @Lob
    @Column(name = "Picture")
    private String picture;

    @Column(name = "Enabled")
    private boolean enabled;

//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "authorities",
//            joinColumns = @JoinColumn(name = "UserID"),
//            inverseJoinColumns = @JoinColumn(name = "RoleID")
//    )
//    private List<Role> roles;

    @ManyToMany
    @JoinTable(name = "users_drank_beers",
    joinColumns = @JoinColumn(name = "UserID"),
    inverseJoinColumns = @JoinColumn(name = "BeerID"))
    private Set<Beer> drankList;

    @ManyToMany
    @JoinTable(name = "users_wished_beers",
    joinColumns = @JoinColumn(name = "UserID"),
    inverseJoinColumns = @JoinColumn(name = "BeerID"))
    private Set<Beer> wishList;

    @Transient
    private String passwordConfirmation;

    public User() {
        enabled = true;
//        roles = new ArrayList<>();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

//    public List<Role> getRoles() {
//        return new ArrayList<>(roles);
//    }
//
//    public void setRoles(List<Role> roles) {
//        this.roles = roles;
//    }

    public Set<Beer> getDrankList() {
        return drankList;
    }

    public void setDrankList(Set<Beer> drankList) {
        this.drankList = drankList;
    }

    public Set<Beer> getWishList() {
        return wishList;
    }

    public void setWishList(Set<Beer> wishList) {
        this.wishList = wishList;
    }


}
