package com.beerapplication.models.dtoes;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.beerapplication.utils.Constants.*;

public class BeerDto {

    @PositiveOrZero
    private int id;

    @NotNull
    @Size(min = MIN_BEER_NAME_LENGTH, max = MAX_BEER_NAME_LENGTH, message = BEER_NAME_ERROR_MESSAGE)
    private String name;

    @Size(min = MIN_DESCRIPTION_NAME_LENGTH, max = MAX_DESCRIPTION_NAME_LENGTH, message = DESCRIPTION_ERROR_MESSAGE)
    private String description;

    @NotNull
    @PositiveOrZero
    private Double abv;

    @PositiveOrZero
    private int styleId;

    @PositiveOrZero
    private int breweryId;

    @PositiveOrZero
    private int originCountryId;

    @PositiveOrZero
    private int creatorId;

    private String picture;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAbv() {
        return abv;
    }

    public void setAbv(Double abv) {
        this.abv = abv;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getOriginCountryId() {
        return originCountryId;
    }

    public void setOriginCountryId(int originCountryId) {
        this.originCountryId = originCountryId;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
