package com.beerapplication.models.dtoes;

public class BeerGetAllDto {
    private int id;
    private String name;
    private Double abv;
    private String style;
    private Double rating;
    private String picture;

    public BeerGetAllDto() {
    }

    public BeerGetAllDto(int id, String name, Double abv, String style) {
        this.id = id;
        this.name = name;
        this.abv = abv;
        this.style = style;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAbv() {
        return abv;
    }

    public void setAbv(Double abv) {
        this.abv = abv;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
