package com.beerapplication.models.dtoes;

import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.*;
import com.beerapplication.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.Base64;
import java.util.HashSet;
import java.util.Set;

@Component
public class DtoMapper {
    private BeersRepository beersRepository;
    private StylesRepository stylesRepository;
    private BreweriesRepository breweriesRepository;
    private OriginCountriesRepository originCountriesRepository;
    private UsersRepository usersRepository;
    private TagsRepository tagsRepository;

    @Autowired
    public DtoMapper(BeersRepository beersRepository,
                     StylesRepository stylesRepository,
                     BreweriesRepository breweriesRepository,
                     OriginCountriesRepository originCountriesRepository,
                     UsersRepository usersRepository,
                     TagsRepository tagsRepository) {
        this.beersRepository = beersRepository;
        this.stylesRepository = stylesRepository;
        this.breweriesRepository = breweriesRepository;
        this.originCountriesRepository = originCountriesRepository;
        this.usersRepository = usersRepository;
        this.tagsRepository = tagsRepository;
    }

    public Beer fromBeerDto(BeerDto beerDto) {
        Beer beer = new Beer(beerDto.getName(), beerDto.getDescription(), beerDto.getAbv());

        Style style;
        if (beerDto.getStyleId() != 0) {
            try {
                style = stylesRepository.getById(beerDto.getStyleId());
            } catch (EntityNotFoundException enf) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, enf.getMessage());
            }
            beer.setStyle(style);
        }

        Brewery brewery;
        if (beerDto.getBreweryId() != 0) {
            try {
                brewery = breweriesRepository.getById(beerDto.getBreweryId());
            } catch (EntityNotFoundException enf) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, enf.getMessage());
            }
            beer.setBrewery(brewery);
        }

        OriginCountry originCountry;
        if (beerDto.getOriginCountryId() != 0) {
            try {
                originCountry = originCountriesRepository.getById(beerDto.getOriginCountryId());
            } catch (EntityNotFoundException enf) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, enf.getMessage());
            }
            beer.setOriginCountry(originCountry);
        }

        User user;
        if (beerDto.getCreatorId() != 0) {
            try {
                user = usersRepository.getById(beerDto.getCreatorId());
                beer.setCreator(user);
            } catch (EntityNotFoundException enf) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, enf.getMessage());
            }
        }


        return beer;
    }

    public BeerDto fromBeerToBeerDto(Beer beer) {
        BeerDto beerDto = new BeerDto();
        beerDto.setId(beer.getBeerId());
        if (beer.getName() != null) {
            beerDto.setName(beer.getName());
        }

        if (beer.getAbv() != null) {
            beerDto.setAbv(beer.getAbv());
        }

        if (beer.getDescription() != null) {
            beerDto.setDescription(beer.getDescription());
        }

        if (beer.getStyle() != null) {
            beerDto.setStyleId(beer.getStyle().getStyleId());
        }

        if (beer.getBrewery() != null) {
            beerDto.setBreweryId(beer.getBrewery().getBreweryId());
        }

        if (beer.getOriginCountry() != null) {
            beerDto.setOriginCountryId(beer.getOriginCountry().getOriginCountryId());
        }

        if (beer.getCreator() != null) {
            beerDto.setCreatorId(beer.getCreator().getUserId());
        }
        if (beer.getPicture() != null){
            beerDto.setPicture(beer.getPicture());
        }
        return beerDto;
    }

    public BeerGetAllDto fromBeerToBeerGetAllDto(Beer beer){
        BeerGetAllDto beerGetAllDto = new BeerGetAllDto();
        beerGetAllDto.setId(beer.getBeerId());
        beerGetAllDto.setName(beer.getName());
        beerGetAllDto.setAbv(beer.getAbv());
        beerGetAllDto.setStyle(beer.getStyle().getName());
        beerGetAllDto.setRating(beersRepository.getBeerRating(beer.getBeerId()).get(0));
        return beerGetAllDto;
    }

    public SingleBeerDto fromBeerToSingleBeerDto(Beer beer) {
        SingleBeerDto singleBeerDto = new SingleBeerDto();
        singleBeerDto.setCreatorId(beer.getCreator().getUserId());
        if (beer.getBeerId() != 0) {
            singleBeerDto.setBeerId(beer.getBeerId());
        }

        if (beer.getName() != null) {
            singleBeerDto.setName(beer.getName());
        }

        if (beer.getAbv() != null) {
            singleBeerDto.setAbv(beer.getAbv());
        }

        if (beer.getDescription() != null) {
            singleBeerDto.setDescription(beer.getDescription());
        }

        if (beer.getStyle() != null) {
            singleBeerDto.setStyleId(beer.getStyle().getStyleId());
        }

        if (beer.getBrewery() != null) {
            singleBeerDto.setBreweryId(beer.getBrewery().getBreweryId());
        }

        if (beer.getOriginCountry() != null) {
            singleBeerDto.setOriginCountryId(beer.getOriginCountry().getOriginCountryId());
        }

        if (beer.getPicture() != null){
            singleBeerDto.setPicture(beer.getPicture());
        }

        for (Tag tag:beer.getTags()) {
            singleBeerDto.addTag(tag.getName());
        }
        return singleBeerDto;
    }

    public Beer fromSingleBeerDtoToBeer(SingleBeerDto singleBeerDto,
                                        Beer beerToUpdate,
                                        MultipartFile file) throws Exception{

        beerToUpdate.setName(singleBeerDto.getName());
        beerToUpdate.setAbv(singleBeerDto.getAbv());
        beerToUpdate.setDescription(singleBeerDto.getDescription());
        beerToUpdate.setStyle(stylesRepository.getById(singleBeerDto.getStyleId()));
        beerToUpdate.setBrewery(breweriesRepository.getById(singleBeerDto.getBreweryId()));
        beerToUpdate.setOriginCountry(originCountriesRepository.getById(singleBeerDto.getOriginCountryId()));
        if(!file.isEmpty()) {
            beerToUpdate.setPicture(Base64.getEncoder().encodeToString(file.getBytes()));
        }
        beerToUpdate.addTag(tagsRepository.getTagByName(singleBeerDto.getTagsList().get(0)));

        return beerToUpdate;
    }

    public User fromRegUserDto(UserRegDto userRegDto) {
        User user = new User();
        user.setUsername(userRegDto.getUsername());
        user.setEmail(userRegDto.getEmail());
        user.setPassword(userRegDto.getPassword());
        return user;
    }

    public UserGetAllDto fromUserToUserGetAllDto(User user){
        UserGetAllDto userGetAllDto = new UserGetAllDto();
        userGetAllDto.setUserId(user.getUserId());
        userGetAllDto.setUsername(user.getUsername());
        userGetAllDto.setPicture(user.getPicture());
        return userGetAllDto;
    }

    public UserProfileDto fromUserToUserProfileDto(User user){
        UserProfileDto userProfileDto = new UserProfileDto();
        userProfileDto.setUsername(user.getUsername());
        userProfileDto.setEmail(user.getEmail());
        userProfileDto.setFirstName(user.getFirstName());
        userProfileDto.setLastName(user.getLastName());
        userProfileDto.setPassword(user.getPassword());
        userProfileDto.setPasswordConfirmation(user.getPasswordConfirmation());
        userProfileDto.setPicture(user.getPicture());
        return userProfileDto;
    }

    public Set<BeerGetAllDto> fillSetOfBeersIntoBeerGetAllDto(Set<Beer> beers){
        Set<BeerGetAllDto> userListDtos = new HashSet<>();

        for (Beer beer : beers) {
            userListDtos.add(fromBeerToBeerGetAllDto(beer));
        }
        return userListDtos;
    }
}
