package com.beerapplication.models.dtoes;

import java.util.ArrayList;
import java.util.List;

public class SingleBeerDto {

    private int beerId;
    private String name;
    private Double abv;
    private String description;
    private int originCountryId;
    private int breweryId;
    private int styleId;
    private int creatorId;
    private String picture;
    private Double rating;
    private List<String> tagsList;
    private List<Integer> ratersList;

    public SingleBeerDto() {
        tagsList = new ArrayList<>();
        ratersList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOriginCountryId() {
        return originCountryId;
    }

    public void setOriginCountryId(int originCountryId) {
        this.originCountryId = originCountryId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public Double getAbv() {
        return abv;
    }

    public void setAbv(Double abv) {
        this.abv = abv;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public List<String> getTagsList() {
        return tagsList;
    }

    public void addTag(String tag){
        tagsList.add(tag);
    }

    public void setTagsList(List<String> tagsList) {
        this.tagsList = tagsList;
    }

    public List<Integer> getRatersList() {
        return ratersList;
    }

    public void setRatersList(List<Integer> ratersList) {
        this.ratersList = ratersList;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }
}