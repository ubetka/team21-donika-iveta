package com.beerapplication.models.dtoes;

public class UserGetAllDto {
    private int userId;
    private String username;
    private String picture;

    public UserGetAllDto() {
    }

    public UserGetAllDto(int userId, String username, String picture) {
        this.userId = userId;
        this.username = username;
        this.picture = picture;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
