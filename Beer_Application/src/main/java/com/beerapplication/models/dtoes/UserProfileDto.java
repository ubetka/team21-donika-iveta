package com.beerapplication.models.dtoes;

import com.beerapplication.models.Role;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import java.util.ArrayList;
import java.util.List;

import static com.beerapplication.utils.Constants.*;
import static com.beerapplication.utils.Constants.USER_LAST_NAME_ERROR_MESSAGE;

@ScriptAssert(lang = "javascript", alias = "_",
        script = "_.passwordConfirmation != null && _.passwordConfirmation.equals(_.password)")
public class UserProfileDto {
    @PositiveOrZero
    private int id;

    private String username;

    @NotNull
    @Size(min = MIN_EMAIL_LENGTH, max = MAX_EMAIL_LENGTH, message = USER_EMAIL_ERROR_MESSAGE)
    private String email;

    @NotNull
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH, message = PASSWORD_ERROR_MESSAGE)
    private String password;

    @NotNull
    private String passwordConfirmation;

    @NotNull
    @Size(min = MIN_USER_FIRST_NAME_LENGTH, max = MAX_USER_FIRST_NAME_LENGTH, message = USER_FIRST_NAME_ERROR_MESSAGE)
    private String firstName;

    @NotNull
    @Size(min = MIN_USER_LAST_NAME_LENGTH, max = MAX_USER_LAST_NAME_LENGTH, message = USER_LAST_NAME_ERROR_MESSAGE)
    private String lastName;

//    @NotNull
//    private List<Role> roles;

    private String picture;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //    public List<Role> getRoles() {
//        return new ArrayList<>(roles);
//    }
//
//    public void setRoles(List<Role> roles) {
//        this.roles = roles;
//    }
}
