package com.beerapplication.models.dtoes;

import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import static com.beerapplication.utils.Constants.*;

@ScriptAssert(lang = "javascript", alias = "_",
        script = "_.passwordConfirmation != null && _.passwordConfirmation.equals(_.password)")
public class UserRegDto {
    @PositiveOrZero
    private int id;

    @NotNull
    @Size(min = MIN_USERNAME_LENGTH, max = MAX_USERNAME_LENGTH, message = USERNAME_ERROR_MESSAGE)
    private String username;

    @NotNull
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH, message = PASSWORD_ERROR_MESSAGE)
    private String password;

    @NotNull
    private String passwordConfirmation;

    //    @NotNull
    @Size(min = MIN_EMAIL_LENGTH, max = MAX_EMAIL_LENGTH, message = USER_EMAIL_ERROR_MESSAGE)
    private String email;

    public UserRegDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

        public String getPasswordConfirmation() {
            return passwordConfirmation;
        }

        public void setPasswordConfirmation(String passwordConfirmation) {
            this.passwordConfirmation = passwordConfirmation;
        }
}
