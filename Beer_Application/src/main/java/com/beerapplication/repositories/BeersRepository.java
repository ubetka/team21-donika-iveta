package com.beerapplication.repositories;

import com.beerapplication.models.Beer;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.Rating;
import com.beerapplication.models.dtoes.SingleBeerDto;

import java.util.List;

public interface BeersRepository {

    Beer create(Beer beer);

    List<BeerGetAllDto> getAll();

    Beer getById(int id);

    List<Beer> getBeerByName(String name);

    boolean checkIfBeerExists(String name);

    Beer update(int id, Beer beer);

    Beer delete(int id);

    List<Beer> filterByName(String beerName);

    List<Beer> filterByStyle(String style);

    List<Beer> filterByCountry(String originCountry);

    List<Beer> filterByTag(String tag);

    List<BeerGetAllDto> sortByName();

    List<BeerGetAllDto> sortByAbvAsc();

    List<BeerGetAllDto> sortByAbvDesc();

    List<BeerGetAllDto> sortByRatingAsc();

    List<BeerGetAllDto> sortByRatingDesc();

    List<Double> getBeerRating(int id);

    List<Rating> getListOfRatings(int id);

    Double addBeerRating(int id, Rating rating);

    void addPicture(int beerId, String pictureUrl);

    void removePicture(int beerId);

    List<BeerGetAllDto> getTopThreeBeers();
}
