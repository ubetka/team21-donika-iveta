package com.beerapplication.repositories;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Beer;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.Rating;
import com.beerapplication.models.Tag;
import com.beerapplication.models.dtoes.SingleBeerDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.beerapplication.utils.BeersCollectionHelper.fillListOfBeersIntoBeerGetAllDto;
import static com.beerapplication.utils.BeersCollectionHelper.updateParameters;
import static com.beerapplication.utils.Constants.*;

@Repository
public class BeersRepositoryImpl implements BeersRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BeersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Beer create(Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(beer);
        return beer;
    }

    @Override
    public List<BeerGetAllDto> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<BeerGetAllDto> query = session.
                createQuery("select distinct new com.beerapplication.models.dtoes.BeerGetAllDto" +
                                "(beerId, name, abv, style.name) " +
                                "from Beer where enabled = true order by beerId",
                        BeerGetAllDto.class);
        List<BeerGetAllDto> list = query.list();
        return addRatingToBeerGetAllDto(list);
    }

    public List<Beer> filterByName(String beerName) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.
                createQuery("from Beer where enabled = true " +
                        "and name like :beerName " +
                        "   order by beerId", Beer.class);
        query.setParameter("beerName", "%" + beerName + "%");
        return query.list();
    }

    public List<Beer> filterByStyle(String style) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.
                createQuery("from Beer where enabled = true " +
                        "and style.name like :style " +
                        "order by beerId", Beer.class);
        query.setParameter("style", "%" + style + "%");
        return query.list();
    }

    public List<Beer> filterByCountry(String originCountry) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.
                createQuery("from Beer where enabled = true " +
                        "and originCountry.name like :originCountry " +
                        "order by beerId", Beer.class);
        query.setParameter("originCountry", "%" + originCountry + "%");
        return query.list();
    }

    public List<Beer> filterByTag(String tag) {
        Session session = sessionFactory.getCurrentSession();
        Query<Tag> tagQuery = session.createQuery("from Tag where name like :tag and enabled = true");
        tagQuery.setParameter("tag", "%" + tag + "%");
        Query<Beer> query = session.
                createQuery("from Beer where enabled = true " +
                        "order by beerId", Beer.class);
        List<Beer> beers = query.list();

        return beers = beers.stream().
                filter(beer -> beer.getTags()
                        .stream()
                        .anyMatch(
                                (tag1 -> tag1.getName().toLowerCase().contains(tag.toLowerCase()))))
                .collect(Collectors.toList());
    }

    @Override
    public List<BeerGetAllDto> sortByName() {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> list = session
                .createQuery("from Beer where enabled = true order by name", Beer.class)
                .list();
        return fillListOfBeersIntoBeerGetAllDto(list);
    }

    @Override
    public List<BeerGetAllDto> sortByAbvAsc() {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> list = session
                .createQuery("from Beer where enabled = true order by abv asc", Beer.class)
                .list();
        return fillListOfBeersIntoBeerGetAllDto(list);
    }

    @Override
    public List<BeerGetAllDto> sortByAbvDesc() {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> list = session
                .createQuery("from Beer where enabled = true order by abv desc", Beer.class)
                .list();
        return fillListOfBeersIntoBeerGetAllDto(list);
    }

    @Override
    public List<BeerGetAllDto> sortByRatingAsc() {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> list = session
                .createQuery("from Beer where enabled = true", Beer.class)
                .list();
        List<BeerGetAllDto> beers = fillListOfBeersIntoBeerGetAllDto(list);
        addRatingToBeerGetAllDto(beers);
        return beers.stream()
                .sorted(Comparator.comparingDouble(BeerGetAllDto::getRating))
                .collect(Collectors.toList());
    }

    @Override
    public List<BeerGetAllDto> sortByRatingDesc() {
        Session session = sessionFactory.getCurrentSession();
        List<Beer> list = session
                .createQuery("from Beer where enabled = true", Beer.class)
                .list();
        List<BeerGetAllDto> beers = fillListOfBeersIntoBeerGetAllDto(list);
        addRatingToBeerGetAllDto(beers);
        return beers.stream()
                .sorted(Comparator.comparingDouble(BeerGetAllDto::getRating).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public Beer getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Beer beer = session.get(Beer.class, id);
        if (beer == null || !beer.isEnabled()) {
            throw new EntityNotFoundException(String.format(BEER_NOT_FOUND, id));
        }
        return beer;
    }

    @Override
    public List<Beer> getBeerByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery(
                    "from Beer where name = :name and enabled = true order by beerId",
                    Beer.class);
            query.setParameter("name", name);
            if (query == null) {
                throw new EntityNotFoundException(String.format(BEER_NAME_NOT_FOUND, name));
            }
            return query.list();
        }
    }

    @Override
    public boolean checkIfBeerExists(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where name = :name order by beerId", Beer.class);
        query.setParameter("name", name);
        return query.list().size() != 0;
    }

    @Override
    public Beer update(int id, Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Beer beerToUpdate = getById(id);

        if (beer == null || !beer.isEnabled()) {
            throw new EntityNotFoundException(String.format(BEER_NOT_FOUND, id));
        }

        updateParameters(beerToUpdate, beer);

        session.update(beerToUpdate);
        session.getTransaction().commit();
        return beer;
    }

    @Override
    public Beer delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Beer beerToDelete = getById(id);
        beerToDelete.setEnabled(false);
        session.update(beerToDelete);
        session.getTransaction().commit();
        return beerToDelete;
    }

    @Override
    public List<Double> getBeerRating(int id) {
        Beer beer = getById(id);
        Session session = sessionFactory.getCurrentSession();
        Query<Double> query = session.createQuery(
                "select avg(rating) from Rating where beerId = :id " +
                        "order by beerId", Double.class);
        query.setParameter("id", id);
        return query.list();
    }

    public List<Rating> getListOfRatings(int id){
        Session session = sessionFactory.getCurrentSession();
        Query<Rating> query = session.createQuery(
                "from Rating where beerId = :id order by beerId", Rating.class)
                .setParameter("id", id);
        return query.list();
    }

    @Override
    public Double addBeerRating(int id, Rating rating) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Beer beer = getById(id);
            session.save(rating);
            session.getTransaction().commit();
            return rating.getRating();
        } catch (Exception e) {
            throw new DuplicateEntityException(String.format(BEER_ALREADY_RATED, rating.getUserId(), rating.getBeerId()));
        }
    }

    @Override
    public void addPicture(int beerId, String pictureUrl) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Beer beer = getById(beerId);
        beer.setPicture(pictureUrl);
        session.save(beer);
        session.getTransaction().commit();
    }

    @Override
    public void removePicture(int beerId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Beer beer = getById(beerId);
        beer.setPicture(null);
        session.save(beer);
        session.getTransaction().commit();
    }

    private List<BeerGetAllDto> addRatingToBeerGetAllDto(List<BeerGetAllDto> list) {
        for (int i = 0; i < list.size(); i++) {
            BeerGetAllDto beer = list.get(i);
            beer.setRating(getBeerRating(beer.getId()).get(0));
            if (beer.getRating() == null){
                list.remove(i);
                i -= 1;
            }
        }
        return list;
    }

    private List<BeerGetAllDto> addRatingToBeerGetAllDtoList(List<BeerGetAllDto> list) {
        for (int i = 0; i < list.size(); i++) {
            BeerGetAllDto beer = list.get(i);
            beer.setRating(getBeerRating(beer.getId()).get(0));
        }
        return list;
    }

    @Override
    public List<BeerGetAllDto> getTopThreeBeers(){
        return sortByRatingDesc().stream().limit(3).collect(Collectors.toList());
    }
}
