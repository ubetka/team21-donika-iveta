package com.beerapplication.repositories;

import com.beerapplication.models.Brewery;

import java.util.List;

public interface BreweriesRepository {

    Brewery create(Brewery brewery);

    List<Brewery> getAll();

    boolean checkIfBreweryExists(String name);

    Brewery getById(int id);

    Brewery update(int id, Brewery brewery);

    Brewery delete(int id);

}
