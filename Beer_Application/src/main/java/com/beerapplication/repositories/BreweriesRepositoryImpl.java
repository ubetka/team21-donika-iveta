package com.beerapplication.repositories;

import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.beerapplication.utils.Constants.BREWERY_NOT_FOUND;

@Repository
public class BreweriesRepositoryImpl implements BreweriesRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BreweriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session
                    .createQuery("from Brewery where enabled = true order by id", Brewery.class);
            return query.list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null || !brewery.isEnabled()) {
                throw new EntityNotFoundException(String.format(BREWERY_NOT_FOUND, id));
            }
            return brewery;
        }
    }

    @Override
    public Brewery create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
        return brewery;
    }


    @Override
    public boolean checkIfBreweryExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session
                    .createQuery("from Brewery where name like :name order by breweryId", Brewery.class);
            query.setParameter("name", name);
           return query.list().size() != 0;
        }
    }

    @Override
    public Brewery update(int id, Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Brewery breweryToUpdate = getById(id);
            breweryToUpdate.setName(brewery.getName());
            session.update(breweryToUpdate);
            session.getTransaction().commit();
            return breweryToUpdate;
        }
    }

    @Override
    public Brewery delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Brewery breweryToDelete = getById(id);
            breweryToDelete.setEnabled(false);
            session.update(breweryToDelete);
            session.getTransaction().commit();
            return breweryToDelete;
        }
    }
}
