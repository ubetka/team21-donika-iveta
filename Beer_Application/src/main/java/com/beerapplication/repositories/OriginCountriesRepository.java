package com.beerapplication.repositories;

import com.beerapplication.models.OriginCountry;

import java.util.List;

public interface OriginCountriesRepository {

    OriginCountry create(OriginCountry originCountry);

    List<OriginCountry> getAll();

    OriginCountry getById(int id);

    boolean checkIfOriginCountryExists(String name);

    OriginCountry update(int id, OriginCountry originCountry);

    OriginCountry delete(int id);

}
