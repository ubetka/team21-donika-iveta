package com.beerapplication.repositories;

import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.OriginCountry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.beerapplication.utils.Constants.ORIGIN_COUNTRY_NOT_FOUND;

@Repository
public class OriginCountriesRepositoryImpl implements OriginCountriesRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public OriginCountriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public OriginCountry create(OriginCountry originCountry) {
        try (Session session = sessionFactory.openSession()) {
            session.save(originCountry);
        }
        return originCountry;
    }

    @Override
    public List<OriginCountry> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<OriginCountry> query = session
                    .createQuery("from OriginCountry where enabled = true order by id", OriginCountry.class);
            return query.list();
        }
    }

    @Override
    public OriginCountry getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            OriginCountry originCountry = session.get(OriginCountry.class, id);
            if (originCountry == null || !originCountry.isEnabled()) {
                throw new EntityNotFoundException(String.format(ORIGIN_COUNTRY_NOT_FOUND, id));
            }
            return originCountry;
        }
    }

    @Override
    public boolean checkIfOriginCountryExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<OriginCountry> query = session
                    .createQuery("from OriginCountry where name like :name order by originCountryId", OriginCountry.class);
            query.setParameter("name", name);
            return query.list().size() != 0;
        }
    }

    @Override
    public OriginCountry update(int id, OriginCountry originCountry) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            OriginCountry originCountryToUpdate = getById(id);
            originCountryToUpdate.setName(originCountry.getName());
            session.update(originCountryToUpdate);
            session.getTransaction().commit();
            return originCountryToUpdate;
        }
    }

    @Override
    public OriginCountry delete(int id) {
       try (Session session = sessionFactory.openSession()){
           session.beginTransaction();
           OriginCountry originCountryToDelete = getById(id);
           originCountryToDelete.setEnabled(false);
           session.update(originCountryToDelete);
           session.getTransaction().commit();
           return originCountryToDelete;
       }
    }
}
