package com.beerapplication.repositories;

import com.beerapplication.models.Style;

import java.util.List;

public interface StylesRepository {
    Style create(Style style);

    List<Style> getAll();

    Style getById(int id);

    boolean checkIfStyleExists(String name);

    Style update(int id, Style style);

    Style delete(int id);
}
