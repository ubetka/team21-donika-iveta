package com.beerapplication.repositories;

import com.beerapplication.models.Style;
import com.beerapplication.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.beerapplication.utils.Constants.STYLE_NOT_FOUND;

@Repository
public class StylesRepositoryImpl implements StylesRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StylesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Style create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.save(style);
        }
        return style;
    }

    @Override
    public List<Style> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where enabled = true order by styleId", Style.class);
            return query.list();
        }
    }

    @Override
    public Style getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null || !style.isEnabled()) {
                throw new EntityNotFoundException(String.format(STYLE_NOT_FOUND, id));
            }
            return style;
        }
    }

    @Override
    public boolean checkIfStyleExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session
                    .createQuery("from Style where name like :name order by styleId", Style.class);
            query.setParameter("name", name);
            return query.list().size() != 0;
        }
    }

    @Override
    public Style update(int id, Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Style styleToUpdate = getById(id);
            styleToUpdate.setName(style.getName());
            session.update(styleToUpdate);
            session.getTransaction().commit();
            return styleToUpdate;
        }
    }

    @Override
    public Style delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Style styleToDelete = getById(id);
            styleToDelete.setEnabled(false);
            session.update(styleToDelete);
            session.getTransaction().commit();
            return styleToDelete;
        }
    }
}
