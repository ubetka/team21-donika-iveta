package com.beerapplication.repositories;

import com.beerapplication.models.Tag;

import java.util.List;

public interface TagsRepository {
    Tag create(Tag tag);

    List<Tag> getAll();

    Tag getTagByName(String name);

    List<Tag> filterByName(String name);

    boolean checkIfTagExists(String name);

    Tag getById(int id);

    Tag update(int id, Tag tag);

    Tag delete(int id);
}
