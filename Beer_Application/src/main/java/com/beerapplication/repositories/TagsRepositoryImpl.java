package com.beerapplication.repositories;

import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.beerapplication.utils.Constants.TAG_NAME_NOT_FOUND;
import static com.beerapplication.utils.Constants.TAG_NOT_FOUND;

@Repository
public class TagsRepositoryImpl implements TagsRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Tag create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
        return tag;
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where enabled = true order by id", Tag.class);
            return query.list();
        }
    }

    @Override
    public Tag getTagByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name like :name", Tag.class).
                    setParameter("name", "%" + name + "%");
            if (query.list().size() == 0) {
                throw new EntityNotFoundException(String.format(TAG_NAME_NOT_FOUND, name));
            }
            return query.list().get(0);
        }
    }

    public List<Tag> filterByName(String name){
        try(Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery(
                    "from Tag where enabled = true and name like :name order by id", Tag.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public boolean checkIfTagExists(String name) {
        return getByName(name).size() != 0;
    }

    @Override
    public Tag getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null || !tag.isEnabled()) {
                throw new EntityNotFoundException(String.format(TAG_NOT_FOUND, id));
            }
            return tag;
        }
    }

    private List<Tag> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name like :name order by id", Tag.class);
            query.setParameter("name", name);
            return query.list();
        }
    }

    @Override
    public Tag update(int id, Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Tag tagToUpdate = getById(id);
            tagToUpdate.setName(tag.getName());
            session.update(tagToUpdate);
            session.getTransaction().commit();
            return tagToUpdate;
        }
    }

    @Override
    public Tag delete(int id) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Tag tagToDelete = getById(id);
            tagToDelete.setEnabled(false);
            session.update(tagToDelete);
            session.getTransaction().commit();
            return tagToDelete;
        }
    }
}
