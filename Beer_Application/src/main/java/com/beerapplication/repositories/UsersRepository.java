package com.beerapplication.repositories;

import com.beerapplication.models.Beer;
import com.beerapplication.models.User;
import com.beerapplication.models.dtoes.UserGetAllDto;

import java.util.List;
import java.util.Set;

public interface UsersRepository {
    User create(User user);

    List<UserGetAllDto> getAll();

    User getById(int userId);

    List<User> getByUsername(String username);

    List<User> getByEmail(String email);

    boolean checkIfUsernameExists(String username);

    boolean checkIfEmailExists(String email);

    User update(int userId, User user);

    User delete(int userId);

    Set<Beer> getUserWishList(int id);

    Set<Beer> addBeerToWishList(int userId, int beerId);

    Set<Beer> getUserDrankList(int id);

    Set<Beer> addBeerToDrankList(int userId, int beerId);

    void addPicture(int userId, String pictureUrl);

    void removePicture(int userId);
}
