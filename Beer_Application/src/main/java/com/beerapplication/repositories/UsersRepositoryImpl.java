package com.beerapplication.repositories;

import com.beerapplication.exceptions.EntityNotFoundException;
import com.beerapplication.models.Beer;
import com.beerapplication.models.User;
import com.beerapplication.models.dtoes.UserGetAllDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

import static com.beerapplication.utils.Constants.USER_NOT_FOUND;

@Repository
public class UsersRepositoryImpl implements UsersRepository {

    private SessionFactory sessionFactory;
    private BeersRepository beersRepository;

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory, BeersRepository beersRepository) {
        this.sessionFactory = sessionFactory;
        this.beersRepository = beersRepository;
    }

    @Override
    public User create(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
        return user;
    }

    @Override
    public List<UserGetAllDto> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<UserGetAllDto> query = session.createQuery(
                "select distinct new com.beerapplication.models.dtoes.UserGetAllDto " +
                           "(userId, username, picture) from User where enabled = true",
                            UserGetAllDto.class);
        return query.list();
    }

    @Override
    public boolean checkIfUsernameExists(String username) {
        return getByUsername(username).size() != 0;
    }

    @Override
    public boolean checkIfEmailExists(String email) {
        return getByEmail(email).size() != 0;
    }

    @Override
    public User getById(int userId) {
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("from User where id = :id and enabled = true", User.class).
        setParameter("id", userId);
        User user = query.list().get(0);
//        User user = session.get(User.class, userId);
        if (user == null) {
            throw new EntityNotFoundException(String.format(USER_NOT_FOUND, userId));
        }
        return user;
    }

    @Override
    public User update(int userId, User user) {
        Session session = sessionFactory.getCurrentSession();

        session.beginTransaction();
        User userToUpdate = getById(userId);

        if (user.getPassword() != null) {
            userToUpdate.setPassword(user.getPassword());
        }

        if (user.getEmail() != null) {
            userToUpdate.setEmail(user.getEmail());
        }

        if (user.getFirstName() != null) {
            userToUpdate.setFirstName(user.getFirstName());
        }

        if (user.getLastName() != null) {
            userToUpdate.setLastName(user.getLastName());
        }

        if (user.getPicture() != null) {
            userToUpdate.setPicture(user.getPicture());
        }

        session.update(userToUpdate);
        session.getTransaction().commit();
        return userToUpdate;
    }

    @Override
    public User delete(int userId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        User userToDelete = getById(userId);
//        userToDelete.setDeleted(true);
        userToDelete.setEnabled(false);
        session.update(userToDelete);
        session.getTransaction().commit();
        return userToDelete;
    }

    public List<User> getByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
            Query<User> query = session.createQuery(
                    "from User where username = :username", User.class);
            query.setParameter("username", username);
            return query.list();
    }

    public List<User> getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email " +
                    "and enabled = true order by userId", User.class);
            query.setParameter("email", email);
            return query.list();
        }
    }

    public Set<Beer> getUserWishList(int id){
        Session session = sessionFactory.getCurrentSession();
        User user = getById(id);
        return user.getWishList();
    }

    @Override
    public Set<Beer> addBeerToWishList(int userId, int beerId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        User user = getById(userId);
        Beer beer = beersRepository.getById(beerId);
        Set<Beer> userWishList = getUserWishList(userId);
        userWishList.add(beer);
        session.update(user);
        session.getTransaction().commit();
        return userWishList;
    }

    @Override
    public Set<Beer> getUserDrankList(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = getById(id);
        return user.getDrankList();
    }

    @Override
    public Set<Beer> addBeerToDrankList(int userId, int beerId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        User user = getById(userId);
        Beer beer = beersRepository.getById(beerId);
        Set<Beer> userDrankList = getUserDrankList(userId);
        userDrankList.add(beer);
        session.update(user);
        session.getTransaction().commit();
        return userDrankList;
    }

    @Override
    public void addPicture(int userId, String pictureUrl) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        User user = getById(userId);
        user.setPicture(pictureUrl);
        session.save(user);
        session.getTransaction().commit();
    }

    @Override
    public void removePicture(int userId) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        User user = getById(userId);
        user.setPicture(null);
        session.save(user);
        session.getTransaction().commit();
    }
}
