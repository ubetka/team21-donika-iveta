package com.beerapplication.services;

import com.beerapplication.models.Beer;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.Rating;
import com.beerapplication.models.dtoes.SingleBeerDto;

import java.util.List;

public interface BeersService {

    Beer create(Beer beer);

    List<BeerGetAllDto> getAll();

//    List<Beer> filterByName(String beerName);

    List<BeerGetAllDto> getAll(String style, String originCountry, String brewery, String name);

    List<BeerGetAllDto> sort(String orderBy);

    Beer getById(int id);

    Beer update(int id, Beer beer);

    Beer delete(int id);

    Double getBeerRating(int id);

    List<Rating> getListOfRatings(int id);

    Double addBeerRating(int id, Rating rating);

    void addPicture(int beerId, String pictureUrl);

    void removePicture(int beerId);

    List<BeerGetAllDto> getTopThreeBeers();
}
