package com.beerapplication.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.*;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.repositories.BeersRepository;
import org.omg.CORBA.BAD_PARAM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.beerapplication.utils.Constants.*;

@Service
public class BeersServiceImpl implements BeersService {
    private BeersRepository repository;
    private DtoMapper mapper;

    @Autowired
    public BeersServiceImpl(BeersRepository repository,
                            DtoMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Beer create(Beer beer) {
        if (repository.checkIfBeerExists(beer.getName())) {
            throw new DuplicateEntityException(
                    String.format(BEER_ALREADY_EXISTS, beer.getName()));
        }
        return repository.create(beer);
    }

    @Override
    public List<BeerGetAllDto> getAll() {
        return repository.getAll();
    }

    @Override
    public List<BeerGetAllDto> getAll(String style, String originCountry, String tag, String name) {
        List<BeerGetAllDto> beers = new ArrayList<>();
        List<Beer> beerList = new ArrayList<>();

        if (style.isEmpty() && originCountry.isEmpty() && tag.isEmpty() && name.isEmpty()) {
            return repository.getAll();
        }
        if (!style.isEmpty()) {
            beerList = repository.filterByStyle(style);
        }
        if (!originCountry.isEmpty()) {
            beerList = repository.filterByCountry(originCountry);
        }
        if (!tag.isEmpty()) {
            beerList = repository.filterByTag(tag);
        }
        if (!name.isEmpty()) {
            beerList = repository.filterByName(name);
        }

        for (Beer b : beerList) {
            beers.add(mapper.fromBeerToBeerGetAllDto(b));
        }
        return beers;
    }


    @Override
    public List<BeerGetAllDto> sort(String orderBy) {
        if (orderBy.isEmpty()) {
            return repository.getAll();
        }
        switch (orderBy.toLowerCase()) {
            case "name":
                return repository.sortByName();
            case "abvasc":
                return repository.sortByAbvAsc();
            case "abvdesc":
                return repository.sortByAbvDesc();
            case "ratingasc":
                return repository.sortByRatingAsc();
            case "ratingdesc":
                return repository.sortByRatingDesc();
            default:
                throw new BAD_PARAM(String.format(INVALID_SORTING_CRITERIA, orderBy));
        }
    }

    @Override
    public Beer getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Beer update(int id, Beer beer) {
        if (repository.checkIfBeerExists(beer.getName())) {
            if (!(beer.getBeerId() == repository.getBeerByName(beer.getName()).get(0).getBeerId())) {
                throw new DuplicateEntityException(
                        String.format(BEER_ALREADY_EXISTS, beer.getName()));
            }
        }
        return repository.update(id, beer);
    }

    @Override
    public Beer delete(int id) {
        return repository.delete(id);
    }

    @Override
    public Double getBeerRating(int id) {
        List<Double> rating = repository.getBeerRating(id);
        return rating.get(0);
    }

    @Override
    public Double addBeerRating(int id, Rating rating) {
        repository.addBeerRating(id, rating);
        return getBeerRating(id);
    }

    public List<Rating> getListOfRatings(int id){
        return repository.getListOfRatings(id);
    }

    @Override
    public List<BeerGetAllDto> getTopThreeBeers() {
        return repository.getTopThreeBeers();
    }

    @Override
    public void addPicture(int beerId, String pictureUrl) {
        repository.addPicture(beerId, pictureUrl);
    }

    @Override
    public void removePicture(int beerId) {
        repository.removePicture(beerId);
    }
}
