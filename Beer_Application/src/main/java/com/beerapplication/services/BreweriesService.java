package com.beerapplication.services;

import com.beerapplication.models.Brewery;

import java.util.List;

public interface BreweriesService {

    Brewery create(Brewery brewery);

    List<Brewery> getAll();

    Brewery getById(int id);

    Brewery update(int id, Brewery brewery);

    Brewery delete(int id);
}
