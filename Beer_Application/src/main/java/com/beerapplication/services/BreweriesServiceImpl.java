package com.beerapplication.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.Brewery;
import com.beerapplication.repositories.BreweriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.beerapplication.utils.Constants.BREWERY_ALREADY_EXISTS;

@Service
public class BreweriesServiceImpl implements BreweriesService {

    private BreweriesRepository repository;

    @Autowired
    public BreweriesServiceImpl(BreweriesRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Brewery> getAll() {
        return repository.getAll();
    }

    @Override
    public Brewery getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Brewery create(Brewery brewery) {
        if (repository.checkIfBreweryExists(brewery.getName())){
            throw new DuplicateEntityException(String.format(BREWERY_ALREADY_EXISTS, brewery.getName()));
        }
        return repository.create(brewery);
    }

    @Override
    public Brewery update(int id, Brewery brewery) {
        if (repository.checkIfBreweryExists(brewery.getName())){
            throw new DuplicateEntityException(String.format(BREWERY_ALREADY_EXISTS, brewery.getName()));
        }
        return repository.update(id, brewery);
    }

    @Override
    public Brewery delete(int id) {
       return repository.delete(id);
    }
}
