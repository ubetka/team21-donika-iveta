package com.beerapplication.services;

import com.beerapplication.models.OriginCountry;

import java.util.List;

public interface OriginCountriesService {

    OriginCountry create(OriginCountry originCountry);

    List<OriginCountry> getAll();

    OriginCountry getById(int id);

    OriginCountry update(int id, OriginCountry originCountry);

    OriginCountry delete(int id);
}
