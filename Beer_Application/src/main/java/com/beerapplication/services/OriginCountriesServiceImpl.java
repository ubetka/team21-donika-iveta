package com.beerapplication.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.OriginCountry;
import com.beerapplication.repositories.OriginCountriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.beerapplication.utils.Constants.*;

@Service
public class OriginCountriesServiceImpl implements OriginCountriesService {

    private OriginCountriesRepository repository;

    @Autowired
    public OriginCountriesServiceImpl(OriginCountriesRepository repository) {
        this.repository = repository;
    }

    @Override
    public OriginCountry create(OriginCountry originCountry) {
        if (repository.checkIfOriginCountryExists(originCountry.getName())){
            throw new DuplicateEntityException(
                    String.format(ORIGIN_COUNTRY_ALREADY_EXISTS, originCountry.getName()));
        }
        return repository.create(originCountry);
    }

    @Override
    public List<OriginCountry> getAll() {
        return repository.getAll();
    }

    @Override
    public OriginCountry getById(int id) {
        return repository.getById(id);
    }

    @Override
    public OriginCountry update(int id, OriginCountry originCountry) {
        if (repository.checkIfOriginCountryExists(originCountry.getName())){
            throw new DuplicateEntityException(String.format(ORIGIN_COUNTRY_ALREADY_EXISTS, originCountry.getName()));
        }
        return repository.update(id, originCountry);
    }

    @Override
    public OriginCountry delete(int id) {
        return repository.delete(id);
    }
}
