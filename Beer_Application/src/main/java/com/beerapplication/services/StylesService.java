package com.beerapplication.services;

import com.beerapplication.models.Style;

import java.util.List;

public interface StylesService {

    Style create(Style style);

    List<Style> getAll();

    Style getById(int id);

    Style update(int id, Style style);

    Style delete(int id);
}
