package com.beerapplication.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.Style;
import com.beerapplication.repositories.StylesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.beerapplication.utils.Constants.STYLE_ALREADY_EXISTS;
import static com.beerapplication.utils.Constants.TAG_ALREADY_EXISTS;

@Service
public class StylesServiceImpl implements StylesService {

    private StylesRepository repository;

    @Autowired
    public StylesServiceImpl(StylesRepository repository) {
        this.repository = repository;
    }

    @Override
    public Style create(Style style) {
        if (repository.checkIfStyleExists(style.getName())){
            throw new DuplicateEntityException(String.format("Style with name '%s' already exists", style.getName()));
        }
        return repository.create(style);
    }

    @Override
    public List<Style> getAll() {
        return repository.getAll();
    }

    @Override
    public Style getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Style update(int id, Style style) {
        if (repository.checkIfStyleExists(style.getName())){
            throw new DuplicateEntityException(String.format(STYLE_ALREADY_EXISTS, style.getName()));
        }
        return repository.update(id, style);
    }
//        if (repository.checkIfStyleExists(style.getName())){
//            if (repository.getById(id).getName().equalsIgnoreCase(style.getName())){
//                return repository.update(id, style);
//            }else{
//                throw new DuplicateEntityException(String.format("Style with name '%s' already exists", style.getName()));
//            }
//        }
//        return repository.update(id, style);


    @Override
    public Style delete(int id) {
//        Style styleToDelete = repository.getById(id);
       return repository.delete(id);
//        return styleToDelete;
    }
}
