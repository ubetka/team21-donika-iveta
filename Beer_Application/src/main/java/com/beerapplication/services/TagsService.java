package com.beerapplication.services;

import com.beerapplication.models.Tag;

import java.util.List;

public interface TagsService {
    Tag create(Tag tag);

    List<Tag> getAll();

    Tag getTagByName(String name);

    List<Tag> filterByName(String name);

    Tag getById(int id);

    Tag update(int id, Tag tag);

    Tag delete(int id);
}
