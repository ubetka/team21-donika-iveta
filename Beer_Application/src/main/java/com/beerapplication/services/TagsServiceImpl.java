package com.beerapplication.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.Tag;
import com.beerapplication.repositories.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.beerapplication.utils.Constants.TAG_ALREADY_EXISTS;

@Service
public class TagsServiceImpl implements TagsService {

    private TagsRepository repository;

    @Autowired
    public TagsServiceImpl(TagsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Tag create(Tag tag) {
        if (repository.checkIfTagExists(tag.getName())){
            throw new DuplicateEntityException(String.format(TAG_ALREADY_EXISTS, tag.getName()));
        }
        return repository.create(tag);
    }

    @Override
    public List<Tag> getAll() {
        return repository.getAll();
    }

    @Override
    public Tag getTagByName(String name) {
        return repository.getTagByName(name);
    }

    @Override
    public List<Tag> filterByName(String name){
        return repository.filterByName(name);
    }

    @Override
    public Tag getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Tag update(int id, Tag tag) {
        if (repository.checkIfTagExists(tag.getName())){
            throw new DuplicateEntityException(String.format(TAG_ALREADY_EXISTS, tag.getName()));
        }
        return repository.update(id, tag);
    }

    @Override
    public Tag delete(int id) {
        return repository.delete(id);
    }
}
