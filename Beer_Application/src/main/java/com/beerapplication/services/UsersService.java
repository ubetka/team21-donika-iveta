package com.beerapplication.services;

import com.beerapplication.models.Tag;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.User;
import com.beerapplication.models.dtoes.UserGetAllDto;

import java.util.List;
import java.util.Set;

public interface UsersService {
    User create(User user);

    List<UserGetAllDto> getAll();

    User getById(int userId);

    List<User> getByUsername(String username);

    User update(int userId, User user);

    User delete(int userId);

    Set<BeerGetAllDto> getUserWishList(int id);

    Set<BeerGetAllDto> addBeerToWishList(int userId, int beerId);

    Set<BeerGetAllDto> getUserDrankList(int id);

    Set<BeerGetAllDto> addBeerToDrankList(int userId, int beerId);

    void addPicture(int userId, String pictureUrl);

    void removePicture(int userId);
}
