package com.beerapplication.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.*;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.dtoes.DtoMapper;
import com.beerapplication.models.dtoes.UserGetAllDto;
import com.beerapplication.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import static com.beerapplication.utils.Constants.EMAIL_ALREADY_EXISTS;
import static com.beerapplication.utils.Constants.USERNAME_ALREADY_EXISTS;

@Service
public class UsersServiceImpl implements UsersService {
    private UsersRepository repository;
    private DtoMapper mapper;

    @Autowired
    public UsersServiceImpl(UsersRepository repository, DtoMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public User create(User user) {
        if (repository.checkIfUsernameExists(user.getUsername())){
            throw new DuplicateEntityException(String.format(USERNAME_ALREADY_EXISTS, user.getUsername()));
        }
        if (repository.checkIfEmailExists(user.getEmail())){
            throw new DuplicateEntityException(String.format(EMAIL_ALREADY_EXISTS, user.getEmail()));
        }
        return repository.create(user);
    }

    @Override
    public List<UserGetAllDto> getAll() {
        return repository.getAll();
    }

    @Override
    public User getById(int userId) {
        return repository.getById(userId);
    }

    @Override
    public List<User> getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public User update(int userId, User user) {
        if (repository.checkIfEmailExists(user.getEmail())) {
            if (!(user.getUserId() == repository.getByEmail(user.getEmail()).get(0).getUserId())) {
                throw new DuplicateEntityException(String.format(EMAIL_ALREADY_EXISTS, user.getEmail()));
            }
        }
        if (repository.checkIfUsernameExists(user.getUsername())) {
            if (!(user.getUserId() == repository.getByUsername(user.getUsername()).get(0).getUserId())) {
                throw new DuplicateEntityException(String.format(USERNAME_ALREADY_EXISTS, user.getUsername()));
            }
        }
        return repository.update(userId, user);
    }

    @Override
    public User delete(int userId) {
        return repository.delete(userId);
    }

    @Override
    public Set<BeerGetAllDto> getUserWishList(int id) {
        Set<Beer> set = repository.getUserWishList(id);
        return mapper.fillSetOfBeersIntoBeerGetAllDto(set);
    }

    @Override
    public Set<BeerGetAllDto> addBeerToWishList(int userId, int beerId) {
        Set<Beer> set = repository.addBeerToWishList(userId, beerId);
        return mapper.fillSetOfBeersIntoBeerGetAllDto(set);
    }

    @Override
    public Set<BeerGetAllDto> getUserDrankList(int id) {
        Set<Beer> set = repository.getUserDrankList(id);
        return mapper.fillSetOfBeersIntoBeerGetAllDto(set);
    }

    @Override
    public Set<BeerGetAllDto> addBeerToDrankList(int userId, int beerId) {
        Set <Beer> set = repository.addBeerToDrankList(userId, beerId);
        return mapper.fillSetOfBeersIntoBeerGetAllDto(set);
    }

    @Override
    public void addPicture(int userId, String pictureUrl) {
        repository.addPicture(userId, pictureUrl);
    }

    @Override
    public void removePicture(int userId) {
        repository.removePicture(userId);
    }
}
