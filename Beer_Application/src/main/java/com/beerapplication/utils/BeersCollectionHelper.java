package com.beerapplication.utils;

import com.beerapplication.models.*;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.dtoes.SingleBeerDto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BeersCollectionHelper {

    public static List<Brewery> filterBreweryByName(List<Brewery> list, String name){
        if (!name.isEmpty()){
            list = list
                    .stream()
                    .filter(brewery -> brewery.getName().toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return list;
    }

    public static List<Style> filterStyleByName(List<Style> list, String name){
        if (!name.isEmpty()){
            list = list
                    .stream()
                    .filter(style -> style.getName().toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return list;
    }

    public static List<OriginCountry> filterOriginCountryByName(List<OriginCountry> list, String name){
        if (!name.isEmpty()){
            list = list
                    .stream()
                    .filter(originCountry -> originCountry.getName().toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return list;
    }

    public static Beer updateParameters(Beer beerToUpdate, Beer beer){
        if (beer.getName() != null) {
            beerToUpdate.setName(beer.getName());
        }
        if (beer.getAbv() != null) {
            beerToUpdate.setAbv(beer.getAbv());
        }
        if (beer.getDescription() != null) {
            beerToUpdate.setDescription(beer.getDescription());
        }
        if (beer.getStyle() != null) {
            beerToUpdate.setStyle(beer.getStyle());
        }
        if (beer.getBrewery() != null) {
            beerToUpdate.setBrewery(beer.getBrewery());
        }
        if (beer.getOriginCountry() != null) {
            beerToUpdate.setOriginCountry(beer.getOriginCountry());
        }
//        if (beer.getTags() != null) {
//            beerToUpdate.setTags(beer.getTags());
//        }
        return beerToUpdate;
    }

    public static List<BeerGetAllDto> fillListOfBeersIntoBeerGetAllDto(List<Beer> beers) {
        List<BeerGetAllDto> userListDtos = new ArrayList<>();
        for (Beer beer : beers) {
            userListDtos.add(new BeerGetAllDto(
                    beer.getBeerId(), beer.getName(), beer.getAbv(), beer.getStyle().getName()));
        }
        return userListDtos;
    }
}
