package com.beerapplication.utils;

public class Constants {

    //Beer//

    public static final int MIN_BEER_NAME_LENGTH = 2;
    public static final int MAX_BEER_NAME_LENGTH = 50;
    public static final String BEER_NAME_ERROR_MESSAGE =
                "Beer's name length should be between {min} and {max} characters long.";
    public static final String BEER_NOT_FOUND = "Beer with id '%d' does not exists.";
    public static final String BEER_NAME_NOT_FOUND = "Beer with name '%s' does not exists.";
    public static final String BEER_ALREADY_EXISTS = "Beer with name '%s' already exists.";
    public static final String BEER_ALREADY_RATED = "User with id '%d' has already rated beer with id '%d'.";

    //Sort
    public static final String INVALID_SORTING_CRITERIA = "Invalid sorting criteria - '%s'.";

    //Style
    public static final int MIN_STYLE_NAME_LENGTH = 3;
    public static final int MAX_STYLE_NAME_LENGTH = 50;
    public static final String STYLE_NAME_ERROR_MESSAGE =
            "Style's name length should be between {min} and {max} characters long.";
    public static final String STYLE_NOT_FOUND = "Style with id '%d' does not exists.";
    public static final String STYLE_ALREADY_EXISTS = "Style with name '%s' already exists.";

    //Brewery
    public static final int MIN_BREWERY_NAME_LENGTH = 3;
    public static final int MAX_BREWERY_NAME_LENGTH = 50;
    public static final String BREWERY_NAME_ERROR_MESSAGE =
            "Brewery's name length should be between {min} and {max} characters long.";
    public static final String BREWERY_NOT_FOUND = "Brewery with id '%d' does not exists.";
    public static final String BREWERY_ALREADY_EXISTS = "Brewery with name '%s' already exists.";

    //Origin country
    public static final int MIN_ORIGIN_COUNTRY_NAME_LENGTH = 4;
    public static final int MAX_ORIGIN_COUNTRY_NAME_LENGTH = 50;
    public static final String ORIGIN_COUNTRY_NAME_ERROR_MESSAGE =
            "Origin country's name length should be between {min} and {max} characters long.";
    public static final String ORIGIN_COUNTRY_NOT_FOUND = "Origin country with id '%d' does not exists.";
    public static final String ORIGIN_COUNTRY_ALREADY_EXISTS = "Origin country with name '%s' already exists.";

    //Description
    public static final int MIN_DESCRIPTION_NAME_LENGTH = 20;
    public static final int MAX_DESCRIPTION_NAME_LENGTH = 500;
    public static final String DESCRIPTION_ERROR_MESSAGE =
            "Description should be between {min} and {max} characters long.";

    //Tag
    public static final int MIN_TAG_NAME_LENGTH = 3;
    public static final int MAX_TAG_NAME_LENGTH = 50;
    public static final String TAG_ERROR_MESSAGE =
            "Tag should be between {min} and {max} characters long.";
    public static final String TAG_NOT_FOUND = "Tag with id '%d' does not exists.";
    public static final String TAG_NAME_NOT_FOUND = "Tag with name '%s' does not exists.";
    public static final String TAG_ALREADY_EXISTS = "Tag with name '%s' already exists.";

    //Picture
    public static final String PICTURE_ADDED_SUCCESS_MESSAGE = "Picture added successfully to %s with id '%d'.";
    public static final String PICTURE_REMOVED_SUCCESS_MESSAGE = "Picture removed successfully from %s with id '%d'.";

    //User//

    public static final String USER_ALREDY_RATED_BEER = "User with username '%s' has already rated beer '%s'.";

    //Username
    public static final int MIN_USERNAME_LENGTH = 3;
    public static final int MAX_USERNAME_LENGTH = 30;
    public static final String USERNAME_ERROR_MESSAGE = "Username should be between {min} and {max} characters long.";
    public static final String USERNAME_ALREADY_EXISTS = "Username '%s' already exists.";
    public static final String USER_NOT_FOUND = "User with id '%d' does not exist.";

    //Password
    public static final int MIN_PASSWORD_LENGTH = 6;
    public static final int MAX_PASSWORD_LENGTH = 30;
    public static final String PASSWORD_ERROR_MESSAGE = "Password should be between {min} and {max} characters long.";

    //Mail
    public static final int MIN_EMAIL_LENGTH = 8;
    public static final int MAX_EMAIL_LENGTH = 50;
    public static final String USER_EMAIL_ERROR_MESSAGE = "E-mail should be between {min} and {max} characters long.";
    public static final String EMAIL_ALREADY_EXISTS = "Email '%s' already exists.";

    //FirstName
    public static final int MIN_USER_FIRST_NAME_LENGTH = 3;
    public static final int MAX_USER_FIRST_NAME_LENGTH = 30;
    public static final String USER_FIRST_NAME_ERROR_MESSAGE = "User's first name should be between {min} and {max} characters long.";

    //LastName
    public static final int MIN_USER_LAST_NAME_LENGTH = 3;
    public static final int MAX_USER_LAST_NAME_LENGTH = 30;
    public static final String USER_LAST_NAME_ERROR_MESSAGE = "User's last name should be between {min} and {max} characters long.";
}
