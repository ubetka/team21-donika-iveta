package com.beerapplication.utils;

import com.beerapplication.models.User;
import com.beerapplication.models.dtoes.UserProfileDto;

public class UsersHelper {

    public static User updateParameters(User user, UserProfileDto userProfileDto){
            if (userProfileDto.getFirstName() != null) {
                user.setFirstName(userProfileDto.getFirstName());
            }
            if (userProfileDto.getLastName() != null){
                user.setLastName(userProfileDto.getLastName());
            }
            if (userProfileDto.getEmail() != null) {
                user.setEmail(userProfileDto.getEmail());
            }
            if (userProfileDto.getPicture() != null) {
                user.setPicture(userProfileDto.getPicture());
            }
            if (userProfileDto.getPassword() != null) {
                user.setPassword(userProfileDto.getPassword());
            }
            if (userProfileDto.getPasswordConfirmation() != null) {
                user.setPasswordConfirmation(userProfileDto.getPasswordConfirmation());
            }
        return user;
    }

}
