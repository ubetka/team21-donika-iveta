package com.teamproject.demo.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.Beer;

import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.repositories.BeersRepository;
import com.beerapplication.services.BeersServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.teamproject.demo.services.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BeersServiceImplTests {

    @Mock
    BeersRepository repository;

    @InjectMocks
    BeersServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnBeer_WhenBeerExists() {
        //Arrange
        Beer expectedBeer = createBeer();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Beer expectedBeer = createBeer();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedBeer);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository, times(1)).getById(anyInt());

    }

    @Test
    public void createBeerShould_ReturnNewBeer_IfDoesNotExists() {
        //Arrange
        Beer beer = new Beer();
        Beer expectedBeer = createBeer();
        Mockito.when(repository.create(beer)).thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = mockService.create(beer);

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);
    }


    @Test(expected = DuplicateEntityException.class)
    public void createBeerShould_Throw_IfAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkIfBeerExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.create(createBeer());
    }

    @Test
    public void getAllShould_returnAllBeers() {
        //Arrange
        List<BeerGetAllDto> listBeers = new ArrayList<>();
        listBeers.add(createBeerForList());
        listBeers.add(createBeerForList2());
        listBeers.add(createBeerForList3());
        Mockito.when(repository.getAll()).thenReturn(listBeers);

        //Act
        List<BeerGetAllDto> testList = mockService.getAll();

        //Assert
        Assert.assertSame(listBeers, testList);
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        List<BeerGetAllDto> listBeers = new ArrayList<>();
        listBeers.add(createBeerForList());
        listBeers.add(createBeerForList2());
        listBeers.add(createBeerForList3());
        Mockito.when(repository.getAll()).thenReturn(listBeers);

        //Act
        mockService.getAll();

        //Assert
        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void updateBeerShould_ReturnUpdatedBeer() {
        //Arrange
        Beer beer = new Beer();
        Beer expectedBeer = createBeer();
        Mockito.when(repository.update(beer.getBeerId(), beer)).thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = mockService.update(beer.getBeerId(), beer);

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);
    }


    @Test
    public void deleteBeerShould_ReturnDeletedBeer_IfExists() {
        //Arrange
        Beer beer = new Beer();
        Beer expectedBeer = createBeer();
        Mockito.when(repository.delete(beer.getBeerId())).thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = mockService.delete(beer.getBeerId());

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);
    }

    @Test
    public void SortByNameShould_ReturnSortedBeers_IfExists() {
        //Arrange
        BeerGetAllDto beer = new BeerGetAllDto();
        beer.setName("ariana");
        BeerGetAllDto beer2 = new BeerGetAllDto();
        beer2.setName("shumensko");
        List<BeerGetAllDto> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer2);
        Mockito.when(repository.sortByName()).thenReturn(beerList);
        //Act
        List<BeerGetAllDto> result = mockService.sort("name");
        //Assert
        Assert.assertEquals(beerList, result);
    }

    @Test
    public void SortByAbvAscShould_ReturnSortedBeers_IfExists() {
        //Arrange
        BeerGetAllDto beer = new BeerGetAllDto();
        beer.setAbv(10.2);
        BeerGetAllDto beer2 = new BeerGetAllDto();
        beer2.setAbv(1.1);
        List<BeerGetAllDto> beerList = new ArrayList<>();
        beerList.add(beer2);
        beerList.add(beer);
        Mockito.when(repository.sortByAbvAsc()).thenReturn(beerList);
        //Act
        List<BeerGetAllDto> result = mockService.sort("abvasc");
        //Assert
        Assert.assertEquals(beerList, result);
    }

    @Test
    public void SortByAbvDescShould_ReturnSortedBeers_IfExists() {
        //Arrange
        BeerGetAllDto beer = new BeerGetAllDto();
        beer.setAbv(10.2);
        BeerGetAllDto beer2 = new BeerGetAllDto();
        beer2.setAbv(1.1);
        List<BeerGetAllDto> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer2);
        Mockito.when(repository.sortByAbvDesc()).thenReturn(beerList);
        //Act
        List<BeerGetAllDto> result = mockService.sort("abvdesc");
        //Assert
        Assert.assertEquals(beerList, result);
    }

    @Test
    public void SortByRatingAscShould_ReturnSortedBeers_IfExists() {
        //Arrange
        BeerGetAllDto beer = new BeerGetAllDto();
        beer.setRating(4.5);
        BeerGetAllDto beer2 = new BeerGetAllDto();
        beer2.setAbv(1.1);
        List<BeerGetAllDto> beerList = new ArrayList<>();
        beerList.add(beer2);
        beerList.add(beer);
        Mockito.when(repository.sortByRatingAsc()).thenReturn(beerList);
        //Act
        List<BeerGetAllDto> result = mockService.sort("ratingasc");
        //Assert
        Assert.assertEquals(beerList, result);
    }

    @Test
    public void SortByRatingDescShould_ReturnSortedBeers_IfExists() {
        //Arrange
        BeerGetAllDto beer = new BeerGetAllDto();
        beer.setRating(4.5);
        BeerGetAllDto beer2 = new BeerGetAllDto();
        beer2.setRating(1.1);
        List<BeerGetAllDto> beerList = new ArrayList<>();
        beerList.add(beer);
        beerList.add(beer2);
        Mockito.when(repository.sortByRatingDesc()).thenReturn(beerList);
        //Act
        List<BeerGetAllDto> result = mockService.sort("ratingdesc");
        //Assert
        Assert.assertEquals(beerList, result);
    }

}


