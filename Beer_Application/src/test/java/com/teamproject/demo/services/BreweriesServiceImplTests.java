package com.teamproject.demo.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.Brewery;
import com.beerapplication.repositories.BreweriesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.teamproject.demo.services.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BreweriesServiceImplTests {
    @Mock
    BreweriesRepository repository;

    @InjectMocks
    com.beerapplication.services.BreweriesServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnBrewery_WhenBreweryExists() {
        //Arrange
        Brewery expectedBrewery = createBrewery();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedBrewery);

        //Act
        Brewery returnedBrewery = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedBrewery, returnedBrewery);
    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Brewery expectedBrewery = createBrewery();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedBrewery);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository, times(1)).getById(anyInt());
    }

    @Test
    public void createBreweryShould_ReturnNewBrewery_IfDoesNotExists() {
        //Arrange
        Brewery brewery = new Brewery();
        Brewery expectedBrewery = createBrewery();
        Mockito.when(repository.create(brewery)).thenReturn(expectedBrewery);

        //Act
        Brewery returnedBrewery = mockService.create(brewery);

        //Assert
        Assert.assertSame(expectedBrewery, returnedBrewery);
    }


    @Test(expected = DuplicateEntityException.class)
    public void createBreweryShould_Throw_IfAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkIfBreweryExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.create(createBrewery());
    }

    @Test
    public void getAllShould_returnAllBreweries() {
        //Arrange
        List<Brewery> listBreweries = new ArrayList<>();
        listBreweries.add(createBrewery());
        listBreweries.add(createBrewery2());
        listBreweries.add(createBrewery3());
        Mockito.when(repository.getAll()).thenReturn(listBreweries);

        //Act
        List<Brewery> testList = mockService.getAll();

        //Assert
        Assert.assertSame(listBreweries, testList);
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        List<Brewery> listBreweries = new ArrayList<>();
        listBreweries.add(createBrewery());
        listBreweries.add(createBrewery2());
        listBreweries.add(createBrewery3());
        Mockito.when(repository.getAll()).thenReturn(listBreweries);

        //Act
        mockService.getAll();

        // Assert
        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void updateBreweryShould_ReturnUpdatedBrewery() {
        //Arrange
        Brewery brewery = new Brewery();
        Brewery expectedBrewery = createBrewery();
        Mockito.when(repository.update(brewery.getBreweryId(), brewery)).thenReturn(expectedBrewery);

        //Act
        Brewery returnedBrewery = mockService.update(brewery.getBreweryId(), brewery);

        //Assert
        Assert.assertSame(expectedBrewery, returnedBrewery);
    }


    @Test(expected = DuplicateEntityException.class)
    public void updateBreweryShould_Throw_IfAlreadyNotExists() {
        //Arrange
        Brewery brewery = createBrewery();
        Mockito.when(repository.checkIfBreweryExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.update(brewery.getBreweryId(), brewery);
    }

    @Test
    public void deleteBreweryShould_ReturnDeletedBrewery_IfExists() {
        //Arrange
        Brewery brewery = new Brewery();
        Brewery expectedBrewery = createBrewery();
        Mockito.when(repository.delete(brewery.getBreweryId())).thenReturn(expectedBrewery);

        //Act
        Brewery returnedBrewery = mockService.delete(brewery.getBreweryId());

        //Assert
        Assert.assertSame(expectedBrewery, returnedBrewery);
    }


}
