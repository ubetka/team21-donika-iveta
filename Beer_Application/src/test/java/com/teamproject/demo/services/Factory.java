package com.teamproject.demo.services;


import com.beerapplication.models.*;
import com.beerapplication.models.dtoes.BeerGetAllDto;
import com.beerapplication.models.dtoes.UserGetAllDto;

public class Factory {
    // Beers
    public static Beer createBeer() {
        return new Beer("testbeer1", "test description1", 4.1);
    }
    public static Beer createBeer2() {
        return new Beer("testbeer2", "test description2", 4.2);
    }
    public static Beer createBeer3() {
        return new Beer("testbeer3", "test description3", 4.3);
    }

    // Beers
    public static BeerGetAllDto createBeerForList() {
        return new BeerGetAllDto(1, "testbeer1", 4.1, "teststyle1");
    }
    public static BeerGetAllDto createBeerForList2() {
        return new BeerGetAllDto(2, "testbeer2", 4.2, "teststyle2");
    }
    public static BeerGetAllDto createBeerForList3() {
        return new BeerGetAllDto(3, "testbeer3", 4.3, "teststyle3");
    }



    // Breweries
    public static Brewery createBrewery() {
        return new Brewery("testbrewery1");
    }
    public static Brewery createBrewery2() {
        return new Brewery("testbrewery2");
    }
    public static Brewery createBrewery3() {
        return new Brewery("testbrewery3");
    }

    // OriginCountries
    public static OriginCountry createOriginCountry() {
        return new OriginCountry("testorigincountry1");
    }
    public static OriginCountry createOriginCountry2() {
        return new OriginCountry("testorigincountry2");
    }
    public static OriginCountry createOriginCountry3() {
        return new OriginCountry("testorigincountry3");
    }

    // Styles
    public static Style createStyle() {
        return new Style("teststyle1");
    }
    public static Style createStyle2() {
        return new Style("teststyle2");
    }
    public static Style createStyle3() {
        return new Style("teststyle3");
    }

    // Tags
    public static Tag createTag() {
        return new Tag("testtag1");
    }
    public static Tag createTag2() {
        return new Tag("testtag2");
    }
    public static Tag createTag3() {
        return new Tag("testtag3");
    }

    // Users
    public static User createUser() { return new User(); }

    public static UserGetAllDto createUserForList() { return new UserGetAllDto(1, "testuser1", "testpicture1"); }
    public static UserGetAllDto createUserForList2() { return new UserGetAllDto(2, "testuser2", "testpicture2"); }
    public static UserGetAllDto createUserForList3() { return new UserGetAllDto(3, "testuser3", "testpicture3"); }
}
