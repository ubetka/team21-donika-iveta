package com.teamproject.demo.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.OriginCountry;
import com.beerapplication.repositories.OriginCountriesRepository;
import com.beerapplication.services.OriginCountriesServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.teamproject.demo.services.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OriginCountriesServiceImplTests {
    @Mock
    OriginCountriesRepository repository;

    @InjectMocks
    OriginCountriesServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnOriginCountry_WhenOriginCountyExists() {
        //Arrange
        OriginCountry expectedOriginCountry = createOriginCountry();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedOriginCountry);

        //Act
        OriginCountry returnedOriginCountry = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedOriginCountry, returnedOriginCountry);
    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        OriginCountry expectedOriginCountry = createOriginCountry();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedOriginCountry);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository, times(1)).getById(anyInt());
    }

    @Test
    public void createOriginCountryShould_ReturnNewOriginCountry_IfDoesNotExists() {
        //Arrange
        OriginCountry originCountry = new OriginCountry();
        OriginCountry expectedOriginCountry = createOriginCountry();
        Mockito.when(repository.create(originCountry)).thenReturn(expectedOriginCountry);

        //Act
        OriginCountry returnedOriginCountry = mockService.create(originCountry);

        //Assert
        Assert.assertSame(expectedOriginCountry, returnedOriginCountry);
    }


    @Test(expected = DuplicateEntityException.class)
    public void createOriginCountryShould_Throw_IfAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkIfOriginCountryExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.create(createOriginCountry());
    }

    @Test
    public void getAllShould_returnAllOriginCountries() {
        //Arrange
        List<OriginCountry> listOriginCountries = new ArrayList<>();
        listOriginCountries.add(createOriginCountry());
        listOriginCountries.add(createOriginCountry2());
        listOriginCountries.add(createOriginCountry3());
        Mockito.when(repository.getAll()).thenReturn(listOriginCountries);

        //Act
        List<OriginCountry> testList = mockService.getAll();

        //Assert
        Assert.assertSame(listOriginCountries, testList);
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        List<OriginCountry> listOriginCountries = new ArrayList<>();
        listOriginCountries.add(createOriginCountry());
        listOriginCountries.add(createOriginCountry2());
        listOriginCountries.add(createOriginCountry3());
        Mockito.when(repository.getAll()).thenReturn(listOriginCountries);

        //Act
        mockService.getAll();

        //Assert
        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void updateOriginCountryShould_ReturnUpdatedOriginCountry() {
        //Arrange
        OriginCountry originCountry = new OriginCountry();
        OriginCountry expectedOriginCountry = createOriginCountry();
        Mockito.when(repository.update(originCountry.getOriginCountryId(), originCountry))
                .thenReturn(expectedOriginCountry);

        //Act
        OriginCountry returnedOriginCoutry = mockService.update(originCountry.getOriginCountryId(), originCountry);

        //Assert
        Assert.assertSame(expectedOriginCountry, returnedOriginCoutry);
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateOriginCountryShould_Throw_IfAlreadyNotExists() {
        //Arrange
        OriginCountry originCountry = createOriginCountry();
        Mockito.when(repository.checkIfOriginCountryExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.update(originCountry.getOriginCountryId(), originCountry);
    }

    @Test
    public void deleteOriginCountryShould_ReturnDeletedTag_IfExists() {
        //Arrange
        OriginCountry originCountry = new OriginCountry();
        OriginCountry expectedOriginCountry = createOriginCountry();
        Mockito.when(repository.delete(originCountry.getOriginCountryId())).thenReturn(expectedOriginCountry);

        //Act
        OriginCountry returnedOriginCountry = mockService.delete(originCountry.getOriginCountryId());

        //Assert
        Assert.assertSame(expectedOriginCountry, returnedOriginCountry);
    }


}
