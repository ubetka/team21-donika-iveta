package com.teamproject.demo.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.Style;
import com.beerapplication.repositories.StylesRepository;
import com.beerapplication.services.StylesServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.teamproject.demo.services.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class StylesServiceImplTests {
    @Mock
    StylesRepository repository;

    @InjectMocks
    StylesServiceImpl mockService;
    @Test
    public void getByIdShould_ReturnStyle_WhenStyleExists() {
        //Arrange
        Style expectedStyle = createStyle();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedStyle);

        //Act
        Style returnedStyle = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedStyle, returnedStyle);
    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Style expectedStyle = createStyle();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedStyle);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository, times(1)).getById(anyInt());
    }

    @Test
    public void createStyleShould_ReturnNewStyle_IfDoesNotExists() {
        //Arrange
        Style style = new Style();
        Style expectedStyle = createStyle();
        Mockito.when(repository.create(style)).thenReturn(expectedStyle);

        //Act
        Style returnedStyle = mockService.create(style);

        //Assert
        Assert.assertSame(expectedStyle, returnedStyle);
    }


    @Test(expected = DuplicateEntityException.class)
    public void createStyleShould_Throw_IfAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkIfStyleExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.create(createStyle());
    }

    @Test
    public void getAllShould_returnAllStyles() {
        //Arrange
        List<Style> listStyles = new ArrayList<>();
        listStyles.add(createStyle());
        listStyles.add(createStyle2());
        listStyles.add(createStyle3());
        Mockito.when(repository.getAll()).thenReturn(listStyles);

        //Act
        List<Style> testList = mockService.getAll();

        //Assert
        Assert.assertSame(listStyles, testList);
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        List<Style> listStyles = new ArrayList<>();
        listStyles.add(createStyle());
        listStyles.add(createStyle2());
        listStyles.add(createStyle3());
        Mockito.when(repository.getAll()).thenReturn(listStyles);

        //Act
        mockService.getAll();

        //Assert
        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void updateStyleShould_ReturnUpdatedStyle() {
        //Arrange
        Style style = new Style();
        Style expectedStyle = createStyle();
        Mockito.when(repository.update(style.getStyleId(), style)).thenReturn(expectedStyle);

        //Act
        Style returnedStyle = mockService.update(style.getStyleId(), style);

        //Assert
        Assert.assertSame(expectedStyle, returnedStyle);
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateStyleShould_Throw_IfAlreadyNotExists() {
        //Arrange
        Style style = createStyle();
        Mockito.when(repository.checkIfStyleExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.update(style.getStyleId(), style);
    }

    @Test
    public void deleteStyleShould_ReturnDeletedStyle_IfExists() {
        Style style = new Style();
        Style expectedStyle = createStyle();
        Mockito.when(repository.delete(style.getStyleId())).thenReturn(expectedStyle);

        //Act
        Style returnedStyle = mockService.delete(style.getStyleId());

        //Assert
        Assert.assertSame(expectedStyle, returnedStyle);
    }


}
