package com.teamproject.demo.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.Tag;
import com.beerapplication.repositories.TagsRepository;
import com.beerapplication.services.TagsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.ArrayList;
import java.util.List;

import static com.teamproject.demo.services.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagsServiceImplTests {
    @Mock
    TagsRepository repository;

    @InjectMocks
    TagsServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnTag_WhenTagExists() {
        //Arrange
        Tag expectedTag = createTag();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedTag);

        //Act
        Tag returnedTag = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedTag, returnedTag);
    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Tag expectedTag = createTag();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedTag);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository, times(1)).getById(anyInt());
    }

    @Test
    public void createTagShould_ReturnNewTag_IfDoesNotExists() {
        //Arrange
        Tag tag = new Tag();
        Tag expectedTag = createTag();
        Mockito.when(repository.create(tag)).thenReturn(expectedTag);

        //Act
        Tag returnedTag = mockService.create(tag);

        //Assert
        Assert.assertSame(expectedTag, returnedTag);
    }


    @Test(expected = DuplicateEntityException.class)
    public void createTagShould_Throw_IfAlreadyExists() {
        //Arrange
        Mockito.when(repository.checkIfTagExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.create(createTag());
    }

    @Test
    public void getAllShould_returnAllTagsIfNoFilterEntered() {
        //Arrange
        List<Tag> listTags = new ArrayList<>();
        listTags.add(createTag());
        listTags.add(createTag2());
        listTags.add(createTag3());
        Mockito.when(repository.getAll()).thenReturn(listTags);

        //Act
        List<Tag> testList = mockService.getAll();

        //Assert
        Assert.assertSame(listTags, testList);
    }

    @Test
    public void getAllShould_returnAllTagsIfFilterEntered() {
        //Arrange
        List<Tag> listTags = new ArrayList<>();
//        listTags.add(createTag());
        listTags.add(createTag2());
//        listTags.add(createTag3());
        Mockito.when(repository.filterByName("2")).thenReturn(listTags);

        //Act
        List<Tag> testList = mockService.filterByName("2");

        //Assert
        Assert.assertSame(listTags, testList);
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        List<Tag> listTags = new ArrayList<>();
        listTags.add(createTag());
        listTags.add(createTag2());
        listTags.add(createTag3());
        Mockito.when(repository.getAll()).thenReturn(listTags);

        //Act
        mockService.getAll();

        //Assert
        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void updateTagShould_ReturnUpdatedTag() {
        //Arrange
        Tag tag = new Tag();
        Tag expectedTag = createTag();
        Mockito.when(repository.update(tag.getId(), tag)).thenReturn(expectedTag);

        //Act
        Tag returnedTag = mockService.update(tag.getId(), tag);

        //Assert
        Assert.assertSame(expectedTag, returnedTag);
    }

    @Test(expected = DuplicateEntityException.class)
    public void updateTagShould_Throw_IfAlreadyNotExists() {
        //Arrange
        Tag tag = createTag();
        Mockito.when(repository.checkIfTagExists(anyString())).thenReturn(true);

        //Act & Assert
        mockService.update(tag.getId(), tag);
    }

    @Test
    public void deleteTagShould_ReturnDeletedTag_IfExists() {
        Tag tag = new Tag();
        Tag expectedTag = createTag();
        Mockito.when(repository.delete(tag.getId())).thenReturn(expectedTag);

        //Act
        Tag returnedTag = mockService.delete(tag.getId());

        //Assert
        Assert.assertSame(expectedTag, returnedTag);
    }

}

