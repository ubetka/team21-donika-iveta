package com.teamproject.demo.services;

import com.beerapplication.exceptions.DuplicateEntityException;
import com.beerapplication.models.User;
import com.beerapplication.models.dtoes.UserGetAllDto;
import com.beerapplication.repositories.UsersRepository;
import com.beerapplication.services.UsersServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.teamproject.demo.services.Factory.*;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {
    @Mock
    UsersRepository repository;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnUser_WhenUserExists() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedUser);

        //Act
        User returnedUser = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedUser);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository, times(1)).getById(anyInt());
    }

    @Test
    public void createUserShould_ReturnNewUser_IfDoesNotExists() {
        //Arrange
        User user = new User();
        User expectedUser = createUser();
        Mockito.when(repository.create(user)).thenReturn(expectedUser);

        //Act
        User returnedUser = mockService.create(user);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }


    @Test
    public void getAllShould_returnAllUsers() {
        //Arrange
        List<UserGetAllDto> listUsers = new ArrayList<>();
        listUsers.add(createUserForList());
        listUsers.add(createUserForList2());
        listUsers.add(createUserForList3());
        Mockito.when(repository.getAll()).thenReturn(listUsers);

        //Act
        List<UserGetAllDto> testList = mockService.getAll();

        //Assert
        Assert.assertSame(listUsers, testList);
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange
        List<UserGetAllDto> listUsers = new ArrayList<>();
        listUsers.add(createUserForList());
        listUsers.add(createUserForList2());
        listUsers.add(createUserForList3());
        Mockito.when(repository.getAll()).thenReturn(listUsers);

        //Act
        mockService.getAll();

        //Assert
        Mockito.verify(repository, times(1)).getAll();
    }

    @Test
    public void updateUserShould_ReturnUpdatedUser() {
        //Arrange
        User user = new User();
        User expectedUser = createUser();
        Mockito.when(repository.update(user.getUserId(), user)).thenReturn(expectedUser);

        //Act
        User returnedUser = mockService.update(user.getUserId(), user);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }


    @Test
    public void deleteUserShould_ReturnDeletedUser_IfExists() {
        User user = new User();
        User expectedUser = createUser();
        Mockito.when(repository.delete(user.getUserId())).thenReturn(expectedUser);

        //Act
        User returnedUser = mockService.delete(user.getUserId());

        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }

}