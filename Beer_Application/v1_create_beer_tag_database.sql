create database if not exists beer_tag;
use beer_tag;

create table styles
(
    StyleID   int(10)     not null auto_increment,
    Name      varchar(50) not null unique,
    IsDeleted boolean     not null default false,
    primary key (StyleID),
    check (length(name) > 2)
);

create table breweries
(
    BreweryID int(10)     not null auto_increment,
    Name      varchar(50) not null unique,
    IsDeleted boolean     not null default false,
    primary key (BreweryID),
    check (length(name) > 2)
);

create table origin_countries
(
    OriginCountryID int(10)     not null auto_increment,
    Name            varchar(50) not null unique,
    IsDeleted       boolean     not null default false,
    primary key (OriginCountryID),
    check (length(name) > 3)
);

create table users
(
    UserID    int(10)      not null auto_increment,
    Username  varchar(30)  not null unique,
    Password  varchar(30)  not null,
    Email     varchar(50) not null unique,
    Type      varchar(20)  not null default 'user',
    IsDeleted boolean      not null default false,
    primary key (UserID),
    check (length(username) > 2),
    check (length(password) > 5),
    check (length(email) > 7)
);

create table user_profile
(
    UserProfileID int(10)     not null auto_increment,
    FirstName     varchar(30) not null,
    LastName      varchar(30) not null,
    Picture       varchar(1000),
    UserID        int(10)     not null,
    primary key (UserProfileID),
    foreign key (UserID) references users (UserID),
    check (length(FirstName) > 2),
    check (length(LastName) > 2)
);

create table beers
(
    BeerID          int(10) not null auto_increment,
    StyleID         int(10) not null,
    BreweryID       int(10) not null,
    OriginCountryID int(10) not null,
    UserID          int(10) not null,
    IsDeleted       boolean not null default false,
    primary key (BeerID),
    foreign key (StyleID) references styles (StyleID),
    foreign key (BreweryID) references breweries (BreweryID),
    foreign key (OriginCountryID) references origin_countries (OriginCountryID),
    foreign key (UserID) references users (UserID)
);

create table beer_details
(
    BeerDetailsID int(10)     not null auto_increment,
    Name          varchar(50) not null unique,
    Description   varchar(500),
    ABV           double      not null,
    Picture       varchar(1000),
    BeerID        int(10)     not null,
    primary key (BeerDetailsID),
    foreign key (BeerID) references beers (BeerID),
    check (length(name) > 1),
    check (length(Description) > 20)
);

create table tags
(
    TagID     int(10)     not null auto_increment,
    Name      varchar(50) not null unique,
    IsDeleted boolean     not null default false,
    primary key (TagID),
    check (length(name) > 2)
);

create table beers_tags
(
    BeerTagID     int(10) not null auto_increment,
    TagID         int(10) not null,
    BeerDetailsID int(10) not null,
    primary key (BeerTagID),
    foreign key (TagID) references tags (TagID),
    foreign key (BeerDetailsID) references beer_details (BeerDetailsID)
);

create table beers_ratings
(
    BeerRatingID int(10) not null auto_increment,
    BeerID       int(10) not null,
    UserID       int(10) not null,
    Rating       int(1)  not null,
    primary key (BeerRatingID),
    foreign key (BeerID) references beers (BeerID),
    foreign key (UserID) references users (UserID),
    check ( Rating > 0 and Rating <6)
);

create table users_wished_beers
(
    UserWishedBeerID int(10) not null auto_increment,
    BeerID           int(10) not null,
    UserID           int(10) not null,
    primary key (UserWishedBeerID),
    foreign key (BeerID) references beers (BeerID),
    foreign key (UserID) references users (UserID)
);

create table users_drank_beers
(
    UserDrankBeerID int(10) not null auto_increment,
    BeerID          int(10) not null,
    UserID          int(10) not null,
    primary key (UserDrankBeerID),
    foreign key (BeerID) references beers (BeerID),
    foreign key (UserID) references users (UserID)
);