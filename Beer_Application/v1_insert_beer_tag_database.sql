use beer_tag;

insert into styles (Name)
values ('Amber ale'),
       ('Blonde Ale'),
       ('Dunkel'),
       ('India pale ale'),
       ('Malt liquor'),
       ('Old ale');

insert into breweries (Name)
values ('Bloody Muddy'),
       ('Flying Dog'),
       ('Hop Soul'),
       ('Dogfish Head Craft Brewery'),
       ('Harpoon Brewery'),
       ('Hook Norton Brewery'),
       ('Erdinger Brewery'),
       ('De Halve Maan Brewery');

insert into origin_countries (Name)
values ('Bulgaria'),
       ('United States'),
       ('United Kingdom'),
       ('Germany'),
       ('Denmark'),
       ('Belgium');

insert into users (Username, Password, Email)
values ('dodoni', 'www111www', 'dododni@dodoni.com'),
       ('iveto', 'aaabccc', 'iveta@iveta.com');

insert into user_profile (FirstName, LastName, UserID)
values ('Donika', 'Yordanova', 1),
       ('Iveta', 'Dimitrova', 2);

insert into beers (StyleID, BreweryID, OriginCountryID, UserID)
values (1, 2, 3, 1),
       (2, 1, 4, 1),
       (4, 6, 5, 2),
       (5, 4, 6, 2),
       (3, 5, 1, 1),
       (6, 3, 2, 2);

insert into beer_details(Name, ABV, BeerID)
values ('Erdinger Weissbier', 5.6, 1),
       ('Trima & Dvama', 3.3, 2),
       ('Under Dog', 4.4, 3),
       ('Dragons & YumYums', 3.8, 4),
       ('Straffe Hendrik', 5.0, 5),
       ('One Hazy Summer', 3.3, 6);

insert into tags (Name)
values ('beerific'),
       ('beerme'),
       ('beernerd'),
       ('hophead'),
       ('ibeer'),
       ('friendsdontletfriendsdrinkshittybeer'),
       ('cerveza'),
       ('Bira');

insert into beers_tags (BeerDetailsID, TagID)
values (1, 8),
       (2, 5),
       (1, 7),
       (3, 6),
       (4, 4),
       (6, 2),
       (5, 3);

insert into beers_ratings (BeerID, UserID, Rating)
values (2, 2, 5),
       (5, 1, 4),
       (6, 2, 2),
       (4, 1, 3),
       (1, 1, 1),
       (3, 2, 3),
       (2, 1, 2),
       (1, 1, 2);