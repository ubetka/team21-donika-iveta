create database if not exists beer_tag;
use beer_tag;

create table styles
(
    StyleID   int(10)     not null auto_increment,
    Name      varchar(50) not null unique,
    Enabled   boolean not null default true,
    primary key (StyleID),
    check (length(name) > 2)
);

create table breweries
(
    BreweryID int(10)     not null auto_increment,
    Name      varchar(50) not null unique,
    Enabled   boolean not null default true,
    primary key (BreweryID),
    check (length(name) > 2)
);

create table origin_countries
(
    OriginCountryID int(10)     not null auto_increment,
    Name            varchar(50) not null unique,
    Enabled   boolean not null default true,
    primary key (OriginCountryID),
    check (length(name) > 3)
);

create table users
(
    UserID    int(10)     not null auto_increment,
    Username  varchar(30) not null unique,
    Password  varchar(68) not null,
    Email     varchar(50) unique,
    Enabled   boolean not null default true,
    FirstName varchar(30),
    LastName  varchar(30),
    Picture   longblob,
#     IsDeleted boolean     not null default false,
    primary key (Username),
    index (UserID),
    check (length(Username) > 2),
    check (length(Password) > 5),
    check (length(Email) > 7),
    check (length(FirstName) > 2),
    check (length(LastName) > 2)
);

create table roles(
    RoleID int(2) not null auto_increment,
    Role varchar(5) not null unique,
    primary key (RoleID)
);

create table authorities
(
    Username varchar(30) not null,
    Authority varchar(5) not null,
    unique key Username_Authority (Username, Authority),
    constraint authorities_fk foreign key (Username) references users(Username)
);

# create table users_roles(
#     UserRoleID int(10) not null auto_increment,
#     UserID int(10) not null,
#     RoleID int(2) not null,
#     primary key (UserRoleID),
#     foreign key (UserID) references users (UserID),
#     foreign key (RoleID) references roles (RoleID),
#     constraint unique (UserID, RoleID)
# );

create table beers
(
    BeerID          int(10)     not null auto_increment,
    Name            varchar(50) not null unique,
    Description     varchar(500),
    ABV             double      not null,
    Picture         longblob,
    StyleID         int(10)     not null,
    BreweryID       int(10)     not null,
    OriginCountryID int(10)     not null,
    UserID          int(10)     not null,
    Enabled   boolean not null default true,
    primary key (BeerID),
    foreign key (StyleID) references styles (StyleID),
    foreign key (BreweryID) references breweries (BreweryID),
    foreign key (OriginCountryID) references origin_countries (OriginCountryID),
    foreign key (USERID) references users (UserID),
    check (length(name) > 1),
    check (length(Description) > 19)
);

create table tags
(
    TagID     int(10)     not null auto_increment,
    Name      varchar(50) not null unique,
    Enabled   boolean not null default true,
    primary key (TagID),
    check (length(name) > 2)
);

create table beers_tags
(
    BeerTagID int(10) not null auto_increment,
    TagID     int(10) not null,
    BeerID    int(10) not null,
    primary key (BeerTagID),
    foreign key (TagID) references tags (TagID),
    foreign key (BeerID) references beers (BeerID)
);

create table beers_ratings
(
    BeerRatingID int(10) not null auto_increment,
    BeerID       int(10) not null,
    UserID       int(10) not null,
    Rating       int(1)  not null,
    primary key (BeerRatingID),
    foreign key (BeerID) references beers (BeerID),
    foreign key (UserID) references users (UserID),
    check ( Rating > 0 and Rating < 6),
    constraint unique (UserID, BeerID)
);

create table users_wished_beers
(
    UserWishedBeerID int(10) not null auto_increment,
    BeerID           int(10) not null,
    UserID           int(10) not null,
    primary key (UserWishedBeerID),
    foreign key (BeerID) references beers (BeerID),
    foreign key (UserID) references users (UserID),
    constraint unique (BeerID, UserID)
);

create table users_drank_beers
(
    UserDrankBeerID int(10) not null auto_increment,
    BeerID          int(10) not null,
    UserID          int(10) not null,
    primary key (UserDrankBeerID),
    foreign key (BeerID) references beers (BeerID),
    foreign key (UserID) references users (UserID),
    constraint unique (BeerID, UserID)
);