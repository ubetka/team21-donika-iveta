use beer_tag;

insert into styles (Name)
values ('Amber ale'),
       ('Blonde Ale'),
       ('Dunkel'),
       ('India pale ale'),
       ('Malt liquor'),
       ('Old ale');

insert into breweries (Name)
values ('Bloody Muddy'),
       ('Flying Dog'),
       ('Hop Soul'),
       ('Dogfish Head Craft Brewery'),
       ('Harpoon Brewery'),
       ('Hook Norton Brewery'),
       ('Erdinger Brewery'),
       ('De Halve Maan Brewery');

insert into origin_countries (Name)
values ('Bulgaria'),
       ('United States'),
       ('United Kingdom'),
       ('Germany'),
       ('Denmark'),
       ('Belgium');

# insert into users (Username, Password, Email, FirstName, LastName)
# values ('dodoni', '{noop}asdasd', 'dododni@dodoni.com', 'Donika', 'Yordanova'),
#        ('iveto', '{noop}aaabccc', 'iveta@iveta.com', 'Iveta', 'Dimitrova'),
#        ('pesho', '{noop}llooll', 'pesh@pesho.com', 'Petar', 'Petrov');

insert into beers (Name, ABV, StyleID, BreweryID, OriginCountryID, UserID)
values ('Erdinger Weissbier', 5.6, 1, 2, 3, 1),
       ('Trima & Dvama', 3.3, 2, 1, 4, 1),
       ('Under Dog', 4.4, 4, 6, 5, 2),
       ('Dragons & YumYums', 3.8, 5, 4, 6, 2),
       ('Straffe Hendrik', 5.0, 3, 5, 1, 1),
       ('One Hazy Summer', 3.3, 6, 3, 2, 2);

insert into tags (Name)
values ('beerific'),
       ('beerme'),
       ('beernerd'),
       ('hophead'),
       ('ibeer'),
       ('friendsdontletfriendsdrinkshittybeer'),
       ('cerveza'),
       ('Bira');

insert into beers_tags (BeerID, TagID)
values (1, 8),
       (2, 5),
       (1, 7),
       (3, 6),
       (4, 4),
       (6, 2),
       (5, 3);

insert into beers_ratings (BeerID, UserID, Rating)
values (2, 2, 5),
       (5, 1, 4),
       (6, 2, 2),
       (4, 1, 3),
       (3, 2, 3),
       (2, 1, 2),
       (1, 1, 2);

insert into roles (Role)
values ('ADMIN'),
       ('USER');

insert into users_wished_beers(BeerID, UserID)
value (2, 1),
      (5, 1),
      (6, 1);

insert into users_drank_beers(BeerID, UserID)
    value (3, 1),
          (4, 1),
          (1, 1);

# insert into authorities (Username, Authority)
# values ('dodoni', 'ADMIN'),
#        ('dodoni', 'USER'),
#        ('iveto', 'ADMIN'),
#        ('iveto', 'USER'),
#        ('pesho', 'USER');
