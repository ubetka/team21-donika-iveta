## **Work Item Management (WIM) - OOP Teamwork Assignment**

[For Trello click here](https://trello.com/b/SNgo8jHa/wim-project)

[For GitLab click here](https://gitlab.com/ubetka/team21-donika-iveta)

[For GitLab Test repository click here](https://gitlab.com/ubetka/test_repo/tree/master/Work_Item_Management)

[For Architecture diagram - old version click here](https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&fbclid=IwAR3ujIQzW0yyidFfVIA1Gxqr7vO1Vx4XXhWL1JsjIbBrxfO7KlBfHLlAFy4#R7V1tj6O6Ff41kdpKE4ExL%2Fk4ydzdnWpX3c6serv95oCT0CU4BedO5v76mgQTsE1CMjgQidVIG79g4JzH583HZmTN1rvPCdqsvpEARyNgBLuR9TQCwDQnNvsvq3k%2F1DiWcahYJmGQdzpWvIZ%2F4rySd9uGAU4rHSkhEQ031UqfxDH2aaUOJQl5q3ZbkKh61w1aYqni1UeRXPt7GNDVodazjWP9FxwuV%2FzOppG3rBHvnFekKxSQt1KV9dvImiWE0MOv9W6Go4x4nC6H6z7VtBYPluCYNrng8%2FM%2Fd%2F%2Fyf%2F5cgH%2F8x%2Fv77L8%2BnK0f8lH%2BQNE2f%2BHRDIwep2FMcbJAPs6K0%2BkIOBG7yXSesF%2FL7FdRwyumBCXBYxx8x0lKYt7MHmd%2BvGRPBvrOabui64j9MlnbIoyiGYlIsm%2BxFosF8H1Wn9KE%2FMKllsCZO7ZTjFZ%2Bff4uOKF4V6rKyfEZkzWmyTvrwlvBZHK4Jgen5R2Kb0dOmxyIqxKXOfdRDq5lMfSR%2FuxHzoIL2GG3y4774IJp2hUumI6CDeCWbHAlNqQr8vbo0%2FCPkL7%2F5a8y%2FaMwpb%2BT5Nczxes069A3IotQ97qGutca1O9J4kCnb1g3ZRXw247iOEgl6rHKx0ylstI8Iv4vRhNWlets0zkUP4XZ%2FZ8MVipRm5Esef93Xr8v%2FMwKY2Dz8tOu3Pr0zku7kO6vGzuOnZezSx%2BMsWHl5eOlWaF8JYNGyMiEk7yuloUp2SY%2BPi%2BZKUqWmJ4XHTiomBW1gDDGlmM5VUhYMiKcnPsJjhATQlXbRAWJ%2FIbfCZtOZVE7EeEHqmMc6JBfVjYmxJGAWx0JQK860oFS0kgMQOi91G2TdUgveWTDFDB%2FGFK4nI9PFosU05E4SwpOfGDiAGnioCCYbpcq%2FcBaXilJlLqDtX3COJgj%2F1cfNYeonoHZuciCMuXTNFzGXAOrqLyNWR%2BxU99oLaqHHtDauYV6qKVdUznK1AGArlUhHjT1CVIb1OjxiwUpNKqG2USTHBWfOL%2FPGTHKyQuNysUPtkAyjWJWNsavNRR%2FYLS%2BCzPRc%2BG4yq3OPVNTttczn%2BgbXs%2BZ%2Ba0SuFnz3gutbc34UXaqeseFKg%2Baukwe0MWDicSDjITP600kES99C9cRinFGPBLT17wlIyaKmBZkv30c7y3jaUaS0EfRY95AyYbV%2BqswCr6id7LN3iulzDzhpemKJOGfbFjEGcSaE5qLeuBUerxmV%2BYiP8Ep6%2FOdc8UUqr6hXaXjV5TSvMInUYQ2aTgvXmPNBGMYTwmlZJ13EmASIOwtlDBxfA%2FPFy3BBE7EycqlpUrzlJFiGlATVLiNUJaagL2tsQhxFgF9ZD9jtMYSbthL05MUq6MxR1WEF7QWU%2BkG%2BWG8%2FLrv8wSPNS85UbIqwq5dRHs7YhUGAY4zPBCKKDowP%2BN0ru7YI9hT9sfedmYwLtjskWasbB7L7C%2FrntAZidlboXDPaMyQ9YZTqoTA6cl3HhjceHOaoYD3ax8EinCiCIKvYZrp%2BfVBjg9w0AcHG3QNB9mUr4PDfK%2B3BzToQ4OrMCZuiwZFALaBRxegdIWDEY%2Fqlf25krMHhOhfEcQrh%2FCKiJ06fFdEDMdFjPDnqBRNrAkY1mr0syE%2BzsSzMT5uC591TjV4nhPgidYGuNL1nFhVpw5AwXmocT3b8uyAbM7u%2FYXBnu3UnmU%2BzZXmLNdwrcsqa7BmW1ZY4P6sWUsWF4eVv0FedCsvIKxRSOeg4nqaoAIHedGyvLDuT15Aq7G7s8FkEw1o0IiGzp1fKGuPXrk7tYK4CyfDNF2oNgEvXt4SRwLiQplmJ8NWqQJdfG%2FbZe2E9%2BJC4dX%2BpTTSzXnfXnYrTxq4i%2BVDc1J17DtfPOTSv8QIxvfnJ9XCIGv4EdII17Q94dRPwg0NSVzTg9nbdLtfczT%2BhuaZmvOpsuOMrNeMyMrlSdb8hdkGecZOz%2FnbdGGyyE1vn8EKW%2BvKmcZToe5ipgEhgqZK17ntTJNTo67OIN8uH%2BND1tpd8AKK2qZ7ZrSYzr9d3gUTLGFGWFBh%2BFs3ZYJi1etKJtzPXLDN3rFBTuNiOvaFWarxsocqVhTsqjT928oSeblub%2BvgTfqDvOBNQoKtX2c0vWL25v1MspIExqQpUnUFDx3ZY8yclCQkSc3mH9b8uE8txriHFJb0YudYdmS%2FLAMpc6p7SD5JlHYPUNmb6mNKtjE2bKea2%2FxwKij8wbiF5YgqTxiiadhCni%2BT6kAtZWSLD8znZbOMbOEhHywhk1BfQrYj%2B3o6N4yZY8MA5WhatvPLcM6E1Palxru%2FzmZ8cFZdgn0eNuPsVYiNlqDvGCL0r4zWOoY7vi5kdyn4xUe%2BDPwPgtFQJAjcAP2yg60D%2FcUKgmXyXY4c%2B%2BDybY%2FFZNrPncpsMsa2ey4%2B3fJk4tHAnioS1x47E%2BP4T4wquGPPLjWD66aaKeyoMaGw8tXSRBP2MJj5Wn3vlYwcOtGtZFxhWkz6Pytup2LgpCa4drF1ZRlj%2FpxHTLIZV%2FpnaZkJ4ivwGdd%2FlSNHsHiQfEh96jT1ScAE5OsyZz1rQWm05xoqNkuKOS%2FJPuYmoWZIdjk3%2FT6U%2BqSCgbZkF1cOsOxj14O46FRciKEkpbxQhpJ0yQtXDiVJ8iIj7yAtmkoLtzkqTkgLFQj0SQvZvphul4Os6FRWiOsiSllhK2CiLanabWBbpPlC0yAvGsuLVqwLFRD0yQtVKq0IBGFhcgCEPkCocqtvCghPXjEtJQ8xRTKqpOINOqUjnQIFnWKrdMqlG3Xyu71gn6J4me2iuOh2bgOgoohxP0YUT8mWRwBbjrF4ilRhUaZt8hSAQZY1lWWFYLifXUNeg11DKE%2F2GICgDwidbxjy5IU%2Bvgng4BoNKq0HKq0wdE65Scqjl7Sd0uU1OHWHDTDIjqayAzYGRW%2BUSANXmWabXAYU6ENB5xqEn1R8EgbBcT%2FTAAZ9YOj8uCWTJ4qcjppkW9cGHOjDQXHiYodAaH4ugZ%2FvUBwQoRERVkOLUSMiFJv2ahCxOmxKHQChERB21%2Bajaagi7ALHh0MLylmO1UMmCi%2Fw0uw08dgLfiTgjQ4sKLJEbsx4CK0K68eGAc%2Bwv6vsRw3gAVCKIfAs%2BEvxIw7FACQOpR1CKqdDF4SKNHXoji7LUi9JHSgkpE887xrwtfu9H74Mdx65HR4GagkHQUDvyqxcaSC7WTZ6e6hVeUfaUXunxzPZQlL%2FBwSWNJS46VM741XekH6N55hwVNV44BqZc73Ca7yjTEsyf%2FVDOfa15hL0jDGojmWZ7ri8e8Zolsx%2FBZ52xoq4%2FzO%2FuyZ6fvmxNLAdfX6Q0VT9LumwdNOfpRuGuwp0lAe28GWaj6cjNLofMBo4eu3mIyhx3CAiMBxgWhMHOCkYPrSIpAJjG1EA5RM3%2F1rDG%2F%2Fw7AAHbXBQrSbpgoP6zNXmeED5N7W%2BDMHCWlS0oK4mXUsI1Spzj3ymso9vFwc25Ob2jfecN%2F0sr44DVaWDPq7dOyuN1HTTeFtWiXwGlcazD8wy0ArYXeien43z8A8hXPKhUderhomLD9LqQA9w4NiTzxsojnZ1x2Vv68ozb0zpGC4gbPVv6zOk0o2cRruwW91lrcS2vOSiEdv7czisCsChOTktTVsNb8Km4Ycj7GGBiWKhTB%2FqIZdsHCbulSEKE4qH4wA9xwvIN3IaHbXxEWSzYkIILXdP0Gb1jQQ46%2FF%2F)

### **General Description**

Work Item Management project is a project about application which supports multiple teams.

### **Architecture**
There is an EngineImpl located in the core package that has a loop that cycles until the Exit command is submitted. With each cycle, it takes the input, passes it to the command parser that parses the parameters. Then there is CommandFactoryImpl that creates the command. All commands are located in the commands package. The commands themselves use the FactoryImpl located in the core.factories package to create the needed objects and RepositoryImpl to store them. After the command executes, it returns a result message to the EngineImpl that prints it to the console and then the cycle begins again.

In the EngineImpl, there is a try-catch block that catches every possible exception type and prints the exception's message to the console.

You can stay focused on the **workitems**, **teams** and **commands** packages.

### **Functionality**

Each team has name, members and boards.

- **Member**:

- Member has name, list of work items and activity history.
- Name should be unique in the application
- Name is a string between 5 and 15 symbols.



- **Board**:

- Board has name, list of work items and activity history.
- Name should be unique in the team
- Name is a string between 5 and 10 symbols.



There are 3 types of work items: bug, story and feedback.

- **Bug**:

- Bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments and history.
- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Steps to reproduce is a list of strings.
- Priority is one of the following: High, Medium, Low
- Severity is one of the following: Critical, Major, Minor & Status is one of the following: Active, Fixed & Assignee is a member from the team.
- Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the bug.



- **Story**:

- Story has ID, title, description, priority, size, status, assignee, comments and history.
- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Priority is one of the following: High, Medium, Low
- Size is one of the following: Large, Medium, Small & Status is one of the following: NotDone, InProgress, Done & Assignee is a member from the team.
- Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the story.



- **Feedback**:

- Feedback has ID, title, description, rating, status, comments and history.
- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Rating is an integer.
- Status is one of the following: New, Unscheduled, Scheduled, Done & Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the feedback.



#### **Operations**
Each command is represented in the code base as a separate class, that is invoked by the CommandFactoryImpl. 
Application support the following operations:

- **create**

- **CreatePerson** [personName] - Creates a new Person.
- **CreateTeam** [teamName] - Creates a new Team.
- **CreateBoard** [boardName] [teamName] - Creates a new Board.
- **CreateBug** [bugName] [bugDescription] [severityType] [boardName] - Creates a new Bug.
- **CreateStory** [storyName] [storyDescription] [boardName] - Creates a new Story.
- **CreateFeedback** [feedbackName] [feedbackDescription] [boardName] - Creates a new Feedback.



- **addremove**

- **AddPersonToTeam** [personName] [teamName] - Add Person to Team.
- **RemovePersonFromTeam** [personName] [teamName] - Removes Person from Team.
- **AssignToPerson** [assignmentName] [assignmentID] [personName] - Assign Work Item to Person.
- **UnassignFromPerson** [assignmentName] [assignmentID] [personName] - Unassign Work Item from Person.
- **AddCommentToWorkItem** [workItemName] [workItemID] [comment] - Add comment to Work Item.
- **RemoveCommentFromWorkItem** [workItemName] [workItemID] [comment] - Removes comment from Work Item.
- **AddStepsToReproduceToBug** [workItemName] [workItemID] [stepsToReproduce] - Add steps to reproduce to Bug.
- **RemoveBoardFromTeam** [boardName] [teamName] - Removes Board from Team.
- **RemoveWorkItemFromBoard** [workItemName] [boardName] - Removes Work Item from Board.



- **set**

- **SetPriority** [workItemType] [workItemName] [workItemID] [priorityType] - Changes Priority type of Bug/Story.
- **SetRating** [workItemType] [workItemName] [workItemID] [newRating] - Changes Rating of a Feedback.
- **SetSeverity** [workItemType] [workItemName] [workItemID] [severityType] - Changes Severity type of Bug.
- **SetSize** [workItemType] [workItemName] [workItemID] [sizeType] - Changes Size type of Story.
- **SetStatus** [workItemType] [workItemName] [workItemID] [statusType] - Changes Status type of Bug/Feedback/Story.



- **sort**

- **SortByPriority** - Sort by Priority type of Bug&Story.
- **SortByRating** - Sort by Rating of a Feedback.
- **SortBySeverity** - Sort by Severity type of Bug.
- **SortBySize** - Sort by Size type of Story.
- **SortByStatus** - Sort by Status Type of Bug/Feedback/Story.



- **filter**

- **FilterAssignee** [personName] - Filter by Assignee.
- **FilterStatus** [statusType] - Filter by Status type.
- **FilterStatusAssignee** [personName] [statusType] - Filter by Status type and Assignee.



- **show**

- **ShowActivity** [objectToShow] [objectName] - Show Team/Person/Board activity.
- **ShowAll** [objectToShow] - Show all Teams/People/Boards/WorkItems/Bug/Story/Feedback.
- **ShowAllForTeam** [objectToShow] - Show all Members/Boards for Team.


#### **Sample Input**

```
createPerson Iveta
createTeam OurTeam
createBoard WIMboard OurTeam
createStory newStory123456 description1234567890 WIMboard
createBug newbug1111111 description1234567890 minor WIMboard
createFeedback feedback123 description1234567890 WIMboard
createboard BoardDEL OurTeam
removeboardfromteam BoardDEL OurTeam
removeworkitemfromboard feedback123 3 WIMboard
createPerson Donika
addpersontoteam Iveta OurTeam
addpersontoteam Donika OurTeam
createPerson pesho
addpersontoteam pesho OurTeam
removepersonfromteam pesho OurTeam
addstepstoreproducetobug newbug1111111 2 Click.Open.Run.Broke.
addcommenttoworkitem newbug1111111 2 TryThisAtHome
removecommentfromworkitem newbug1111111 2 TryThisAtHome
createteam OtherTeam
createStory newStory98765 description1234567890 WIMboard
createBug newbug2222 description1234567890 major WIMboard
createFeedback feedback987 description1234567890 WIMboard
createStory newStoryMyStory description1234567890 WIMboard
createBug newbug33333 description1234567890 major WIMboard
createFeedback feedbackNewF description1234567890 WIMboard
assigntoperson newStory123456 1 Iveta
assigntoperson newStory98765 4 Iveta
assigntoperson feedback987 6 Iveta
assigntoperson feedbackNewF 9 Iveta
assigntoperson newbug1111111 2 Donika
assigntoperson newbug33333 8 Donika
assigntoperson newStoryMyStory 7 Donika
unassignfromperson newStory123456 1 Iveta
assigntoperson newStory123456 1 Donika
setsize story newStory123456 1 small  
setsize story newStory98765 4 small
setseverity bug newbug1111111 2 minor
setseverity bug newbug2222 5 minor
setseverity bug newbug33333 8 major
setpriority bug newbug1111111 2 low
setpriority story newStoryMyStory 7 low 
setpriority story newStory123456 1 high
setpriority bug newbug33333 8 medium
setrating feedback feedbackNewF 9 5
setrating feedback feedbackNewF 9 0
setrating feedback feedbackNewF 9 7
setrating feedback feedbackNewF 9 1
setrating feedback feedback987 6 2  
setstatus bug newbug1111111 2 fixed 
setstatus bug newbug33333 8 fixed
setstatus feedback feedbackNewF 9 done
setstatus feedback feedback987 6 done
setstatus story newStory98765 4 done 
showall people
showall teams
showall workitems
showallforteam OurTeam members
showallforteam OurTeam boards
showactivity person Iveta
showactivity team OurTeam
showactivity board WIMboard
filterassignee Donika
filterstatus done 
filterstatusassignee fixed Donika
sortbytitle
sortbyrating
sortbysize
sortbyseverity
sortbypriority
exit
```

#### **Sample Output**

```
Person Iveta created.
Team "OurTeam" created.
Board "WIMboard" created in "OurTeam" team.
Story "newStory123456" with ID: 1 created in "WIMboard" board.
Bug "newbug1111111" with ID: 2 created in "WIMboard" board.
Feedback "feedback123" with ID: 3 created in "WIMboard" board.
Board "BoardDEL" created in "OurTeam" team.
Board "BoardDEL" successfully removed from team "OurTeam".
feedback "feedback123" with ID: 3 successfully removed from board "WIMboard".
Person Donika created.
Person Iveta successfully added to team "OurTeam".
Person Donika successfully added to team "OurTeam".
Person pesho created.
Person pesho successfully added to team "OurTeam".
Person pesho successfully removed from team "OurTeam".
Steps to reproduce successfully added to bug "newbug1111111".
Comment successfully added to bug "newbug1111111".
Comment successfully removed from bug "newbug1111111".
Team "OtherTeam" created.
Story "newStory98765" with ID: 4 created in "WIMboard" board.
Bug "newbug2222" with ID: 5 created in "WIMboard" board.
Feedback "feedback987" with ID: 6 created in "WIMboard" board.
Story "newStoryMyStory" with ID: 7 created in "WIMboard" board.
Bug "newbug33333" with ID: 8 created in "WIMboard" board.
Feedback "feedbackNewF" with ID: 9 created in "WIMboard" board.
story with name "newStory123456" and ID: 1 assigned to Iveta.
story with name "newStory98765" and ID: 4 assigned to Iveta.
Wrong work item type entered. Please enter either "bug" or "story".
Wrong work item type entered. Please enter either "bug" or "story".
bug with name "newbug1111111" and ID: 2 assigned to Donika.
bug with name "newbug33333" and ID: 8 assigned to Donika.
story with name "newStoryMyStory" and ID: 7 assigned to Donika.
story with name "newStory123456" and ID: 1 unassigned from Iveta.
story with name "newStory123456" and ID: 1 assigned to Donika.
Size type of story with name "newStory123456" and ID: 1 changed to "small".
Size type of story with name "newStory98765" and ID: 4 changed to "small".
Severity type of bug with name "newbug1111111" and ID: 2 changed to "minor".
Severity type of bug with name "newbug2222" and ID: 5 changed to "minor".
Severity type of bug with name "newbug33333" and ID: 8 changed to "major".
Priority type of bug with name "newbug1111111" and ID: 2 changed to "low".
Priority type of story with name "newStoryMyStory" and ID: 7 changed to "low".
Priority type of story with name "newStory123456" and ID: 1 changed to "high".
Priority type of bug with name "newbug33333" and ID: 8 changed to "medium".
Rating of feedback with name "feedbackNewF" and ID: 9 changed to 5
Rating must be between 1 and 10.
Rating of feedback with name "feedbackNewF" and ID: 9 changed to 6
Rating of feedback with name "feedbackNewF" and ID: 9 changed to 4
Rating of feedback with name "feedback987" and ID: 6 changed to 2
Status type of bug with name "newbug1111111" and ID: 2 changed to "fixed".
Status type of bug with name "newbug33333" and ID: 8 changed to "fixed".
Status type of feedback with name "feedbackNewF" and ID: 9 changed to "done".
Status type of feedback with name "feedback987" and ID: 6 changed to "done".
Status type of story with name "newStory98765" and ID: 4 changed to "done".
pesho, Donika, Iveta
OurTeam, OtherTeam
newStory123456, newbug2222, feedbackNewF, newStory98765, feedback987, newbug1111111, newbug33333, newStoryMyStory
Iveta, Donika
WIMboard
Person's name: Iveta
Activity History:
story with name "newStory123456" and ID: 1 assigned to Iveta.
story with name "newStory98765" and ID: 4 assigned to Iveta.
story with name "newStory123456" and ID: 1 unassigned from Iveta.

Team: OurTeam
Team Activity:
Board "WIMboard" created in "OurTeam" team.
Board "BoardDEL" created in "OurTeam" team.
Board "BoardDEL" successfully removed from team "OurTeam".
Person Iveta successfully added to team "OurTeam".
Person Donika successfully added to team "OurTeam".
Person pesho successfully added to team "OurTeam".
Person pesho successfully removed from team "OurTeam".

Board: WIMboard
History:
story "newStory123456" with ID: 1 successfully added to board "WIMboard".
bug "newbug1111111" with ID: 2 successfully added to board "WIMboard".
feedback "feedback123" with ID: 3 successfully added to board "WIMboard".
feedback "feedback123" with ID: 3 successfully removed from board "WIMboard".
story "newStory98765" with ID: 4 successfully added to board "WIMboard".
bug "newbug2222" with ID: 5 successfully added to board "WIMboard".
feedback "feedback987" with ID: 6 successfully added to board "WIMboard".
story "newStoryMyStory" with ID: 7 successfully added to board "WIMboard".
bug "newbug33333" with ID: 8 successfully added to board "WIMboard".
feedback "feedbackNewF" with ID: 9 successfully added to board "WIMboard".

newbug1111111, newbug33333, newStoryMyStory, newStory123456
newStory98765, feedback987, feedbackNewF
newbug1111111, newbug33333
feedback987, feedbackNewF, newStory123456, newStory98765, newStoryMyStory, newbug1111111, newbug2222, newbug33333
feedback987, feedbackNewF
newStory123456, newStory98765, newStoryMyStory
newbug33333, newbug1111111, newbug2222
```