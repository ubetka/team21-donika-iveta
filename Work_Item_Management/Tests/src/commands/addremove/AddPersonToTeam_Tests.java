package commands.addremove;

import com.commands.addremove.AddPersonToTeam;
import com.commands.contracts.Command;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonToTeam_Tests {
    private Command testCommand;
    private Team testTeam;
    private Person testPerson;
    private Repository repository;
    private Factory factory;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new AddPersonToTeam(repository, factory);
        testTeam = new TeamImpl("teamName");
        testPerson = new PersonImpl("personName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        testList.add("no");
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("personName");
        repository.addPerson("personName", testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_personDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("personName");
        repository.addTeam("teamName", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }


    @Test
    public void execute_should_addPersonToTeam_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("personName");
        testList.add("teamName");
        repository.addPerson("personName", testPerson);
        repository.addTeam("teamName", testTeam);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testTeam.getMembers().size());

    }
}

