package commands.addremove;

import com.commands.addremove.AddStepsToReproduceToBug;
import com.commands.contracts.Command;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddStepsToReproduceToBug_Tests {
    private Command testCommand;
    private Bug testBug;
    private Repository repository;
    private Factory factory;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new AddStepsToReproduceToBug(repository, factory);
        testBug = new BugImpl("bugNameeeee", "bugDescriptionnnnnnnnn", SeverityType.MAJOR);
        repository.addWorkItem(testBug.getID(),testBug);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("workItemName");
        testList.add("workItemID");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("workItemName");
        testList.add("workItemID");
        testList.add("stepsToReproduce");
        testList.add("12345");


        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_AddStepsToReproduceToBug_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bugNameeeee");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("stepsToReproduce.steps.step.step.bla");

        // Act & Assert
        testCommand.execute(testList);
    }
}

