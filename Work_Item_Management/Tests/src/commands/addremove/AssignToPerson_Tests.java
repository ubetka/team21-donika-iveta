package commands.addremove;

import com.commands.addremove.AssignToPerson;
import com.commands.contracts.Command;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.commands.common.CommandConstants.EXPECTED_TYPE_BUG;
import static com.commands.common.CommandConstants.EXPECTED_TYPE_STORY;

public class AssignToPerson_Tests {
    private Command testCommand;
    private Board testBoard;
    private Team testTeam;
    private Person testPerson;
    private Bug testBug;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new AssignToPerson(repository, factory);
        testPerson = new PersonImpl("personName");
        testTeam = new TeamImpl("teamName");
        testBoard = new BoardImpl("boardName");
        testBug = new BugImpl("bugName123", "description123456789", SeverityType.MAJOR);
    }

    @After
    public void after(){
        repository.getBoard().remove(testBoard, "boardName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("assignmentName");
        testList.add("assignmentID");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("assignmentName");
        testList.add("assignmentID");
        testList.add("personName");
        testList.add("12345");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongWorkItemType() {
        Feedback testFeedback = new FeedbackImpl("feedbackName", "description123456");
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("feedbackName");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add(String.valueOf(4));

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("personName");
        repository.addPerson("personName", testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_personDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("personName");
        repository.addWorkItem(testBug.getID(),testBug);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("personName");
        repository.addPerson("personName", testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_AssignBugToPerson_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("personName");
        repository.addWorkItem(testBug.getID(),testBug);
        repository.addPerson("personName", testPerson);
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertEquals(1, testBug.getAssignee().size());
    }

    @Test
    public void execute_should_AssignStoryToPerson_whenPassedValidArguments(){
        //Arrange
        Story testStory = new StoryImpl("storyName123", "description123456789");
        List<String> testList = new ArrayList<>();
        testList.add("storyName123");
        testList.add(String.valueOf(testStory.getID()));
        testList.add("personName");
        repository.addWorkItem(testStory.getID(),testStory);
        repository.addPerson("personName", testPerson);
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertEquals(1, testStory.getAssignee().size());
    }
}
