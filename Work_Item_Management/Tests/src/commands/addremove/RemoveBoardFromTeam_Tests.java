package commands.addremove;

import com.commands.addremove.RemoveBoardFromTeam;
import com.commands.contracts.Command;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RemoveBoardFromTeam_Tests {
    Command testCommand;
    Repository repository;
    Factory factory;
    Board testBoard;
    Team testTeam;

    @Before
    public void before() {
        repository = new RepositoryImpl();
        factory = new FactoryImpl();
        testCommand = new RemoveBoardFromTeam(repository, factory);
        testBoard = new BoardImpl("boardName");
        testTeam = new TeamImpl("teamName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");

        //Act and Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        testList.add("no");
        testList.add("no");

        //Act and Arrange
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("boardName");
        repository.addBoard("boardName", testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("teamName");
        testList.add("boardName");
        repository.addTeam("teamName", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_findPerson_when_passedCorrectInput() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("boardName");
        testList.add("teamName");
        repository.addBoard("boardName", testBoard);
        repository.addTeam("teamName", testTeam);
        testTeam.addBoard(testBoard);

        //Act and Assert
        testCommand.execute(testList);

        //Assert
        Assert.assertEquals(0, testTeam.getBoards().size());
    }
}
