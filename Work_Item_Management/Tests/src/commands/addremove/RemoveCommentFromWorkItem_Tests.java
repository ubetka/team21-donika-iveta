package commands.addremove;

import com.commands.addremove.AddCommentToWorkItem;
import com.commands.addremove.RemoveCommentFromWorkItem;
import com.commands.contracts.Command;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.models.BoardImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RemoveCommentFromWorkItem_Tests {
    private Command testCommand;
    private Bug testBug;
    private Feedback testFeedback;
    private Story testStory;
    private Board testBoard;


    private Repository repository;
    private Factory factory;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new RemoveCommentFromWorkItem(repository, factory);
        testBoard = new BoardImpl("boardName");
        testBug = new BugImpl("bugNameeeee", "bugDescriptionnnnnnnnn", SeverityType.MAJOR);
        testFeedback = new FeedbackImpl("feedbackNameeee", "feedbackDescriptionnnnnnnnn");
        testStory = new StoryImpl("storyNameeee", "storyDescriptionnnnnnnnn");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        testList.add("no");
        testList.add("no");
        testList.add("no");
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_commentDoesNotExists() {
        // Arrange
        List<String> testList = new ArrayList<>();
        repository.addWorkItem(testBug.getID(), testBug);
        testBug.addComment("comment");
        testList.add("bugNameeeee");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("commentBug");


        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(0, testBug.getComments().size());
    }

    @Test
    public void execute_should_removeCommentToBug_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        repository.addWorkItem(testBug.getID(), testBug);
        testBug.addComment("commentBug");
        testList.add("bugNameeeee");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("commentBug");


        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(0, testBug.getComments().size());
    }

    @Test
    public void execute_should_removeCommentToFeedback_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        repository.addWorkItem(testFeedback.getID(), testFeedback);
        testFeedback.addComment("commentFeedback");
        testList.add("feedbackNameeee");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add("commentFeedback");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(0, testFeedback.getComments().size());
    }

    @Test
    public void execute_should_removeCommentToStory_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        repository.addWorkItem(testStory.getID(), testStory);
        testStory.addComment("commentStory");
        testList.add("storyNameeee");
        testList.add(String.valueOf(testStory.getID()));
        testList.add("commentStory");

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(0, testStory.getComments().size());
    }
}