package commands.addremove;

import com.commands.addremove.AddCommentToWorkItem;
import com.commands.addremove.RemovePersonFromTeam;
import com.commands.addremove.RemoveWorkItemFromBoard;
import com.commands.contracts.Command;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RemoveWorkItemFromBoard_Tests {
    private Command testCommand;
    private Bug testBug;
    private Feedback testFeedback;
    private Story testStory;
    private Board testBoard;
    private Repository repository;
    private Factory factory;


    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new RemoveWorkItemFromBoard(repository, factory);
        testBoard = new BoardImpl("boardName");
        testBug = new BugImpl("bugNameeeee", "bugDescriptionnnnnnnnn", SeverityType.MAJOR);
        testFeedback = new FeedbackImpl("feedbackNameeee", "feedbackDescriptionnnnnnnnn");
        testStory = new StoryImpl("storyNameeee", "storyDescriptionnnnnnnnn");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        testList.add("no");
        testList.add("no");
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardDoesNotExist() {
         //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("workItemName");
        testList.add("workItemID");
        testList.add("boardName");
        repository.addWorkItem(testFeedback.getID(), testFeedback);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_findBug_when_passedCorrectInput() {
        // Arrange
        List<String> testList = new ArrayList<>();
        repository.addBoard("boardName", testBoard);
        repository.addWorkItem(testBug.getID(), testBug);
        testBoard.addWorkItem(testBug);
        testList.add("bugNameeeee");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("boardName");
        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(0, testBoard.listWorkItems().size());
    }
    @Test
    public void execute_should_findFeedback_when_passedCorrectInput() {
        // Arrange
        List<String> testList = new ArrayList<>();
        repository.addBoard("boardName", testBoard);
        repository.addWorkItem(testFeedback.getID(), testFeedback);
        testBoard.addWorkItem(testFeedback);
        testList.add("feedbackNameeee");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add("boardName");
        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(0, testBoard.listWorkItems().size());
    }
    @Test
    public void execute_should_findStory_when_passedCorrectInput() {
        // Arrange
        List<String> testList = new ArrayList<>();
        repository.addBoard("boardName", testBoard);
        repository.addWorkItem(testStory.getID(), testStory);
        testBoard.addWorkItem(testStory);
        testList.add("storyNameeee");
        testList.add(String.valueOf(testStory.getID()));
        testList.add("boardName");
        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(0, testBoard.listWorkItems().size());
    }
}

