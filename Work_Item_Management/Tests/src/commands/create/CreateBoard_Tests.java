package commands.create;

import com.commands.contracts.Command;
import com.commands.create.CreateBoard;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBoard_Tests {
    private Command testCommand;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreateBoard(repository, factory);
        Team testTeam = factory.createTeam("teamName");
        repository.addTeam("teamName", testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("boardName");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("boardName");
        testList.add("teamName");
        testList.add("something");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createBoard_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("boardName");
        testList.add("teamName");
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertEquals(1, repository.getBoard().size());
    }
}
