package commands.create;

import com.commands.contracts.Command;
import com.commands.create.CreateBug;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBug_Tests {
    private Command testCommand;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreateBug(repository, factory);
        Team testTeam = factory.createTeam("teamName");
        Board testBoard = factory.createBoard("boardName");
        repository.addTeam("teamName", testTeam);
        repository.addBoard("boardName", testBoard);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("title");
        testList.add("description");
        testList.add("severity");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("title");
        testList.add("description");
        testList.add("severity");
        testList.add("bugName");
        testList.add("12345");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createBug_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("title12345");
        testList.add("description123456789");
        testList.add("minor");
        testList.add("boardName");
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertEquals(1, repository.getWorkItem().size());
    }
}
