package commands.create;

import com.commands.contracts.Command;
import com.commands.create.CreatePerson;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreatePerson_Tests {
    private Command testCommand;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new CreatePerson(repository, factory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("personName");
        testList.add("age");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_createPerson_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("personName");
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertEquals(1, repository.getPerson().size());
    }
}
