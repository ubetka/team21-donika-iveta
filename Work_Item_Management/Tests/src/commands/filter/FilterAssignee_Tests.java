package commands.filter;

import com.commands.contracts.Command;
import com.commands.filter.FilterAssignee;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Person;
import com.teams.models.PersonImpl;
import com.workitems.contracts.Bug;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterAssignee_Tests {
    private Command testCommand;
    private Person testPerson;
    private Bug testBug;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new FilterAssignee(repository, factory);
        testPerson = new PersonImpl("personName");
        testBug = new BugImpl("bugName123", "description123456789", SeverityType.MAJOR);
        repository.addPerson("personName", testPerson);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("assigneeName");
        testList.add("12345");

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_FilterAssignee_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("personName");
        testPerson.assignWorkItem(testBug);

        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertEquals(1, testPerson.listWorkItems().size());
    }
}
