package commands.filter;

import com.commands.contracts.Command;
import com.commands.filter.FilterStatusAssignee;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Person;
import com.teams.models.PersonImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterStatusAssignee_Tests {
    private Command testCommand;
    private Person testPerson;
    private Bug testBug;
    private Story testStory;
    private Story testStory2;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new FilterStatusAssignee(repository, factory);
        testPerson = new PersonImpl("personName");
        testBug = new BugImpl("bugName123", "description123456789", SeverityType.MAJOR);
        testStory = new StoryImpl("storyName1", "description123456789");
        testStory2 = new StoryImpl("storyName2","description123456789");
        repository.addPerson("personName", testPerson);
        repository.addWorkItem(testStory.getID(), testStory);
        repository.addWorkItem(testBug.getID(), testBug);
        repository.addWorkItem(testStory2.getID(), testStory2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("assigneeName");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("assigneeName");
        testList.add("statusType");
        testList.add("bla");

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_FilterStatusAssignee_whenPassedValidArguments(){
        //Arrange
        testBug.setStatus("Active");
        testStory.setStatus("Done");
        testStory2.setStatus("Done");
        testPerson.assignWorkItem(testBug);
        testPerson.assignWorkItem(testStory);
        testPerson.assignWorkItem(testStory2);
        testBug.setAssignee(testPerson.getName());
        testStory.setAssignee(testPerson.getName());
        testStory.setAssignee(testPerson.getName());
        List<String> testList = new ArrayList<>();
        testList.add("Done");
        testList.add("personName");

        //Act & Assert
        testCommand.execute(testList);
    }
}
