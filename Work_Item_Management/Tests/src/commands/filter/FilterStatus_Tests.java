package commands.filter;

import com.commands.contracts.Command;
import com.commands.filter.FilterStatus;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Person;
import com.workitems.contracts.Bug;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterStatus_Tests {
    private Command testCommand;
    private Repository repository;
    private Bug testBug;
    private Bug testBug2;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new FilterStatus(repository, factory);
        testBug = new BugImpl("bugName123", "description123456789", SeverityType.MAJOR);
        testBug2 = new BugImpl("bugName456", "description123456789", SeverityType.CRITICAL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("statusType");
        testList.add("12345");

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_FilterStatus_whenPassedValidArguments(){
        //Arrange
        repository.addWorkItem(testBug.getID(), testBug);
        repository.addWorkItem(testBug2.getID(), testBug2);
        testBug.setStatus("Fixed");
        testBug2.setStatus("Fixed");
        List<String> testList = new ArrayList<>();
        testList.add("Fixed");

        //Act & Assert
        testCommand.execute(testList);
    }
}
