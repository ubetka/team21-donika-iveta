package commands.set;

import com.commands.contracts.Command;
import com.commands.set.SetRating;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SetRating_Tests {
    private Command testCommand;
    private Feedback testFeedback;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SetRating(repository, factory);
        testFeedback = new FeedbackImpl("newFeedback123", "description1234567890");
        repository.addWorkItem(testFeedback.getID(), testFeedback);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("workItemName");
        testList.add("workItemID");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("workItemName");
        testList.add("workItemID");
        testList.add("personName");
        testList.add("12345");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongWorkItemTypeStory() {
        Story testStory = new StoryImpl("newStory123", "description1234567890");
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("newStory123");
        testList.add(String.valueOf(testStory.getID()));
        testList.add(String.valueOf(4));

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongWorkItemTypeBug() {
        Bug testBug = new BugImpl("bugName123", "description123456", SeverityType.MAJOR);
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add(String.valueOf(4));

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("newFeedback123");
        testList.add(String.valueOf(testFeedback.getID()));

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedGreaterRatingValue() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("newFeedback123");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add(String.valueOf(11));
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedSmallerRatingValue() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("newFeedback123");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add(String.valueOf(-1));
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_whenNameAndIdDoNotMatch(){
        //Arrange
        Bug testBug = new BugImpl("bugName123", "description123456", SeverityType.MAJOR);
        repository.addWorkItem(testBug.getID(), testBug);
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("newFeedback123");
        testList.add(String.valueOf(2));
        testList.add(String.valueOf(5));
        //Act
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_SetRating_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("newFeedback123");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add(String.valueOf(5));
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertTrue(Integer.parseInt(testList.get(3)) >= 1
        && Integer.parseInt(testList.get(3)) <= 10);
    }
}
