package commands.set;

import com.commands.contracts.Command;
import com.commands.set.SetSize;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.enums.SizeType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SetSize_Tests {
    private Command testCommand;
    private Story testStory;
    private Repository repository;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SetSize(repository, factory);
        testStory = new StoryImpl("newStory12345", "description1234567890");
        repository.addWorkItem(testStory.getID(), testStory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("workItemName");
        testList.add("workItemID");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("workItemName");
        testList.add("workItemID");
        testList.add("personName");
        testList.add("12345");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongWorkItemTypeFeedback() {
        Feedback testFeedback = new FeedbackImpl("feedbackName", "description123456");
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("feedbackName");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add(String.valueOf(4));

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongWorkItemTypeBug() {
        Bug testBug = new BugImpl("bugName123", "description123456", SeverityType.MAJOR);
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add(String.valueOf(4));

        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("newStory12345");
        testList.add(String.valueOf(testStory.getID()));

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_whenNameAndIdDoNotMatch(){
        //Arrange
        Bug testBug = new BugImpl("bugName123", "description123456", SeverityType.MAJOR);
        repository.addWorkItem(testBug.getID(), testBug);
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("newStory12345");
        testList.add(String.valueOf(testBug.getID()));
        testList.add(String.valueOf(5));
        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongSizeType() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("workItemName");
        testList.add("workItemID");
        testList.add("minor");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_SetSize_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("newStory12345");
        testList.add(String.valueOf(testStory.getID()));
        testList.add("medium");
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertTrue(testList.get(3).equalsIgnoreCase(String.valueOf(SizeType.MEDIUM)));
    }
}
