package commands.set;

import com.commands.contracts.Command;
import com.commands.set.SetSize;
import com.commands.set.SetStatus;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.enums.StatusTypeBug;
import com.workitems.enums.StatusTypeFeedback;
import com.workitems.enums.StatusTypeStory;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SetStatus_Tests {
    private Command testCommand;
    private Repository repository;
    private Bug testBug;
    private Story testStory;
    private Feedback testFeedback;

    @Before
    public void before() {
        Factory factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SetStatus(repository, factory);
        testStory = new StoryImpl("newStory12345", "description1234567890");
        testBug = new BugImpl("bugName123", "description1234567890", SeverityType.CRITICAL);
        testFeedback = new FeedbackImpl("feedbackName", "description1234567890");
        repository.addWorkItem(testStory.getID(), testStory);
        repository.addWorkItem(testBug.getID(), testBug);
        repository.addWorkItem(testFeedback.getID(), testFeedback);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("workItemName");
        testList.add("workItemID");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("workItemName");
        testList.add("workItemID");
        testList.add("newStatus");
        testList.add("personName");
        //Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_idDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("bugName123");
        testList.add(String.valueOf(4));
        testList.add(String.valueOf(StatusTypeBug.FIXED));
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_whenNameAndIdDoNotMatch(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("bugName123");
        testList.add(String.valueOf(testStory.getID()));
        testList.add(String.valueOf(5));
        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongStatusTypeBug() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("abc");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongStatusTypeFeedback() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("feedbackName");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add("abc");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongStatusTypeStory() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("newStory12345");
        testList.add(String.valueOf(testStory.getID()));
        testList.add("abc");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedWrongWorkItem() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("wrong");
        testList.add("feedbackName");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add("scheduled");
        //Act, Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_SetStatusBug_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        testList.add("bugName123");
        testList.add(String.valueOf(testBug.getID()));
        testList.add("active");
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertTrue(testList.get(3).equalsIgnoreCase(String.valueOf(StatusTypeBug.ACTIVE)));
    }

    @Test
    public void execute_should_SetStatusFeedback_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        testList.add("feedbackName");
        testList.add(String.valueOf(testFeedback.getID()));
        testList.add("scheduled");
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertTrue(testList.get(3).equalsIgnoreCase(String.valueOf(StatusTypeFeedback.SCHEDULED)));
    }

    @Test
    public void execute_should_SetStatusStory_whenPassedValidArguments(){
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("story");
        testList.add("newStory12345");
        testList.add(String.valueOf(testStory.getID()));
        testList.add("inprogress");
        //Act
        testCommand.execute(testList);
        //Arrange
        Assert.assertTrue(testList.get(3).equalsIgnoreCase(String.valueOf(StatusTypeStory.INPROGRESS)));
    }
}
