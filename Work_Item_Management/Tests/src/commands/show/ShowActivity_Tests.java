package commands.show;

import com.commands.contracts.Command;
import com.commands.show.ShowActivity;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import com.workitems.contracts.Bug;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowActivity_Tests {
    private Command testCommand;
    private Repository repository;
    private Factory factory;
    private Bug testBug;
    private Board testBoard;
    private Person testPerson;
    private Team testTeam;

    //    private Person testPerson;
    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ShowActivity(repository, factory);
        testBug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR);
        testPerson = new PersonImpl("personName");
        testBoard = new BoardImpl("boardName");
        testTeam = new TeamImpl("teamName");
//        testPerson = new PersonImpl("personName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        testList.add("no");
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showActivityPerson_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("person");
        testList.add(testPerson.getName());
        repository.addPerson("personName", testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showActivityTeam_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("team");
        testList.add(testTeam.getName());
        repository.addTeam("teamName", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showActivityBoard_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("board");
        testList.add(testBoard.getName());
        repository.addBoard("boardName", testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_worngObjectToShow() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("wrongObject");
        testList.add(testBoard.getName());
        repository.addBoard("boardName", testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showBoardActivity_when_passedValidModel() {
        // Arrange
        Board board = new BoardImpl("boardName");
        board.addWorkItem(testBug);

        // Act
        String activity = board.showActivity();

        // Assert
        Assert.assertEquals(activity, String.format(("Board: boardName\n" + "History:\n" +
                "bug \"bugName12345\" with ID: %d successfully added to board \"boardName\".\n"), testBug.getID()));
    }

    @Test
    public void find_should_showTeamActivity_when_passedValidModel() {
        // Arrange
        Team team = new TeamImpl("teamName");
        team.addBoard(testBoard);

        // Act
        String activity = team.showTeamActivity();

        // Assert
        Assert.assertEquals(activity, "Team: teamName\n" +
                "Team Activity:\n" +
                "Board \"boardName\" created in \"teamName\" team.\n");
    }

    @Test
    public void find_should_showPersonActivity_when_passedValidModel() {
        // Arrange
        Person person = new PersonImpl("personName");
        person.assignWorkItem(testBug);

        // Act
        String activity = person.showActivity();

        // Assert
        Assert.assertEquals(activity, String.format(("Person's name: personName\n" +
                "Activity History:\n" +
                "bug with name \"bugName12345\" and ID: %d assigned to personName.\n"), testBug.getID()));
    }
}
