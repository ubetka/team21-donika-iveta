package commands.show;

import com.commands.contracts.Command;
import com.commands.show.ShowActivity;
import com.commands.show.ShowAllForTeam;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import com.workitems.contracts.Bug;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllForTeam_Tests {
    private Command testCommand;
    private Repository repository;
    private Factory factory;
    private Bug testBug;
    private Board testBoard;
    private Person testPerson;
    private Team testTeam;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ShowAllForTeam(repository, factory);
        testBug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR);
        testPerson = new PersonImpl("personName");
        testBoard = new BoardImpl("boardName");
        testTeam = new TeamImpl("teamName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        testList.add("no");
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void find_should_showAllMembersInTeam_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        testList.add("members");
        repository.addTeam("teamName", testTeam);
        testTeam.addMember(testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showAllBoardsInTeam_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        testList.add("boards");
        repository.addTeam("teamName", testTeam);
        testTeam.addBoard(testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_worngObjectToShow() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testTeam.getName());
        testList.add("wrong");
        repository.addTeam("teamName", testTeam);
        testTeam.addBoard(testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }





    @Test
    public void showMembers_should_showMembers_when_MemberIsAdded() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        team.addMember(testPerson);

        //Assert
        Assert.assertEquals("personName", team.showMembers());
    }
    @Test
    public void showBoards_should_showBoards_when_BoardIsAdded() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        team.addBoard(testBoard);

        //Assert
        Assert.assertEquals("boardName", team.showBoards());
    }

}
