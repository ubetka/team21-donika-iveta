package commands.show;

import com.commands.contracts.Command;
import com.commands.show.ShowAll;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAll_Tests {
    private Command testCommand;
    private Repository repository;
    private Factory factory;
    private Bug testBug;
    private Feedback testFeedback;
    private Story testStory;
    private Board testBoard;
    private Person testPerson;
    private Team testTeam;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new ShowAll(repository, factory);
        testBug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR);
        testFeedback = new FeedbackImpl("feedbackName12345","feedbackDescription12345");
        testStory = new StoryImpl("storyName12345","storyDescription12345");
        testPerson = new PersonImpl("personName");
        testBoard = new BoardImpl("boardName");
        testTeam = new TeamImpl("teamName");
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void find_should_showAllPeople_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("people");
        repository.addPerson("personName", testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showAllTeams_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("teams");
        repository.addTeam("teamName", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showAllWorkItems_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("workitems");
        repository.addWorkItem(testBug.getID(), testBug);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showAllBug_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("bug");
        repository.addWorkItem(testBug.getID(), testBug);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void find_should_showAllFeedback_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("feedback");
        repository.addWorkItem(testFeedback.getID(), testFeedback);

        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void find_should_showAllStory_when_passedValidModel() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("story");
        repository.addWorkItem(testStory.getID(), testStory);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_worngObjectToShow() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("wrong");
        repository.addWorkItem(testStory.getID(), testStory);

        // Act & Assert
        testCommand.execute(testList);
    }


}
