package commands.sort;

import com.commands.contracts.Command;
import com.commands.sort.SortByPriority;
import com.commands.sort.SortByRating;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.PriorityType;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SortByRating_Tests {
    private Command testCommand;
    private Repository repository;
    private Factory factory;

    private Feedback testFeedback;
    private Feedback testFeedback2;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortByRating(repository, factory);
        testFeedback = new FeedbackImpl("feedbackName12345","feedbackDescription12345");
        testFeedback2 = new FeedbackImpl("feedbackName123456","feedbackDescription123456");
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void sortByRating_should_showSortedList_when_SortBy() {
        List<String> testList = new ArrayList<>();
        // Arrange
        repository.addWorkItem(testFeedback.getID(), testFeedback);
        repository.addWorkItem(testFeedback2.getID(), testFeedback2);
        testFeedback.setRating(9);
        testFeedback.setRating(5);

        // Act & Assert
        testCommand.execute(testList);
    }

}