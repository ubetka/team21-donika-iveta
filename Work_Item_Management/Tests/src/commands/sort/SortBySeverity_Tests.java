package commands.sort;

import com.commands.contracts.Command;
import com.commands.sort.SortByPriority;
import com.commands.sort.SortBySeverity;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.StoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SortBySeverity_Tests {
    private Command testCommand;
    private Repository repository;
    private Factory factory;
    private Bug testBug;
    private Bug testBug2;
    private Story testStory;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortBySeverity(repository, factory);
        testBug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR);
        testBug2 = new BugImpl("bugName123456", "bugDescription123456", SeverityType.MAJOR);
        testStory = new StoryImpl("storyName12345","storyDescription12345");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void sortBySeverity_should_showSortedList_when_SortBy() {
        List<String> testList = new ArrayList<>();
        // Arrange
        repository.addWorkItem(testBug.getID(), testBug);
        repository.addWorkItem(testBug2.getID(), testBug2);

        // Act & Assert
        testCommand.execute(testList);
    }

}
