package commands.sort;

import com.commands.contracts.Command;
import com.commands.sort.SortByPriority;
import com.commands.sort.SortBySize;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Story;
import com.workitems.enums.PriorityType;
import com.workitems.enums.SeverityType;
import com.workitems.enums.SizeType;
import com.workitems.models.BugImpl;
import com.workitems.models.StoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SortBySize_Tests {
    private Command testCommand;
    private Repository repository;
    private Factory factory;
    private Story testStory;
    private Story testStory2;

    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortBySize(repository, factory);
        testStory = new StoryImpl("storyName12345","storyDescription12345");
        testStory2 = new StoryImpl("storyName123456","storyDescription123456");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void sortBySize_should_showSortedList_when_SortBy() {
        List<String> testList = new ArrayList<>();
        // Arrange
        repository.addWorkItem(testStory.getID(), testStory);
        repository.addWorkItem(testStory2.getID(), testStory2);
        testStory2.setSize(SizeType.LARGE);

        // Act & Assert
        testCommand.execute(testList);
    }

}
