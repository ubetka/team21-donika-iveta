package commands.sort;

import com.commands.contracts.Command;
import com.commands.sort.SortByPriority;
import com.commands.sort.SortByTitle;
import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.PriorityType;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SortByTitle_Tests {
    private Command testCommand;
    private Repository repository;
    private Factory factory;
    private Bug testBug;
    private Feedback testFeedback;
    private Story testStory;


    @Before
    public void before() {
        factory = new FactoryImpl();
        repository = new RepositoryImpl();
        testCommand = new SortByTitle(repository, factory);
        testBug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR);
        testFeedback = new FeedbackImpl("feedbackName12345","feedbackDescription12345");
        testStory = new StoryImpl("storyName12345","storyDescription12345");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("no");

        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void sortByTitle_should_showSortedList_when_SortBy() {
        List<String> testList = new ArrayList<>();
        // Arrange
        repository.addWorkItem(testBug.getID(), testBug);
        repository.addWorkItem(testFeedback.getID(), testFeedback);
        repository.addWorkItem(testStory.getID(), testStory);

        // Act & Assert
        testCommand.execute(testList);
    }

}
