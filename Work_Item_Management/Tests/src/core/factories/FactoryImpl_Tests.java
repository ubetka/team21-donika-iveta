package core.factories;

import com.core.contracts.Factory;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;

import com.workitems.enums.SeverityType;
import org.junit.Assert;
import org.junit.Test;

public class FactoryImpl_Tests {
    @Test
    public void createStory_should_createNewBug_when_withValidArguments() {
        //Act and Arrange
        Factory factory = new FactoryImpl();
        Bug bug = factory.createBug("bugNameeeeee", "bugDescriptionnnnn", "major");

        //Assert
        Assert.assertEquals(SeverityType.MAJOR, bug.getSeverity());

    }
}
