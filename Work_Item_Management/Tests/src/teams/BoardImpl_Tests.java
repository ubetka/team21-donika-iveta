package teams;

import com.core.RepositoryImpl;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.teams.models.BoardImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.WorkItem;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl_Tests {
    private Bug testBug;

    @Before
    public void before() {
        testBug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithLongerLength() {
        // Arrange, Act, Assert
        Board board = new BoardImpl("boardBoardName");
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithShorterLength() {
        // Arrange, Act, Assert
        Board board = new BoardImpl("bd");
    }

    @Test
    public void constructor_should_returnBoard_when_valuesAreValid() {
        // Arrange, Act
        Board board = new BoardImpl("boardName");

        // Assert
        Assert.assertEquals(board.getName(), "boardName");
    }

    @Test
    public void listWorkItems_should_returnShallowCopy() {
        // Arrange
        Board board = new BoardImpl("boardName");

        // Act
        List<WorkItem> supposedShallowCopy = board.listWorkItems();
        board.addWorkItem(testBug);

        // Assert
        Assert.assertEquals(0, supposedShallowCopy.size());
    }


    @Test
    public void find_should_returnBug_when_passedValidModel() {
        // Arrange
        Board board = new BoardImpl("boardName");
        board.addWorkItem(testBug);
        // Act
        Bug found = (Bug) board.listWorkItems().get(0);

        // Assert
        Assert.assertEquals(testBug.getID(), found.getID());
    }

    @Test
    public void execute_should_showBoard_whenInputCorrect() {
        // Arrange, Act
        Repository repository = new RepositoryImpl();
        Board board = new BoardImpl("boardName");
        repository.addWorkItem(testBug.getID(), testBug);
        repository.addBoard(board.getName(), board);
        board.addWorkItem(testBug);

        // Assert
        Assert.assertEquals(board.toString(), String.format("Board name: boardName\n" +
                "Board's activity:\n" +
                "bug \"bugName12345\" with ID: %d successfully added to board \"boardName\".;\n" +
                "Work items:\n" +
                "bug \"bugName12345\"\n" +
                "Description: bugDescription12345 \n" +
                "Comments:\n" +
                "Activity history:\n\n", testBug.getID()));

    }

}

