package teams;

import com.core.RepositoryImpl;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.WorkItem;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class PersonImpl_Tests {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithLongerLength() {
        // Arrange, Act, Assert
        Person person = new PersonImpl("personName123456789");
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithShorterLength() {
        // Arrange, Act, Assert
        Person person = new PersonImpl("p1");
    }

    @Test
    public void constructor_should_returnBoard_when_valuesAreValid() {
        // Arrange, Act
        Person person = new PersonImpl("personName");

        // Assert
        Assert.assertEquals(person.getName(), "personName");
    }
    @Test
    public void execute_should_showPersonwhenInputCorrect() {
        // Arrange, Act
        Repository repository = new RepositoryImpl();
        Person person = new PersonImpl("personName");

        repository.addPerson(person.getName(), person);
        Bug testBug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );
        repository.addWorkItem(testBug.getID(), testBug);
        person.assignWorkItem(testBug);

        // Assert
        Assert.assertEquals(person.toString(), String.format("Persons's name: personName\n" +
                "Person's activity:\n" +
                "bug with name \"bugName12345\" and ID: %d assigned to personName.;\n" +
                "Work items:\n" +
                "bug \"bugName12345\"\n" +
                "Description: bugDescription12345 \n" +
                "Comments:\n" +
                "Activity history:\n\n", testBug.getID()));

    }


}

