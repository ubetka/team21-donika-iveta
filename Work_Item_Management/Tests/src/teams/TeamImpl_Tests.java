package teams;

import com.commands.common.CommandValidator;
import com.core.RepositoryImpl;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.StringJoiner;

public class TeamImpl_Tests {
    private Person testPerson;
    private Board testBoard;

    @Before
    public void before() {
        testPerson = new PersonImpl("personName");
        testBoard = new BoardImpl("boardName");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithLongerLength() {
        // Arrange, Act, Assert
        Team team = new TeamImpl("teamName123456789");
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithShorterLength() {
        // Arrange, Act, Assert
        Team team = new TeamImpl("t1");
    }

    @Test
    public void constructor_should_returnBoard_when_valuesAreValid() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");

        // Assert
        Assert.assertEquals(team.getName(), "teamName");
    }


    @Test(expected = IllegalArgumentException.class)
    public void showBoards_should_throwError_when_listOfBoardsIsEmpty() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        //Assert
        team.showBoards();
    }


    @Test(expected = IllegalArgumentException.class)
    public void showMembers_should_throwError_when_listOfMembersIsEmpty() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        //Assert
        team.showMembers();
    }


    @Test
    public void getMembers_should_addMember_when_MemberIsAdded() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        team.addMember(testPerson);

        //Assert
        Assert.assertEquals(1, team.getMembers().size());
    }

    @Test
    public void showMembers_should_showMembers_when_MemberIsAdded() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        team.addMember(testPerson);

        //Assert
        Assert.assertEquals("personName", team.showMembers());
    }

    @Test
    public void getBoards_should_addBoards_when_BoardIsAdded() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        team.addBoard(testBoard);

        //Assert
        Assert.assertEquals(1, team.getBoards().size());
    }

    @Test
    public void showBoards_should_showBoards_when_BoardIsAdded() {
        // Arrange, Act
        Team team = new TeamImpl("teamName");
        team.addBoard(testBoard);

        //Assert
        Assert.assertEquals("boardName", team.showBoards());
    }

    @Test
    public void execute_should_showPersonwhenInputCorrect() {
        // Arrange, Act
        Repository repository = new RepositoryImpl();
        Team team = new TeamImpl("teamName");
        Person person = new PersonImpl("personName");
        Board board = new BoardImpl("boardName");
        repository.addTeam(team.getName(), team);
        team.addMember(person);
        team.addBoard(board);



        // Assert
        Assert.assertEquals(team.toString(),("Team name: teamName\n" +
                "Members: personName; \n" +
                "Boards: boardName; \n" +
                "Team Activity:\n" +
                "Person's name: personName\n" +
                "Activity History:\n" +
                "\n"));

    }

}
