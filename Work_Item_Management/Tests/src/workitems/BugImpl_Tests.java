package workitems;

import com.core.RepositoryImpl;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.core.factories.FactoryImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;

import com.workitems.models.FeedbackImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class BugImpl_Tests {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithLongerLength() {
        // Arrange
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < 51; i++) {
            name.append("n");
        }
        // Act, Assert
        Bug bug = new BugImpl(name.toString(), "bugDescription12345", SeverityType.MINOR);
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithShorterLength() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("bugName", "bugDescription12345", SeverityType.MINOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionIsWithLongerLength() {
        // Arrange
        StringBuilder description = new StringBuilder();
        for (int i = 0; i < 501; i++) {
            description.append("d");
        }

        // Act, Assert

        Bug bug = new BugImpl("bugName123456789101112", description.toString(), SeverityType.MINOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionIsWithLessLength() {
        // Arrange, Act, Assert

        Bug bug = new BugImpl("bugName123456789101112", "bugDescr", SeverityType.MINOR);
    }

    @Test
    public void constructor_should_returnBug_when_valuesAreValid() {
        // Arrange, Act, Assert
        Bug bug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );

    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_commentAlreadytExists() {
        // Arrange, Act, Assert
        Repository repository = new RepositoryImpl();
        Bug bug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );
        repository.addWorkItem(bug.getID(), bug);
        bug.addComment("commentBug");
        bug.addComment("commentBug");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_commentDoesNotExists(){
        // Arrange, Act, Assert
        Repository repository = new RepositoryImpl();
        Bug bug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );
        repository.addWorkItem(bug.getID(), bug);
        bug.addComment("comment");
        bug.removeComment("commentBug");
    }

    @Test
    public void execute_should_showCommentOfBug_whenInputCorrect() {
        // Arrange, Act
        Repository repository = new RepositoryImpl();
        Bug bug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );
        repository.addWorkItem(bug.getID(), bug);
        bug.addComment("commentBug");
        bug.addComment("commentBug2");
        bug.addComment("commentBug3");

        // Assert
        Assert.assertEquals(bug.showComments(), "commentBug, commentBug2, commentBug3");
    }

    @Test
    public void execute_should_showDesciptionOfBug_whenInputCorrect() {
        // Arrange, Act
        Repository repository = new RepositoryImpl();
        Bug bug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );
        repository.addWorkItem(bug.getID(), bug);

        // Assert
        Assert.assertEquals(bug.getDescription(), "bugDescription12345");
    }
    @Test
    public void execute_should_showHistoryBug_whenInputCorrect() {
        // Arrange, Act
        Repository repository = new RepositoryImpl();
        Bug bug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );
        repository.addWorkItem(bug.getID(), bug);
        bug.addComment("commentBug");
        bug.removeComment("commentBug");

        // Assert
        Assert.assertEquals(bug.showHistory().toString().replace("[", "")
                        .replace("]", ""),
                "Comment successfully added., Comment successfully removed.");
    }


    @Test
    public void execute_should_showBug_whenInputCorrect() {
        // Arrange, Act
        Repository repository = new RepositoryImpl();
        Bug bug = new BugImpl("bugName12345", "bugDescription12345", SeverityType.MINOR );
        repository.addWorkItem(bug.getID(), bug);
        bug.addComment("commentBug");

        // Assert
        Assert.assertEquals(bug.toString(), "bug \"bugName12345\"\n" +
                "Description: bugDescription12345 \n" + "Comments:\n"+"\"commentBug\";\n"+"Activity history:\n"
                +"Comment successfully added.;\n");
    }

}

