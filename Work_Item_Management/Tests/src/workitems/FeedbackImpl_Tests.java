package workitems;

import com.workitems.contracts.Feedback;
import com.workitems.models.FeedbackImpl;
import org.junit.Assert;
import org.junit.Test;

public class FeedbackImpl_Tests {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithLongerLength() {
        // Arrange
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < 51; i++) {
            name.append("n");
        }
        // Act, Assert
        Feedback feedback = new FeedbackImpl(name.toString(), "feedbackDescription12345");
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithShorterLength() {
        // Arrange, Act, Assert
        Feedback feedback = new FeedbackImpl("name", "feedbackDescription12345");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionIsWithLongerLength() {
        // Arrange
        StringBuilder description = new StringBuilder();
        for (int i = 0; i < 501; i++) {
            description.append("d");
        }

        // Act, Assert

        Feedback feedback = new FeedbackImpl("feedbackName12345", description.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionIsWithLessLength() {
        // Arrange, Act, Assert

        Feedback feedback = new FeedbackImpl("feedbackName12345", "descr");
    }

    @Test
    public void constructor_should_returnFeedback_when_valuesAreValid() {
        // Arrange, Act, Assert
        Feedback feedback = new FeedbackImpl("feedbackName12345","feedbackDescription12345");

    }

    @Test
    public void getStatus_should_returnStatus_when_valuesAreValid() {
        // Arrange, Act
        Feedback feedback = new FeedbackImpl("feedbackName12345","feedbackDescription12345");
        feedback.setStatus("scheduled");

        //Assert
        Assert.assertTrue("scheduled".equalsIgnoreCase(feedback.getStatus().toString()));

    }

}

