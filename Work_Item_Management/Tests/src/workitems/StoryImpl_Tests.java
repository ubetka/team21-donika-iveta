package workitems;

import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;
import org.junit.Assert;
import org.junit.Test;

public class StoryImpl_Tests {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithLongerLength() {
        // Arrange
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < 51; i++) {
            name.append("n");
        }
        // Act, Assert
        Story story = new StoryImpl(name.toString(), "storyDescription12345");
    }


    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsWithShorterLength() {
        // Arrange, Act, Assert
        Feedback feedback = new FeedbackImpl("name", "storyDescription12345");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionIsWithLongerLength() {
        // Arrange
        StringBuilder description = new StringBuilder();
        for (int i = 0; i < 501; i++) {
            description.append("d");
        }

        // Act, Assert

        Story story = new StoryImpl("storyName12345", description.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionIsWithLessLength() {
        // Arrange, Act, Assert

        Story story = new StoryImpl("storyName12345", "descr");
    }

    @Test
    public void constructor_should_returnFeedback_when_valuesAreValid() {
        // Arrange, Act, Assert
        Story story = new StoryImpl("storyName12345","storyDescription12345");

    }

}
