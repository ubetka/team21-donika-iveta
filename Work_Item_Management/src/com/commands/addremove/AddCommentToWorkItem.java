package com.commands.addremove;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.WorkItem;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class AddCommentToWorkItem implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private final Repository repository;
    private final Factory factory;

    public AddCommentToWorkItem(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemName = parameters.get(0);
        int workItemID = Integer.parseInt(parameters.get(1));
        String comment = parameters.get(2);

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        if (!repository.getWorkItem().get(workItemID).getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }
        CommandValidator.checkIfItemAlreadyExistsInList(repository.getWorkItem().get(workItemID).getComments(), comment,
                COMMENT_NOT_FOUND);

        WorkItem workItem = repository.getWorkItem().get(workItemID);

        workItem.addComment(comment);

        return String.format(COMMENT_ADDED_TO_WORKITEM, workItem.getItemType(), workItemName);
    }
}
