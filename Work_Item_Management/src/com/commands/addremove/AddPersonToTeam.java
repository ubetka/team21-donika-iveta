package com.commands.addremove;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Person;
import com.teams.contracts.Team;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class AddPersonToTeam implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Repository repository;
    private final Factory factory;

    public AddPersonToTeam(Repository repository, Factory factory){
        this.repository = repository;
        this.factory = factory;
    }

    public String execute (List<String>parameters){
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String personName = parameters.get(0);
        String teamName = parameters.get(1);

        CommandValidator.checkIfItemExistsInRepository(repository.getPerson(), personName,
                String.format(PERSON_NOT_FOUND, personName));
        CommandValidator.checkIfItemExistsInRepository(repository.getTeam(), teamName,
                String.format(TEAM_NOT_FOUND, teamName));

        Team team = repository.getTeam().get(teamName);
        Person person = repository.getPerson().get(personName);

        team.addMember(person);

        return String.format(MEMBER_ADDED_TO_TEAM, personName, teamName);
    }
}
