package com.commands.addremove;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.Bug;
import com.workitems.contracts.WorkItem;

import java.util.Arrays;
import java.util.List;

import static com.commands.common.CommandConstants.*;

public class AddStepsToReproduceToBug implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private final Repository repository;
    private final Factory factory;

    public AddStepsToReproduceToBug (Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemName = parameters.get(0);
        int workItemID = Integer.parseInt(parameters.get(1));
        List<String> stepsToReproduce = Arrays.asList(parameters.get(2).split("[.]"));

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        WorkItem workItem = repository.getWorkItem().get(workItemID);
        CommandValidator.checkWorkItemType(workItem.getItemType(), EXPECTED_TYPE_BUG);
        if (!repository.getWorkItem().get(workItemID).getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }

        workItem = (Bug)workItem;
        ((Bug) workItem).addStepsToReproduce(stepsToReproduce);

        return String.format(STEPSTOREPRODUCE_ADDED_TO_BUG, workItemName);
    }
}
