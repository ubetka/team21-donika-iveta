package com.commands.addremove;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Person;
import com.workitems.contracts.BugAndStory;
import com.workitems.contracts.WorkItem;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class AssignToPerson implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private final Repository repository;
    private final Factory factory;

    public AssignToPerson(Repository repository, Factory factory){
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters){
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String assignmentName = parameters.get(0);
        int assignmentID = Integer.parseInt(parameters.get(1));
        String personName = parameters.get(2);

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), assignmentID,
                String.format(ID_DOES_NOT_EXIST, assignmentName, assignmentID));
        CommandValidator.checkIfItemExistsInRepository(repository.getPerson(), personName,
                String.format(PERSON_NOT_FOUND, personName));

        WorkItem assignment = repository.getWorkItem().get(assignmentID);

        CommandValidator.checkTwoWorkItemTypes(String.valueOf(assignment.getItemType()), EXPECTED_TYPE_BUG, EXPECTED_TYPE_STORY);

        Person person = repository.getPerson().get(personName);

        person.assignWorkItem(assignment);

        BugAndStory bugAndStory = (BugAndStory) repository.getWorkItem().get(assignmentID);
        bugAndStory.setAssignee(personName);

        return String.format(WORKITEM_ASSIGNED_TO_PERSON, bugAndStory.getItemType(), assignmentName, assignmentID, personName);
        //TODO try-catch if the workitem is feedback
    }
}
