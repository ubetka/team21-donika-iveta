package com.commands.addremove;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.teams.contracts.Team;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class RemoveBoardFromTeam implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Repository repository;
    private final Factory factory;

    public RemoveBoardFromTeam (Repository repository, Factory factory){
        this.repository = repository;
        this.factory = factory;
    }

    public String execute (List<String> parameters){
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String boardName = parameters.get(0);
        String teamName = parameters.get(1);

        CommandValidator.checkIfItemExistsInRepository(repository.getTeam(), teamName,
                String.format(TEAM_NOT_FOUND, teamName));
        CommandValidator.checkIfItemExistsInRepository(repository.getBoard(), boardName,
                String.format(BOARD_NOT_FOUND, boardName));

        Board board = repository.getBoard().get(boardName);
        Team team = repository.getTeam().get(teamName);

        team.removeBoard(board);
        repository.removeBoard(boardName);

        return String.format(BOARD_REMOVED_FROM_TEAM, boardName, teamName);
    }
}
