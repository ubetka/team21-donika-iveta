package com.commands.addremove;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Person;
import com.teams.contracts.Team;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class RemovePersonFromTeam implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Repository repository;
    private final Factory factory;

    public RemovePersonFromTeam(Repository repository, Factory factory){
        this.repository = repository;
        this.factory = factory;
    }

    public String execute (List<String> parameters){
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String personName = parameters.get(0);
        String teamName = parameters.get(1);

        CommandValidator.checkIfItemExistsInRepository(repository.getPerson(), personName,
                String.format(PERSON_NOT_FOUND, personName));
        CommandValidator.checkIfItemExistsInRepository(repository.getTeam(), teamName,
                String.format(TEAM_NOT_FOUND, teamName));

        Person person = repository.getPerson().get(personName);
        Team team = repository.getTeam().get(teamName);

        team.removeMember(person);

        return String.format(MEMBER_REMOVED_FROM_TEAM, personName, teamName);
    }
}
