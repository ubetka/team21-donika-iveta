package com.commands.addremove;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.workitems.contracts.WorkItem;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class RemoveWorkItemFromBoard implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private final Repository repository;
    private final Factory factory;

    public RemoveWorkItemFromBoard(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemName = parameters.get(0);
        int workItemID = Integer.parseInt(parameters.get(1));
        String boardName = parameters.get(2);

        CommandValidator.checkIfItemExistsInRepository(repository.getBoard(), boardName,
                String.format(BOARD_NOT_FOUND, boardName));
        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        if (!repository.getWorkItem().get(workItemID).getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }
        WorkItem workItem = repository.getWorkItem().get(workItemID);
        CommandValidator.checkIfItemExistsInList(repository.getBoard().get(boardName).listWorkItems(), workItem,
                String.format(ITEM_NOT_FOUND, workItem.getItemType(), workItemName, boardName));
        Board board = repository.getBoard().get(boardName);

        board.removeWorkItems(workItem);
        repository.removeWorkItem(workItemID);

        return String.format(WORKITEM_REMOVED_FROM_BOARD, workItem.getItemType(), workItemName, workItemID, boardName);
    }
}
