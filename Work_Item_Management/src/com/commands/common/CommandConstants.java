package com.commands.common;

public class CommandConstants {
    // Error messages
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d.";

    public static final String NAME_EXISTS_ERROR_MESSAGE = "Name %s already exists.";
    public static final String TEAM_EXISTS_ERROR_MESSAGE = "Team \"%s\" already exists.";
    public static final String BOARD_EXISTS_ERROR_MESSAGE = "Board \"%s\" already exists.";
    public static final String ITEM_EXISTS_ERROR_MESSAGE = "%s \"%s\" already exists in %s.";
    public static final String COMMENT_EXISTS_ERROR_MESSAGE = "Comment already exists.";
    public static final String WORKITEM_ASSIGNED_ERROR_MESSAGE = "%s with name \"%s\" and ID: %d already assigned to %s.";

    public static final String WORKITEM_NOT_ASSIGNED = "%s with name \"%s\" and ID: %d not assigned to %s.";
    public static final String WORKITEM_AND_ID_DO_NOT_MATCH = "Name \"%s\" and ID: %d do not match.";

    public static final String INVALID_COMMAND_ERROR_MESSAGE = "Invalid command.";

    public static final String PERSON_CREATED_SUCCESS_MESSAGE = "Person %s created.";
    public static final String TEAM_CREATED_SUCCESS_MESSAGE = "Team \"%s\" created.";
    public static final String BOARD_CREATED_SUCCESS_MESSAGE = "Board \"%s\" created in \"%s\" team.";
    public static final String BUG_CREATED_SUCCESS_MESSAGE = "Bug \"%s\" with ID: %d created in \"%s\" board.";
    public static final String FEEDBACK_CREATED_SUCCESS_MESSAGE = "Feedback \"%s\" with ID: %d created in \"%s\" board.";
    public static final String STORY_CREATED_SUCCESS_MESSAGE = "Story \"%s\" with ID: %d created in \"%s\" board.";

    public static final String PERSON_NOT_FOUND = "Person %s doesn't exist.";
    public static final String TEAM_NOT_FOUND = "Team \"%s\" doesn't exist.";
    public static final String BOARD_NOT_FOUND = "Board \"%s\" doesn't exist.";
    public static final String ITEM_NOT_FOUND = "%s \"%s\" doesn't exist in %s.";
    public static final String ID_DOES_NOT_EXIST = "\"%s\" with ID: %d doesn't exist.";
    public static final String COMMENT_NOT_FOUND = "Comment doesn't exist.";
    public static final String LIST_NOT_FOUND = "List does not exist.";
    public static final String LIST_IS_EMPTY = "List is empty.";

    public static final String EXPECTED_TYPE_BUG = "bug";
    public static final String EXPECTED_TYPE_STORY = "story";
    public static final String EXPECTED_TYPE_FEEDBACK = "feedback";

    public static final String MEMBER_ADDED_TO_TEAM = "Person %s successfully added to team \"%s\".";
    public static final String MEMBER_REMOVED_FROM_TEAM = "Person %s successfully removed from team \"%s\".";
    public static final String BOARD_REMOVED_FROM_TEAM = "Board \"%s\" successfully removed from team \"%s\".";
    public static final String WORKITEM_ASSIGNED_TO_PERSON = "%s with name \"%s\" and ID: %d assigned to %s.";
    public static final String WORKITEM_UNASSIGNED_FROM_PERSON = "%s with name \"%s\" and ID: %d unassigned from %s.";
    public static final String WORKITEM_ADDED_TO_BOARD = "%s \"%s\" with ID: %d successfully added to board \"%s\".";
    public static final String WORKITEM_REMOVED_FROM_BOARD = "%s \"%s\" with ID: %d successfully removed from board \"%s\".";
    public static final String COMMENT_ADDED_TO_WORKITEM = "Comment successfully added to %s \"%s\".";
    public static final String COMMENT_REMOVED_FROM_WORKITEM = "Comment successfully removed from %s \"%s\".";
    public static final String COMMENT_ADDED = "Comment successfully added.";
    public static final String COMMENT_REMOVED = "Comment successfully removed.";
    public static final String STEPSTOREPRODUCE_ADDED_TO_BUG = "Steps to reproduce successfully added to bug \"%s\".";
    public static final String NEW_RATING_ADDED = "New rating added.";
    public static final String NEW_WORKITEM_ASSIGNED = "New work item assigned to %s.";
    public static final String WORKITEM_UNASSIGNED = "Work item unassigned from %s.";

    public static final String NEW_SEVERITY_TYPE =
                                "Severity type of %s with name \"%s\" and ID: %d changed to \"%s\".";
    public static final String NEW_SIZE_TYPE ="Size type of %s with name \"%s\" and ID: %d changed to \"%s\".";
    public static final String WORK_ITEM_STATUS_CHANGED =
                                "Status type of %s with name \"%s\" and ID: %d changed to \"%s\".";
    public static final String WORK_ITEM_PRIORITY_CHANGED =
                                "Priority type of %s with name \"%s\" and ID: %d changed to \"%s\".";
    public static final String PRIORITY_TYPE_CHANGED = "Priority type changed to \"%s\".";
    public static final String SIZE_TYPE_CHANGED = "Size type changed to \"%s\".";
    public static final String SEVERITY_TYPE_CHANGED = "Severity type changed to \"%s\".";
    public static final String STATUS_TYPE_CHANGED = "Status type changed to \"%s\".";
    public static final String NEW_RATING = "Rating of %s with name \"%s\" and ID: %d changed to %d";

    public static final String WRONG_SEVERITY_TYPE =
            "Wrong severity type entered. Please enter one of the following: \"Minor\", \"Major\" or \"Critical\".";
    public static final String WRONG_PRIORITY_TYPE =
            "Wrong priority type entered. Please enter one of the following: \"High\", \"Medium\" or \"Low\".";
    public static final String WRONG_SIZE_TYPE =
            "Wrong size type entered. Please enter one of the following: \"Small\", \"Medium\" or \"Large\".";

    public static final String WRONG_STATUS_TYPE_BUG =
            "Wrong status type entered for %s. Please enter one of the following: \"Active\" or \"Fixed\".";
    public static final String WRONG_STATUS_TYPE_FEEDBACK =
            "Wrong status type entered for %s. Please enter one of the following: " +
                    "\"New\", \"Unscheduled\", \"Scheduled\" or \"Done\".";
    public static final String WRONG_STATUS_TYPE_STORY =
            "Wrong status type entered for %s. Please enter one of the following: " +
                    "\"NotDone\", \"InProgress\" or \"Done\".";
    public static final String WRONG_WORKITEM_TYPE = "Wrong work item type entered. Expected: \"%s\", Received: \"%s\".";
    public static final String WRONG_WORKITEM = "Wrong work item type entered. Please enter either \"bug\" or \"story\".";
    public static final String STATUS_TYPE_NOT_FOUND = "Status type \"%s\" not found.";
    public static final String ASSIGNEE_NOT_FOUND = "Assignee %s not found.";
    public static final String STATUS_TYPE_AND_ASSIGNEE_NOT_FOUND = "Status type \"%s\" and assignee %s not found.";
}