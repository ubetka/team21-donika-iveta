package com.commands.common;

import com.workitems.enums.*;

import java.util.List;
import java.util.Map;

import static com.commands.common.CommandConstants.*;

public final class CommandValidator {

    public static void checkINumberOfArguments(int expectedArguments, int actualArguments) {
        if (expectedArguments != actualArguments) {
            throw new IllegalArgumentException
                    (String.format(INVALID_NUMBER_OF_ARGUMENTS, expectedArguments, actualArguments));
        }
    }

    public static void isEmpty(String list) {
        if (list.length() == 0) {
            throw new IllegalArgumentException(LIST_IS_EMPTY);
        }
    }

    public static <K, V, T> void checkIfItemExistsInRepository(Map<K, V> dictionary, T id, String error) {
        if (!dictionary.containsKey(id)) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void checkIfItemExistsInList(List list, Object item, String error) {
        if (!list.contains(item)) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void checkIfItemAlreadyExistsInList(List list, Object item, String error) {
        if (list.contains(item)) {
            throw new IllegalArgumentException(error);
        }
    }

    public static <K, V, T> void checkIfItemAlreadyExists(Map<K, V> dictionary, T id, String error) {
        if (dictionary.containsKey(id)) {
            throw new IllegalArgumentException(error);
        }
    }

    public static void isValidSizeType(String input) {
        boolean isValid = false;
        for (SizeType type : SizeType.values()) {
            if (type.name().equals(input.toUpperCase())) {
                isValid = true;
            }
        }
        if (!isValid) {
            throw new IllegalArgumentException(WRONG_SIZE_TYPE);
        }
    }

    public static void isValidSeverityType(String input){
        boolean isValid = false;
        for (SeverityType type: SeverityType.values()) {
            if (type.name().equals(input.toUpperCase())){
                isValid = true;
            }
        }
        if (!isValid) {
            throw new IllegalArgumentException(WRONG_SEVERITY_TYPE);
        }
    }

    public static void isValidPriorityType(String input){
        boolean isValid = false;
        for (PriorityType type : PriorityType.values()) {
            if (type.name().equals(input.toUpperCase())){
                isValid = true;
            }
        }
        if (!isValid){
            throw new IllegalArgumentException(WRONG_PRIORITY_TYPE);
        }
    }

    public static boolean isValidStatusTypeBug(String input){
        for (StatusTypeBug s : StatusTypeBug.values()) {
            if (s.name().equals(input.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidStatusTypeFeedback(String newStatus) {
        for (StatusTypeFeedback s : StatusTypeFeedback.values()) {
            if (s.name().equals(newStatus.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidStatusTypeStory(String newStatus) {
        for (StatusTypeStory s : StatusTypeStory.values()) {
            if (s.name().equals(newStatus.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    public static void checkWorkItemType(String workItemType, String expectedType) {
        if (!workItemType.toLowerCase().equals(expectedType))
            throw new IllegalArgumentException(String.format(WRONG_WORKITEM_TYPE, workItemType, expectedType));
    }

    public static void checkTwoWorkItemTypes(String workItemType, String expectedType, String secondExpectedType) {
        if (!workItemType.toLowerCase().equals(expectedType) && !workItemType.toLowerCase().equals(secondExpectedType))
            throw new IllegalArgumentException(WRONG_WORKITEM);
    }

    public static void checkThreeWorkItemTypes
            (String workItemType, String expectedType, String secondExpectedType, String thirdExpectedType) {
        if (!workItemType.toLowerCase().equals(expectedType)
                && !workItemType.toLowerCase().equals(secondExpectedType)
                && !workItemType.toLowerCase().equals(thirdExpectedType))
            throw new IllegalArgumentException(WRONG_WORKITEM);
    }
}
