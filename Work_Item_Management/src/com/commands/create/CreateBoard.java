package com.commands.create;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.teams.contracts.Team;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class CreateBoard implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Repository repository;
    private final Factory factory;
    private String teamName;
    private String boardName;

    public CreateBoard(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        boardName = parameters.get(0);
        teamName = parameters.get(1);

        return createBoard(boardName);
    }

    private String createBoard(String boardName) {
        CommandValidator.checkIfItemAlreadyExists(repository.getBoard(), boardName,
                String.format(BOARD_EXISTS_ERROR_MESSAGE, boardName));
        CommandValidator.checkIfItemExistsInRepository(repository.getTeam(), teamName,
                String.format(TEAM_NOT_FOUND, teamName));

        Board board = factory.createBoard(boardName);
        repository.addBoard(boardName, board);
        Team team = repository.getTeam().get(teamName);
        team.addBoard(board);

        return String.format(BOARD_CREATED_SUCCESS_MESSAGE, boardName, teamName);
    }
}
