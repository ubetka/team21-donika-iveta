package com.commands.create;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.workitems.contracts.Bug;
import com.workitems.models.WorkItemImpl;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class CreateBug implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4 ;

    private final Repository repository;
    private final Factory factory;
    private String boardName;

    public CreateBug(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String title = parameters.get(0);
        String description = parameters.get(1);
        String severityType = parameters.get(2);
        boardName = parameters.get(3);

        return createBug(title, description, severityType);
    }

    private String createBug(String title, String description, String severityType) {
        CommandValidator.checkIfItemExistsInRepository(repository.getBoard(), boardName,
                String.format(BOARD_NOT_FOUND, boardName));
        CommandValidator.isValidSeverityType(severityType);

        Bug bug = factory.createBug(title, description, severityType);
        repository.addWorkItem(bug.getID(), bug);
        Board board = repository.getBoard().get(boardName);
        board.addWorkItem(bug);

        return String.format(BUG_CREATED_SUCCESS_MESSAGE, title, bug.getID(), boardName);
    }
}

