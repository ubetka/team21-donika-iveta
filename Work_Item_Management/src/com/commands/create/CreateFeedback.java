package com.commands.create;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.workitems.contracts.Feedback;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class CreateFeedback implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private final Repository repository;
    private final Factory factory;
    private String boardName;

    public CreateFeedback(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String title = parameters.get(0);
        String description = parameters.get(1);
//        int rating = Integer.parseInt(parameters.get(2));
        boardName = parameters.get(2);

        return createFeedback(title, description);
    }

    private String createFeedback(String title, String description) {

        CommandValidator.checkIfItemExistsInRepository(repository.getBoard(), boardName,
                String.format(BOARD_NOT_FOUND, boardName));

        Feedback feedback = factory.createFeedback(title, description);
        repository.addWorkItem(feedback.getID(), feedback);
        Board board = repository.getBoard().get(boardName);
        board.addWorkItem(feedback);

        return String.format(FEEDBACK_CREATED_SUCCESS_MESSAGE, title, feedback.getID(), boardName);
    }
}