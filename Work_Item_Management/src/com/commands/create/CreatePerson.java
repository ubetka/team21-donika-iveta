package com.commands.create;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Person;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class CreatePerson implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Repository repository;
    private final Factory factory;

    public CreatePerson(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String namePerson = parameters.get(0);
        return createPerson(namePerson);
    }

    private String createPerson(String name) {
        CommandValidator.checkIfItemAlreadyExists(repository.getPerson(), name,
                String.format(NAME_EXISTS_ERROR_MESSAGE, name));

        Person person = factory.createPerson(name);
        repository.addPerson(name, person);

        return String.format(PERSON_CREATED_SUCCESS_MESSAGE, name);
    }
}
