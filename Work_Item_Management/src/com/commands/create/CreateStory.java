package com.commands.create;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.workitems.contracts.Story;

import java.util.List;
import static com.commands.common.CommandConstants.*;

public class CreateStory implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3 ;

    private final Repository repository;
    private final Factory factory;
    private String boardName;

    public CreateStory(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String title = parameters.get(0);
        String description = parameters.get(1);
        boardName = parameters.get(2);

        return createStory(title, description);
    }

    private String createStory(String title, String description) {
        CommandValidator.checkIfItemExistsInRepository(repository.getBoard(), boardName,
                String.format(BOARD_NOT_FOUND, boardName));

        Story story = factory.createStory(title, description);
        repository.addWorkItem(story.getID(), story);
        Board board = repository.getBoard().get(boardName);
        board.addWorkItem(story);

        return String.format(STORY_CREATED_SUCCESS_MESSAGE, title, story.getID(), boardName);
    }
}

