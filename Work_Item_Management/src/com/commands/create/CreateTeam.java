package com.commands.create;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Team;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class CreateTeam implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Repository repository;
    private final Factory factory;

    public CreateTeam(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String nameTeam = parameters.get(0);
        return createTeam(nameTeam);
    }

    private String createTeam(String name) {
        CommandValidator.checkIfItemAlreadyExists(repository.getTeam(), name,
                String.format(TEAM_EXISTS_ERROR_MESSAGE, name));

        Team team = factory.createTeam(name);
        repository.addTeam(name, team);

        return String.format(TEAM_CREATED_SUCCESS_MESSAGE, name);
    }
}
