package com.commands.filter;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static com.commands.common.CommandConstants.ASSIGNEE_NOT_FOUND;
import static com.commands.common.CommandConstants.PERSON_NOT_FOUND;

public class FilterAssignee implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Repository repository;
    private final Factory factory;


    public FilterAssignee(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());
        String assignee = parameters.get(0);
        CommandValidator.checkIfItemExistsInRepository(repository.getPerson(), assignee,
                String.format(PERSON_NOT_FOUND, assignee));

        return listAllAssignments(assignee);
    }

    private String listAllAssignments(String assignee) {

        List<String> list = repository
                .getPerson()
                .get(assignee)
                .listWorkItems()
                .stream()
                .map(item -> item.getTitle())
                .collect(Collectors.toList());

        if (list.size() == 0) {
            throw new IllegalArgumentException(String.format(ASSIGNEE_NOT_FOUND, assignee));
        } else {
            return list.toString().replace("[", "").replace("]", "");
        }
    }
}