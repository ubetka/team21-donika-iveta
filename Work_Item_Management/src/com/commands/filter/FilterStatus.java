package com.commands.filter;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static com.commands.common.CommandConstants.STATUS_TYPE_NOT_FOUND;

public class FilterStatus implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Repository repository;
    private final Factory factory;


    public FilterStatus(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());
        String status = parameters.get(0);

        return listAllStatuses(status);
    }

    private String listAllStatuses(String status) {
        List<String> list = repository
                .getWorkItem()
                .values()
                .stream()
                .filter(x -> x.getStatus().toString().equalsIgnoreCase(status))
                .map(item -> item.getTitle())
                .collect(Collectors.toList());

        if (list.size() == 0) {
            throw new IllegalArgumentException(String.format(STATUS_TYPE_NOT_FOUND, status));
        } else {
            return list.toString().replace("[", "").replace("]", "");
        }
    }
}
