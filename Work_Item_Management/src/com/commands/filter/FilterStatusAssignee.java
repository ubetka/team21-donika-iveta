package com.commands.filter;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.BugAndStory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.commands.common.CommandConstants.*;

public class FilterStatusAssignee implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Repository repository;
    private final Factory factory;


    public FilterStatusAssignee(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String status = parameters.get(0);
        String assignee = parameters.get(1);
        CommandValidator.checkIfItemExistsInRepository(repository.getPerson(), assignee,
                String.format(PERSON_NOT_FOUND, assignee));

        return filterByStatusAssignee(status, assignee);
    }


    private String filterByStatusAssignee(String status, String assignee) {
        List<String> list= new ArrayList<>();
        repository
                .getWorkItem()
                .values()
                .stream()
                .filter(x -> !x.getItemType().equalsIgnoreCase("Feedback"))
                .map(x -> (BugAndStory) x)
                .collect(Collectors.toList()).stream()
                .filter(x -> x.getStatus().toString().equalsIgnoreCase(status)
                        && x.getAssignee().contains(assignee))
                .forEach(x ->  list.add(x.getTitle()));

        if (list.size()==0){
            throw new IllegalArgumentException(String.format(STATUS_TYPE_AND_ASSIGNEE_NOT_FOUND, status, assignee));
        } else{
            return list.toString().replace("[", "").replace("]", "");
        }
    }
}