package com.commands.set;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.*;
import com.workitems.enums.PriorityType;

import java.util.List;

import static com.commands.common.CommandConstants.*;
import static com.commands.common.CommandConstants.WRONG_SEVERITY_TYPE;

public class SetPriority implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private final Factory factory;
    private final Repository repository;


    public SetPriority(Repository repository, Factory factory) {
        this.factory = factory;
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemType = parameters.get(0);
        String workItemName = parameters.get(1);
        int workItemID = Integer.parseInt(parameters.get(2));
        String newPriority = parameters.get(3);

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        CommandValidator.checkTwoWorkItemTypes(workItemType, EXPECTED_TYPE_BUG, EXPECTED_TYPE_STORY);
        BugAndStory bugAndStory = (BugAndStory) repository.getWorkItem().get(workItemID);
        if (!bugAndStory.getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }
        CommandValidator.isValidPriorityType(newPriority);
        bugAndStory.setPriority(PriorityType.valueOf(newPriority.toUpperCase()));
        return String.format(WORK_ITEM_PRIORITY_CHANGED, workItemType, workItemName,workItemID, newPriority);
    }
}