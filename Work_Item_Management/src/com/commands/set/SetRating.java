package com.commands.set;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.models.ValidationHelper;
import com.workitems.contracts.Feedback;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class SetRating implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private final Factory factory;
    private final Repository repository;

    public SetRating(Repository repository, Factory factory) {
        this.factory = factory;
        this.repository = repository;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemType = parameters.get(0);
        String workItemName = parameters.get(1);
        int workItemID = Integer.parseInt(parameters.get((2)));
        int newRating = Integer.parseInt(parameters.get(3));

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        if (!repository.getWorkItem().get(workItemID).getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }
        CommandValidator.checkWorkItemType(EXPECTED_TYPE_FEEDBACK, workItemType);
        Feedback feedback = (Feedback) repository.getWorkItem().get(workItemID);
        feedback.setRating(newRating);
        return String.format(NEW_RATING, workItemType, workItemName, workItemID, feedback.getRating());
    }
}
