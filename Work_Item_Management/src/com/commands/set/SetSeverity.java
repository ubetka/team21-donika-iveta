package com.commands.set;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.Bug;
import com.workitems.enums.SeverityType;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class SetSeverity implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private Repository repository;
    private Factory factory;

    public SetSeverity(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemType = parameters.get(0);
        String workItemName = parameters.get(1);
        int workItemID = Integer.parseInt(parameters.get(2));
        String newSeverity = parameters.get(3);

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        CommandValidator.checkWorkItemType(EXPECTED_TYPE_BUG, workItemType);
        if (!repository.getWorkItem().get(workItemID).getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }
        CommandValidator.isValidSeverityType(newSeverity);

        Bug bug = (Bug) repository.getWorkItem().get(workItemID);
        bug.setSeverity(SeverityType.valueOf(newSeverity.toUpperCase()));
        return String.format(NEW_SEVERITY_TYPE, workItemType,workItemName,workItemID, newSeverity);
    }
}