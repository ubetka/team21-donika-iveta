package com.commands.set;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.Story;
import com.workitems.enums.SizeType;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class SetSize implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private final Repository repository;
    private final Factory factory;

    public SetSize(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemType = parameters.get(0);
        String workItemName = parameters.get(1);
        int workItemID = Integer.parseInt(parameters.get(2));
        String newSize = parameters.get(3);

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        if (!repository.getWorkItem().get(workItemID).getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }
        CommandValidator.checkWorkItemType(EXPECTED_TYPE_STORY, workItemType);
        CommandValidator.isValidSizeType(newSize);

        Story story = (Story) repository.getWorkItem().get(workItemID);
        story.setSize(SizeType.valueOf(newSize.toUpperCase()));
        return String.format(NEW_SIZE_TYPE, workItemType, workItemName, workItemID, newSize);
    }
}
