package com.commands.set;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.WorkItem;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class SetStatus implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private final Repository repository;
    private final Factory factory;

    public SetStatus(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String workItemType = parameters.get(0);
        String workItemName = parameters.get(1);
        int workItemID = Integer.parseInt(parameters.get(2));
        String newStatus = parameters.get(3);

        CommandValidator.checkIfItemExistsInRepository(repository.getWorkItem(), workItemID,
                String.format(ID_DOES_NOT_EXIST, workItemName, workItemID));
        if (!repository.getWorkItem().get(workItemID).getTitle().equals(workItemName)) {
            throw new IllegalArgumentException(String.format(WORKITEM_AND_ID_DO_NOT_MATCH, workItemName, workItemID));
        }
        CommandValidator.checkThreeWorkItemTypes(workItemType, EXPECTED_TYPE_BUG,
                EXPECTED_TYPE_FEEDBACK, EXPECTED_TYPE_STORY);

        WorkItem workItem = repository.getWorkItem().get(workItemID);

        switch (workItemType.toLowerCase()) {
            case EXPECTED_TYPE_BUG:
                if (!CommandValidator.isValidStatusTypeBug(newStatus)) {
                    throw new IllegalArgumentException(String.format(WRONG_STATUS_TYPE_BUG, EXPECTED_TYPE_BUG));
                } else {
                    workItem.setStatus(newStatus.toUpperCase());
                    return String.format(WORK_ITEM_STATUS_CHANGED, workItemType, workItemName, workItemID, newStatus);
                }
            case EXPECTED_TYPE_FEEDBACK:

                if (!CommandValidator.isValidStatusTypeFeedback(newStatus)) {
                    throw new IllegalArgumentException(String.format
                            (WRONG_STATUS_TYPE_FEEDBACK, EXPECTED_TYPE_FEEDBACK));
                } else {
                    workItem.setStatus(newStatus.toUpperCase());
                    return String.format(WORK_ITEM_STATUS_CHANGED, workItemType, workItemName, workItemID, newStatus);
                }
            case EXPECTED_TYPE_STORY:
                if (!CommandValidator.isValidStatusTypeStory(newStatus)) {
                    throw new IllegalArgumentException(String.format(WRONG_STATUS_TYPE_STORY, EXPECTED_TYPE_STORY));
                } else {
                    workItem.setStatus(newStatus.toUpperCase());
                    return String.format(WORK_ITEM_STATUS_CHANGED, workItemType, workItemName, workItemID, newStatus);
                }
            default:
                throw new IllegalArgumentException(WRONG_WORKITEM);
        }
    }
}