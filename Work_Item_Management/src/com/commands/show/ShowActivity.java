package com.commands.show;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class ShowActivity implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Repository repository;
    private final Factory factory;

    public ShowActivity(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String objectToShow = parameters.get(0);
        String nameOfObject = parameters.get(1);

        switch (objectToShow) {
            case "person":
                return showPersonActivity(nameOfObject);
            case "team":
                return showTeamActivity(nameOfObject);
            case "board":
                return showBoardActivity(nameOfObject);
            default:
                throw new IllegalArgumentException(INVALID_COMMAND_ERROR_MESSAGE);
        }
    }

    private String showPersonActivity(String nameOfObject) {
        CommandValidator.checkIfItemExistsInRepository(repository.getPerson(), nameOfObject,
                String.format(PERSON_NOT_FOUND, nameOfObject));

        Person person = repository.getPerson().get(nameOfObject);
        return person.showActivity();
    }

    private String showTeamActivity(String nameOfObject){
        CommandValidator.checkIfItemExistsInRepository(repository.getTeam(), nameOfObject,
                String.format(TEAM_NOT_FOUND, nameOfObject));

        Team team = repository.getTeam().get(nameOfObject);
        return team.showTeamActivity();
    }

    private String showBoardActivity(String nameOfObject){
        CommandValidator.checkIfItemExistsInRepository(repository.getBoard(), nameOfObject,
                String.format(BOARD_NOT_FOUND, nameOfObject));

        Board board = repository.getBoard().get(nameOfObject);
        return board.showActivity();
    }
}
