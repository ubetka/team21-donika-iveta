package com.commands.show;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static com.commands.common.CommandConstants.*;

public class ShowAll implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Repository repository;
    private final Factory factory;


    public ShowAll(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        String objectsToShow = parameters.get(0);

        return showAllLists(objectsToShow);
    }

    private String showAllLists(String object) {
        StringJoiner joiner = new StringJoiner(", ");
        Set<String> objectsNames = Collections.emptySet();

        switch (object) {
            case "people":
                objectsNames = repository.getPerson().keySet();
                break;
            case "teams":
                objectsNames = repository.getTeam().keySet();
                break;
            case "workitems":
                objectsNames = repository.getWorkItem().values()
                        .stream()
                        .map(item -> item.getTitle())
                        .collect(Collectors.toSet());
                break;
            case "story":
                objectsNames = repository.getWorkItem().values()
                        .stream()
                        .filter(item -> item.getItemType().equalsIgnoreCase(object))
                        .map(item -> item.getTitle())
                        .collect(Collectors.toSet());
                break;
            case "bug":
                objectsNames = repository.getWorkItem().values()
                        .stream()
                        .filter(item -> item.getItemType().equalsIgnoreCase(object))
                        .map(item -> item.getTitle())
                        .collect(Collectors.toSet());
                break;
            case "feedback":
                objectsNames = repository.getWorkItem().values()
                        .stream()
                        .filter(item -> item.getItemType().equalsIgnoreCase(object))
                        .map(item -> item.getTitle())
                        .collect(Collectors.toSet());
                break;
            default:
                throw new IllegalArgumentException(LIST_NOT_FOUND);
        }
        for (String name : objectsNames) {
            joiner.add(name);
        }
        return joiner.toString();
    }
}