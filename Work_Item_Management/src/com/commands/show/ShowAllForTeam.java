package com.commands.show;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.teams.contracts.Team;

import java.util.List;

import static com.commands.common.CommandConstants.*;

public class ShowAllForTeam implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Repository repository;
    private final Factory factory;

    private String teamName;
    public ShowAllForTeam(Repository repository, Factory factory){
        this.repository = repository;
        this.factory = factory;
    }

    public String execute (List<String> parameters){
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        teamName = parameters.get(0);
        String objectToShow = parameters.get(1);

        CommandValidator.checkIfItemExistsInRepository(repository.getTeam(), teamName,
                String.format(TEAM_NOT_FOUND, teamName));

        switch (objectToShow){
            case "members": return showMembers();
            case "boards": return showBoards();
                default: throw new IllegalArgumentException(LIST_NOT_FOUND);
        }
    }

    public String showMembers(){
        Team team = repository.getTeam().get(teamName);
        return team.showMembers();
    }

    public String showBoards(){
        Team team = repository.getTeam().get(teamName);
        return team.showBoards();
    }
}
