package com.commands.sort;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.BugAndStory;

import java.util.*;
import java.util.stream.Collectors;

public class SortByPriority implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private Repository repository;
    private final Factory factory;


    public SortByPriority(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        StringJoiner joiner = new StringJoiner(", ");
        List<String> objectsPriority = new ArrayList<>();

        repository.getWorkItem()
                .values()
                .stream()
                .filter(x -> !x.getItemType().equalsIgnoreCase("Feedback"))
                .map(x -> (BugAndStory) x)
                .collect(Collectors.toList()).stream()
                .sorted(Comparator.comparing(BugAndStory::getPriority))
                .forEach(x -> objectsPriority.add(x.getTitle()));

        for (String name : objectsPriority) {
            joiner.add(name);
        }
        return joiner.toString().replace("[", "").replace("]", "");
    }
}
