package com.commands.sort;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.Feedback;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class SortByRating implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private final Repository repository;
    private final Factory factory;

    public SortByRating(Repository repository, Factory factory){
        this.repository = repository;
        this.factory = factory;
    }

    public String execute (List<String> parameters){
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        StringJoiner joiner = new StringJoiner(", ");
        List <String> sortedFeedback = new ArrayList<>();

        repository.getWorkItem().values().stream()
                .filter(item -> item.getItemType().equalsIgnoreCase("Feedback"))
                .map(item -> (Feedback)item)
                .collect(Collectors.toList())
                .stream()
                .sorted((item1, item2) -> item1.getRating())
                .forEach(item -> sortedFeedback.add(item.getTitle()));
        for (String name : sortedFeedback) {
            joiner.add(name);
        }
        return joiner.toString().replace("[", "").replace("]", "");
    }
}
