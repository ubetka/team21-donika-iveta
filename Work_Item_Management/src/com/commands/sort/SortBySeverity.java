package com.commands.sort;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.Bug;
import com.workitems.contracts.BugAndStory;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class SortBySeverity implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private Repository repository;
    private final Factory factory;


    public SortBySeverity(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        StringJoiner joiner = new StringJoiner(", ");
        List<String> objectsSeverity = new ArrayList<>();

        repository.getWorkItem()
                .values()
                .stream()
                .filter(x -> x.getItemType().equalsIgnoreCase("Bug"))
                .map(x -> (Bug) x)
                .collect(Collectors.toList())
                .stream()
                .sorted((x, y)->x.getSeverity().compareTo(y.getSeverity()))
                .forEach(x -> objectsSeverity.add(x.getTitle()));

        for (String name : objectsSeverity) {
            joiner.add(name);
        }
        return joiner.toString().replace("[", "").replace("]", "");
    }
}

