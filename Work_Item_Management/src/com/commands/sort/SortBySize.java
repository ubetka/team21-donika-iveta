package com.commands.sort;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;
import com.workitems.contracts.Story;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class SortBySize implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private final Repository repository;
    private final Factory factory;

    public SortBySize(Repository repository, Factory factory){
        this.repository = repository;
        this.factory = factory;
    }

    public String execute (List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());
        StringJoiner joiner = new StringJoiner(", ");
        List<String> sortedStories = new ArrayList<>();

        repository.getWorkItem()
                .values()
                .stream()
                .filter(x -> x.getItemType().equalsIgnoreCase("Story"))
                .map(x -> (Story) x)
                .collect(Collectors.toList())
                .stream()
                .sorted((x, y)->x.getSize().compareTo(y.getSize()))
                .forEach(x -> sortedStories.add(x.getTitle()));

        for (String name : sortedStories) {
            joiner.add(name);
        }
        return sortedStories.toString().replace("[", "").replace("]", "");
    }
}
