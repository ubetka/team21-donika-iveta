package com.commands.sort;

import com.commands.common.CommandValidator;
import com.commands.contracts.Command;
import com.core.contracts.Factory;
import com.core.contracts.Repository;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class SortByTitle implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private Repository repository;
    private final Factory factory;


    public SortByTitle(Repository repository, Factory factory) {
        this.repository = repository;
        this.factory = factory;
    }

    public String execute(List<String> parameters) {
        CommandValidator.checkINumberOfArguments(EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size());

        StringJoiner joiner = new StringJoiner(", ");
        List<String> objectsNames;

        objectsNames = repository.getWorkItem().values().stream()
                .map(item -> item.getTitle()).sorted().collect(Collectors.toList());

        for (String name : objectsNames) {
            joiner.add(name);
        }
        return joiner.toString().replace("[", "").replace("]", "");
    }
}