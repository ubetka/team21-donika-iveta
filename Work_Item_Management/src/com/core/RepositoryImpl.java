package com.core;


import com.core.contracts.Repository;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.workitems.contracts.WorkItem;

import java.util.HashMap;
import java.util.Map;

public class RepositoryImpl implements Repository {
    private Map<String, Person> persons;
    private Map<String, Team> teams;
    private Map<String, Board> boards;

    private Map<Integer, WorkItem> workItems;

    public RepositoryImpl() {
        this.persons = new HashMap<>();
        this.teams = new HashMap<>();
        this.boards = new HashMap<>();
        this.workItems = new HashMap<>();
    }

    public Map<String, Person> getPerson() {
        return new HashMap<>(persons);
    }

    public Map<String, Team> getTeam() {
        return new HashMap<>(teams);
    }

    public Map<String, Board> getBoard() {
        return new HashMap<>(boards);
    }

    public Map<Integer, WorkItem> getWorkItem(){
        return new HashMap<>(workItems);
    }

    public void addPerson(String name, Person person) {
        this.persons.put(name, person);
    }

    public void addTeam(String name, Team team) {
        this.teams.put(name, team);
    }

    public void addBoard(String name, Board board) {
        this.boards.put(name, board);
    }

    public void removeBoard(String name){
        boards.remove(name);
    }

    public void addWorkItem(Integer id, WorkItem workItem){
        this.workItems.put(id, workItem);
    }

    public void removeWorkItem(Integer id){
        workItems.remove(id);
    }
}
