package com.core.contracts;

import com.commands.contracts.Command;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, Factory factory, Repository agencyRepository);
}
