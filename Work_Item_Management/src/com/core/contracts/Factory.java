package com.core.contracts;

import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;

import java.util.List;

public interface Factory {
    Person createPerson(String name);

    Team createTeam(String name);

    Board createBoard(String boardName);

    Bug createBug(String title, String description, String severity);

    Feedback createFeedback(String title, String description);

    Story createStory(String title, String description);
}
