package com.core.contracts;

public interface Reader {
    String readLine();
}
