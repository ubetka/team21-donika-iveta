package com.core.contracts;

import com.sun.corba.se.spi.orbutil.threadpool.Work;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.contracts.WorkItem;
import com.workitems.enums.SeverityType;

import java.util.List;
import java.util.Map;

public interface Repository {
    Map<String, Person> getPerson();

    Map<String, Team> getTeam();

    Map<String, Board> getBoard();

    Map<Integer, WorkItem> getWorkItem();

//    Map<String, Bug> getBug();
//
//    Map<String, Feedback> getFeedback();
//
//    Map<String, Story> getStory();

//    Map<String, WorkItem> getWorkItem();

    void addPerson(String name, Person person);

    void addBoard(String name, Board board);

    void addTeam(String name, Team team);

    void removeBoard(String name);

    void addWorkItem(Integer id, WorkItem workItem);

    void removeWorkItem(Integer id);

//    void addBug(String title, Bug bug);
//
//    void addFeedback(String title, Feedback feedback);
//
//    void addStory(String title, Story story);

//    void addWorkItem(String title, WorkItem workItem);
}
