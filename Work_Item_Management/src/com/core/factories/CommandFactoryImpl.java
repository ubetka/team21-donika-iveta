package com.core.factories;

import com.commands.addremove.*;
import com.commands.contracts.Command;
import com.commands.create.*;
import com.commands.enums.CommandType;
import com.commands.filter.FilterAssignee;
import com.commands.filter.FilterStatusAssignee;
import com.commands.filter.FilterStatus;
import com.commands.set.*;
import com.commands.show.*;
import com.commands.sort.*;
import com.core.contracts.CommandFactory;
import com.core.contracts.Factory;
import com.core.contracts.Repository;

public class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, Factory factory, Repository repository) {
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());
        switch (commandType) {
            case CREATEPERSON:
                return new CreatePerson(repository, factory);
            case CREATETEAM:
                return new CreateTeam(repository, factory);
            case CREATEBOARD:
                return new CreateBoard(repository, factory);
            case CREATEBUG:
                return new CreateBug(repository, factory);
            case CREATEFEEDBACK:
                return new CreateFeedback(repository, factory);
            case CREATESTORY:
                return new CreateStory(repository, factory);
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(repository, factory);
            case REMOVEPERSONFROMTEAM:
                return new RemovePersonFromTeam(repository, factory);
            case REMOVEBOARDFROMTEAM:
                return new RemoveBoardFromTeam(repository, factory);
            case REMOVEWORKITEMFROMBOARD:
                return new RemoveWorkItemFromBoard(repository, factory);
            case ASSIGNTOPERSON:
                return new AssignToPerson(repository, factory);
            case UNASSIGNFROMPERSON:
                return new UnassignFromPerson(repository, factory);
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentToWorkItem(repository, factory);
            case REMOVECOMMENTFROMWORKITEM:
                return new RemoveCommentFromWorkItem(repository, factory);
            case SETSTATUS:
                return new SetStatus(repository, factory);
            case SETSEVERITY:
                return new SetSeverity(repository, factory);
            case SETSIZE:
                return new SetSize(repository, factory);
            case SETRATING:
                return new SetRating(repository, factory);
            case SETPRIORITY:
                return new SetPriority(repository, factory);
            case SHOWALL:
                return new ShowAll(repository, factory);
            case SHOWALLFORTEAM:
                return new ShowAllForTeam(repository, factory);
            case SHOWACTIVITY:
                return new ShowActivity(repository, factory);
            case FILTERSTATUS:
                return new FilterStatus(repository, factory);
            case FILTERASSIGNEE:
                return new FilterAssignee(repository, factory);
            case FILTERSTATUSASSIGNEE:
                return new FilterStatusAssignee(repository, factory);
            case SORTBYTITLE:
                return new SortByTitle(repository, factory);
            case SORTBYPRIORITY:
                return new SortByPriority(repository, factory);
            case SORTBYSEVERITY:
                return new SortBySeverity(repository, factory);
            case SORTBYSIZE:
                return new SortBySize(repository, factory);
            case SORTBYRATING:
                return new SortByRating(repository, factory);
            case ADDSTEPSTOREPRODUCETOBUG:
                return new AddStepsToReproduceToBug(repository,factory);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
}
