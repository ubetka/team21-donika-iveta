package com.core.factories;


import com.core.contracts.Factory;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;
import com.teams.models.BoardImpl;
import com.teams.models.PersonImpl;
import com.teams.models.TeamImpl;
import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.enums.SeverityType;
import com.workitems.models.BugImpl;
import com.workitems.models.FeedbackImpl;
import com.workitems.models.StoryImpl;

import java.util.List;

public class FactoryImpl implements Factory {
    @Override
    public Person createPerson(String name) {
        return new PersonImpl(name);
    }

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Board createBoard(String boardName) {
        return new BoardImpl(boardName);
    }

    @Override
    public Bug createBug(String title, String description, String severity){
        return new BugImpl(title,description, getSeverityType(severity)); }

    @Override
    public Feedback createFeedback(String title, String description){
        return new FeedbackImpl(title, description); }

    @Override
    public Story createStory(String title, String description){
        return new StoryImpl(title, description); }

    private SeverityType getSeverityType(String severity) {
        return SeverityType.valueOf(severity.toUpperCase());
    }
}



    
