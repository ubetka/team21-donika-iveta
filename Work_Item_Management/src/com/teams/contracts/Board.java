package com.teams.contracts;

import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.contracts.WorkItem;

import java.util.List;

public interface Board{
    String getName();

    void addActivity(String activity);

    String showActivity();

    void addWorkItem(WorkItem workItem);

    List<WorkItem> listWorkItems();

    void removeWorkItems(WorkItem workItem);

    String toString();
}
