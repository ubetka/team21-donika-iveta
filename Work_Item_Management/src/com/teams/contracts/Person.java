package com.teams.contracts;

import com.workitems.contracts.Bug;
import com.workitems.contracts.Feedback;
import com.workitems.contracts.Story;
import com.workitems.contracts.WorkItem;
import javafx.beans.binding.ObjectExpression;

import java.util.List;

public interface Person {
    String getName();

    void assignWorkItem(WorkItem workItem);

    void unassignWorkItem(WorkItem workItem);

    List<WorkItem> listWorkItems();

    void addActivity(String activity);

    String showActivity();

    String toString();
}
