package com.teams.contracts;

import java.util.List;

public interface Team {
    String getName();

    void addMember(Person person);

    void removeMember(Person person);

    String showMembers();

    void addBoard(Board board);

    void removeBoard(Board board);

    String showBoards();

    String showTeamActivity();

    String toString();

    List <Person> getMembers();

    List <Board> getBoards();
}
