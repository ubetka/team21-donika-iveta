package com.teams.models;

import com.teams.contracts.Board;
import com.workitems.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

import static com.commands.common.CommandConstants.*;

public class BoardImpl implements Board {

    private final static int MIN_BOARD_NAME_LENGTH = 5;
    private final static int MAX_BOARD_NAME_LENGTH = 10;

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    public BoardImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getMinLength() {
        return MIN_BOARD_NAME_LENGTH;
    }

    public int getMaxLength() {
        return MAX_BOARD_NAME_LENGTH;
    }

    public void addWorkItem(WorkItem workItem) {
        ValidationHelper.checkIfItemAlreadyExists(workItems, workItem,
                String.format(ITEM_EXISTS_ERROR_MESSAGE,
                        workItem.getItemType(),
                        workItem.getTitle(),
                        name));
        workItems.add(workItem);
        addActivity(String.format(WORKITEM_ADDED_TO_BOARD, workItem.getItemType(), workItem.getTitle(), workItem.getID(), name));
    }

    public void removeWorkItems(WorkItem workItem) {
        ValidationHelper.checkIfItemDoesNotExist(workItems, workItem,
                String.format(ITEM_NOT_FOUND,
                        workItem.getItemType(),
                        workItem.getTitle(),
                        name));
        workItems.remove(workItem);
        addActivity(String.format(WORKITEM_REMOVED_FROM_BOARD, workItem.getItemType(), workItem.getTitle(), workItem.getID(), name));
    }

    public List<WorkItem> listWorkItems() {
        return new ArrayList<>(workItems);
    }

    public void addActivity(String activity) {
        activityHistory.add(activity);
    }

    public String showActivity() {
        StringBuilder str = new StringBuilder();
        str.append(String.format("Board: %s\n", name));
        str.append("History:\n");
        for (String activity : activityHistory) {
            str.append(String.format("%s\n", activity));
        }
        String listBoardActivity = str.toString();
        return listBoardActivity;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Board name: %s\n", getName()));
        builder.append("Board's activity:\n");
        for (String comment : activityHistory) {
            builder.append(String.format("%s;\n", comment));
        }
        builder.append("Work items:\n");
        for (WorkItem workitem : workItems) {
            builder.append(String.format("%s\n", workitem));
        }
        return builder.toString();
    }

    private void setName(String name) {
        ValidationHelper.checkName(name, getMinLength(), getMaxLength(), "Board name");
        this.name = name;
    }
}
