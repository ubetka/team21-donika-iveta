package com.teams.models;

import com.teams.contracts.Person;
import com.workitems.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

import static com.commands.common.CommandConstants.*;

public class PersonImpl implements Person {

    private static final int MIN_NAME_LENGTH = 5;
    private static final int MAX_NAME_LENGTH = 15;

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    public PersonImpl(String name) {
        setName(name);
        workItems = new ArrayList<>();
        activityHistory = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public int getMinLength() {
        return MIN_NAME_LENGTH;
    }

    public int getMaxLength() {
        return MAX_NAME_LENGTH;
    }

    @Override
    public void assignWorkItem(WorkItem workItem){
        ValidationHelper.checkIfItemAlreadyExists(workItems, workItem,
                String.format(WORKITEM_ASSIGNED_ERROR_MESSAGE,
                        workItem.getItemType(),
                        workItem.getTitle(),
                        workItem.getID(), name));
        workItems.add(workItem);
        addActivity(String.format(WORKITEM_ASSIGNED_TO_PERSON, workItem.getItemType(), workItem.getTitle(), workItem.getID(), name));
    }

    @Override
    public void unassignWorkItem(WorkItem workItem) {
        ValidationHelper.checkIfItemDoesNotExist(workItems, workItem, String.format(WORKITEM_NOT_ASSIGNED,
                workItem.getItemType(),
                workItem.getTitle(),
                workItem.getID(), name));
        workItems.remove(workItem);
        addActivity(String.format(WORKITEM_UNASSIGNED_FROM_PERSON, workItem.getItemType(), workItem.getTitle(),workItem.getID(), name));
    }

    public List<WorkItem> listWorkItems() {
        return new ArrayList<>(workItems);
    }

    public void addActivity(String activity) {
        activityHistory.add(activity);
    }

    public String showActivity() {
        StringBuilder str = new StringBuilder();
        str.append(String.format("Person's name: %s\n", name));
        str.append("Activity History:\n");
        for (String activity : activityHistory) {
            str.append(String.format("%s\n", activity));
        }
        String listPersonActivity = str.toString();
        return listPersonActivity;
    }

    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Persons's name: %s\n", getName()));
        builder.append("Person's activity:\n");
        for (String activity : activityHistory) {
            builder.append(String.format("%s;\n", activity));
        }
        builder.append("Work items:\n");
        for (WorkItem workitem : workItems) {
            builder.append(String.format("%s\n", workitem));
        }

        return builder.toString();
    }

    public void setName(String name) {
        ValidationHelper.checkName(name, getMinLength(), getMaxLength(), "Person's name");
        this.name = name;
    }
}
