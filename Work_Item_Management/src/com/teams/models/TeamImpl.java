package com.teams.models;

import com.commands.common.CommandValidator;
import com.teams.contracts.Board;
import com.teams.contracts.Person;
import com.teams.contracts.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import static com.commands.common.CommandConstants.*;

public class TeamImpl implements Team {

    private static final int MIN_TEAM_NAME_LENGTH = 5;
    private static final int MAX_TEAM_NAME_LENGTH = 10;
    private String name;
    private List<Person> members;
    private List<Board> boards;
    private List<String> teamActivity;

    public TeamImpl(String name) {
        setName(name);
        members = new ArrayList<>();
        boards = new ArrayList<>();
        teamActivity = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public static int getMinTeamNameLength() {
        return MIN_TEAM_NAME_LENGTH;
    }

    public static int getMaxTeamNameLength() {
        return MAX_TEAM_NAME_LENGTH;
    }

    public void addMember(Person person) {
        ValidationHelper.checkIfItemAlreadyExists(members, person,
                String.format(ITEM_EXISTS_ERROR_MESSAGE,
                person.getClass().getSimpleName().replace("Impl",""),
                person.getName(),
                name));
        members.add(person);
        addTeamActivity(String.format(MEMBER_ADDED_TO_TEAM,person.getName(), name));
    }

    public void removeMember(Person person) {
        ValidationHelper.checkIfItemDoesNotExist(members, person,
                String.format(ITEM_NOT_FOUND,
                person.getClass().getSimpleName().replace("Impl",""),
                person.getName(),
                name));
        members.remove(person);
        addTeamActivity(String.format(MEMBER_REMOVED_FROM_TEAM, person.getName(), name));
    }

    public String showMembers() {
        StringJoiner joiner = new StringJoiner(", ");
        for (Person person : members) {
            joiner.add(person.getName());
        }
        String listOfMembers = joiner.toString();
        CommandValidator.isEmpty(listOfMembers);
        return listOfMembers;
    }

    public List <Person> getMembers(){
        return new ArrayList<>(members);
    }

    public List<Board> getBoards(){
        return new ArrayList<>(boards);
    }

    public void addBoard(Board board) {
        ValidationHelper.checkIfItemAlreadyExists(boards, board,
                String.format(ITEM_EXISTS_ERROR_MESSAGE,
                board.getClass().getSimpleName().replace("Impl",""),
                board.getName(),
                name));
        boards.add(board);
        addTeamActivity(String.format(BOARD_CREATED_SUCCESS_MESSAGE, board.getName(), name));
    }

    public void removeBoard(Board board) {
        ValidationHelper.checkIfItemDoesNotExist(boards, board,
                String.format(ITEM_NOT_FOUND,
                board.getClass().getSimpleName().replace("Impl",""),
                board.getName(),
                boards.getClass().getSimpleName()));
        boards.remove(board);
        addTeamActivity(String.format(BOARD_REMOVED_FROM_TEAM, board.getName(), name));
    }

    public String showBoards() {
        StringJoiner joiner = new StringJoiner(", ");
        for (Board board : boards) {
            joiner.add(board.getName());
        }
        String listOfBoards = joiner.toString();
        CommandValidator.isEmpty(listOfBoards);
        return listOfBoards;
    }

    public void addTeamActivity(String activity) {
        teamActivity.add(activity);
    }

    public String showTeamActivity() {
        StringBuilder str = new StringBuilder();
        str.append(String.format("Team: %s\n", name));
        str.append("Team Activity:\n");
        for (String activity : teamActivity) {
            str.append(String.format("%s\n", activity));
        }
        String listTeamActivity = str.toString();
        return listTeamActivity;
    }

    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("Team name: %s\n" +
                "Members: %s; \n" +
                "Boards: %s; \n" +
                "Team Activity:\n", getName(), showMembers(), showBoards()));
        for (Person person : members) {
            builder.append(String.format("%s\n", person.showActivity()));
        }
        return builder.toString();
    }

    private void setName(String name) {
        ValidationHelper.checkName(name, getMinTeamNameLength(), getMaxTeamNameLength(), "Team name");
        this.name = name;
    }
}
