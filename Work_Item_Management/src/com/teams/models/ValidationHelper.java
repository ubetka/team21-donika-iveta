package com.teams.models;

import java.util.List;

public class ValidationHelper {
    private static final String NAME_ERROR_MESSAGE = "Value out of bounds. %s should be between %d and %d characters.";
    private static final int MIN_RATING_VALUE = 1;
    private static final int MAX_RATING_VALUE = 10;
    private static final String RATING_ERROR_MESSAGE = "Rating must be between 1 and 10.";

    public static void checkName (String name, int minLength, int maxLength, final String parameter){
        if (name.length() < minLength || name.length() > maxLength){
            throw new IllegalArgumentException(String.format(NAME_ERROR_MESSAGE, parameter, minLength, maxLength));
        }
    }

    public static void checkRating(int rating){
        if (rating < MIN_RATING_VALUE || rating > MAX_RATING_VALUE){
            throw new IllegalArgumentException(RATING_ERROR_MESSAGE);
        }
    }

    public static void checkIfItemAlreadyExists(List list, Object item, String error){
        if (list.contains(item)){
            throw new IllegalArgumentException(error);
        }
    }

    public static void checkIfItemDoesNotExist(List list, Object item, String error){
        if (!list.contains(item)){
            throw new IllegalArgumentException(error);
        }
    }
}
