package com.workitems.contracts;

import com.workitems.enums.SeverityType;

import java.util.List;

public interface Bug extends BugAndStory{

    List<String> getStepsToReproduce();

    void addStepsToReproduce(List<String>steps);

    SeverityType getSeverity();

    void setSeverity(SeverityType newSeverityType);
}
