package com.workitems.contracts;

import com.teams.contracts.Person;
import com.workitems.enums.PriorityType;

import java.util.List;

public interface BugAndStory extends WorkItem {
    PriorityType getPriority();
    List<String> getAssignee();
    void setAssignee(String name);
    void removeAssignee(String name);
    void setPriority(PriorityType priority);
}
