package com.workitems.contracts;

public interface Feedback extends WorkItem {

    int getRating();
    void setRating(int rating);

}
