package com.workitems.contracts;

import com.workitems.enums.SizeType;

public interface Story extends BugAndStory{

    SizeType getSize();
    void setSize(SizeType sizeType);

}
