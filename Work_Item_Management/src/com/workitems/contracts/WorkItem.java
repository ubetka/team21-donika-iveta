package com.workitems.contracts;

import java.util.List;

public interface WorkItem {
    int getID();
    String getTitle();
    String getDescription();
    String getItemType();
    Enum getStatus();
    void addComment(String comment);
    void removeComment(String comment);
    List<String> getComments();
    String showComments();
    List<String> showHistory();
    void setStatus(String newStatus);
}
