package com.workitems.enums;

public enum PriorityType {
    HIGH,
    MEDIUM,
    LOW,
    NOTASSIGNED;

    @Override
    public String toString() {
        switch (this) {
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
            case NOTASSIGNED:
                return "Not assigned";
            default:
                throw new IllegalArgumentException();
        }
    }
}
