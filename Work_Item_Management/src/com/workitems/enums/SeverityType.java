package com.workitems.enums;

public enum SeverityType {
    CRITICAL(0),
    MAJOR(1),
    MINOR(2);

    private final int weight;

    SeverityType(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                throw new IllegalArgumentException();
        }
    }
}
