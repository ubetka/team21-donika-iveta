package com.workitems.enums;

public enum SizeType {
    LARGE (0),
    MEDIUM (1),
    SMALL(2),
    NOTASSIGNED(3);

    public int weight;

    SizeType(int weight){
        this.weight = weight;
    }

    @Override
    public String toString(){
        switch (this){
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            case NOTASSIGNED:
                return "Not assigned";
            default:
                throw new IllegalArgumentException();
        }
    }
}
