package com.workitems.models;

import com.workitems.enums.PriorityType;
import com.workitems.contracts.BugAndStory;

import java.util.ArrayList;
import java.util.List;

import static com.commands.common.CommandConstants.*;

public abstract class BugAndStoryImpl extends WorkItemImpl implements BugAndStory {
    private PriorityType priorityType;
    private List<String> assignee;

    protected BugAndStoryImpl(String title, String description) {
        super(title, description);
        assignee = new ArrayList<>();
        this.priorityType = PriorityType.NOTASSIGNED;
    }

    public void setPriority(PriorityType priorityType) {
        this.priorityType = priorityType;
        super.history.add(String.format(PRIORITY_TYPE_CHANGED, priorityType.toString()));
    }

    @Override
    public PriorityType getPriority() {
        return priorityType;
    }

    @Override
    public List<String> getAssignee() {
        return new ArrayList<>(assignee);
    }

    public void setAssignee(String name) {
        assignee.add(name);
        super.history.add(String.format(NEW_WORKITEM_ASSIGNED, name));
    }

    public void removeAssignee(String name) {
        assignee.remove(name);
        super.history.add(String.format(WORKITEM_UNASSIGNED, name));
    }
}
