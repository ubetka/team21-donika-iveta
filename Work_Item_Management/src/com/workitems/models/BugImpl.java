package com.workitems.models;

import com.workitems.enums.SeverityType;
import com.workitems.enums.StatusTypeBug;
import com.workitems.contracts.Bug;

import java.util.ArrayList;
import java.util.List;

import static com.commands.common.CommandConstants.*;

public class BugImpl extends BugAndStoryImpl implements Bug {
    private List<String> stepsToReproduce;
    private SeverityType severityType;


    public BugImpl(String title, String description, SeverityType severityType) {
        super(title, description);
        this.status = StatusTypeBug.ACTIVE;
        this.severityType = severityType;
        setStepsToReproduce(stepsToReproduce);
    }

    @Override
    public String getItemType() {
        return EXPECTED_TYPE_BUG;
    }

    @Override
    public Enum getStatus() {
        return status;
    }

    public void setStatus(String newStatus) {
        this.status = StatusTypeBug.valueOf(newStatus.toUpperCase());
        super.history.add(String.format(STATUS_TYPE_CHANGED, newStatus));
    }

    public void addStepsToReproduce(List<String> steps) {
        this.stepsToReproduce = steps;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public SeverityType getSeverity() {
        return severityType;
    }

    @Override
    public void setSeverity(SeverityType newSeverityType) {
        this.severityType = newSeverityType;
        super.history.add(String.format(SEVERITY_TYPE_CHANGED, newSeverityType.toString()));
    }

    private void setStepsToReproduce(List<String> stepsToReproduce) {
        this.stepsToReproduce = stepsToReproduce;
    }
}
