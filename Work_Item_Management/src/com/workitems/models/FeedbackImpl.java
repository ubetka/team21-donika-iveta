package com.workitems.models;

import com.teams.models.ValidationHelper;
import com.workitems.enums.StatusTypeFeedback;
import com.workitems.contracts.Feedback;

import static com.commands.common.CommandConstants.*;

public class FeedbackImpl extends WorkItemImpl implements Feedback {

    private int rating;
    private double ratingSum;
    private double ratingGiven;

    public FeedbackImpl(String title, String description) {
        super(title, description);
        this.rating = 0;
        this.status = StatusTypeFeedback.NEW;
    }

    @Override
    public String getItemType() {
        return EXPECTED_TYPE_FEEDBACK;
    }

    @Override
    public Enum getStatus() {
        return status;
    }

    @Override
    public void setStatus(String newStatus) {
        this.status = StatusTypeFeedback.valueOf(newStatus.toUpperCase());
        super.history.add(String.format(STATUS_TYPE_CHANGED, newStatus));
    }

    @Override
    public int getRating() {
        return Math.round(rating);
    }

    public void setRating(int rating) {
        ValidationHelper.checkRating(rating);
        ratingGiven++;
        ratingSum += rating;
        this.rating = (int) (Math.round(ratingSum / ratingGiven));
        super.history.add(NEW_RATING_ADDED);
    }
}
