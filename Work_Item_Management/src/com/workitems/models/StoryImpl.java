package com.workitems.models;

import com.workitems.enums.StatusTypeStory;
import com.workitems.enums.SizeType;
import com.workitems.contracts.Story;

import static com.commands.common.CommandConstants.*;

public class StoryImpl extends BugAndStoryImpl implements Story {

    private SizeType sizeType;

    public StoryImpl(String title, String description) {
        super(title, description);
        this.status = StatusTypeStory.NOTDONE;
        this.sizeType = SizeType.NOTASSIGNED;
    }

    @Override
    public String getItemType() {
        return EXPECTED_TYPE_STORY;
    }

    @Override
    public Enum getStatus() {
        return status;
    }

    @Override
    public void setStatus(String newStatus) {
        this.status = StatusTypeStory.valueOf(newStatus.toUpperCase());
        super.history.add(String.format(STATUS_TYPE_CHANGED, newStatus));
    }

    @Override
    public SizeType getSize() {
        return sizeType;
    }

    public void setSize(SizeType sizeType) {
        this.sizeType = sizeType;
        super.history.add(String.format(SIZE_TYPE_CHANGED, sizeType.toString()));
    }
}
