package com.workitems.models;

import com.commands.common.CommandValidator;
import com.teams.models.ValidationHelper;
import com.workitems.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import static com.commands.common.CommandConstants.*;

public abstract class WorkItemImpl implements WorkItem {


    private static final int MIN_TITLE_LENGTH = 10;
    private static final int MAX_TITLE_LENGTH = 50;
    private static final int MIN_DESCRIPTION_LENGTH = 10;
    private static final int MAX_DESCRIPTION_LENGTH = 500;

    private int id;
    private static int currentID;
    private String title;
    private String description;
    private List<String> comments;
    protected List<String> history;
    protected Enum status;

    static {
        currentID = 1;
    }

    protected WorkItemImpl(String title, String description) {
        setTitle(title);
        setDescription(description);
        comments = new ArrayList<>();
        history = new ArrayList<>();
        setID();
    }

    public abstract String getItemType();

    public abstract Enum getStatus();

    @Override
    public int getID() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public static int getMinTitleLength() {
        return MIN_TITLE_LENGTH;
    }

    public static int getMaxTitleLength() {
        return MAX_TITLE_LENGTH;
    }

    public static int getMinDescriptionLength() {
        return MIN_DESCRIPTION_LENGTH;
    }

    public static int getMaxDescriptionLength() {
        return MAX_DESCRIPTION_LENGTH;
    }

    public void addComment(String comment) {
        if (comments.contains(comment)) {
            throw new IllegalArgumentException(COMMENT_EXISTS_ERROR_MESSAGE);
        }
        comments.add(comment);
        history.add(COMMENT_ADDED);
    }

    public void removeComment(String comment) {
        if (!comments.contains(comment)) {
            throw new IllegalArgumentException(COMMENT_NOT_FOUND);
        }
        comments.remove(comment);
        history.add(COMMENT_REMOVED);
    }

    @Override
    public List<String> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public String showComments() {
        StringJoiner joiner = new StringJoiner(", ");
        for (String comment : comments) {
            joiner.add(comment);
        }
        String listOfComments = joiner.toString();
        CommandValidator.isEmpty(listOfComments);
        return listOfComments;
    }

    @Override
    public List<String> showHistory() {
        return new ArrayList<>(history);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("%s \"%s\"\n" +
                "Description: %s \n" +
                "Comments:\n", getItemType(), getTitle(), getDescription()));
        for (String comment : comments) {
            builder.append(String.format("\"%s\";\n", comment));
        }
        builder.append("Activity history:\n");
        for (String activity : history) {
            builder.append(String.format("%s;\n", activity));
        }
        return builder.toString();
    }

    private void setID() {
        this.id = currentID;
        currentID++;
    }

    private void setTitle(String title) {
        ValidationHelper.checkName(title, getMinTitleLength(), getMaxTitleLength(), "Title");
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelper.checkName(description, getMinDescriptionLength(), getMaxDescriptionLength(), "Description");
        this.description = description;
    }
}
